/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  afes
 * Created: Mar 22, 2018
 */

INSERT INTO bimar.bimar_api_coins(
            id, name, key, secret, website)
    VALUES (1,'BTH','bc4a-3a97-6fb1-1ffa','b1m4r1o0r4umum11','https://block.io/api/v2/'),
    (2,'LTC','1b74-8bb1-5de1-3fd1','b1m4r1o0r4umum11','https://block.io/api/v2/'),
(3,'DOGE','583e-c82d-b505-b9e2','b1m4r1o0r4umum11','https://block.io/api/v2/')
ON CONFLICT (name, key) DO NOTHING;

CREATE TABLE IF NOT EXISTS bimar.bimar_api_mcoins (
  id serial Not Null,
  name varchar(255),  
  name_skt varchar(255),  
  CONSTRAINT bimar_api_mcoins_pkey PRIMARY KEY (id),
  CONSTRAINT KEY_Bimar_Api_mcoins UNIQUE(name)  
);
Alter Table bimar.bimar_api_mcoins Owner TO bimar;

