SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

CREATE SCHEMA bimar AUTHORIZATION bimar;
GRANT ALL ON SCHEMA bimar TO bimar;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

SET search_path = bimar, pg_catalog;


CREATE TABLE IF NOT EXISTS bimar_admin_users (
  id serial Not Null,
  users varchar(50),
  pass varchar(50),
  first_name varchar(50),
  last_name varchar(50),
  company varchar(255),
  address varchar(255),
  city varchar(50),
  country_id integer,
  phone varchar(50),
  email varchar(50),
  website varchar(255),
  f_id integer DEFAULT 0,
  orders integer  DEFAULT 0,
  is_admin smallint DEFAULT 0,
  country_code integer,
  verified_authy smallint DEFAULT 0,
  authy_id varchar(255),
  date_authy_id timestamp,
  CONSTRAINT bimar_admin_users_pkey PRIMARY KEY (id)
);
Alter Table bimar_admin_users Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_site_users (
  id serial Not Null,
  pass varchar(200) ,
  first_name varchar(200) ,
  last_name varchar(200) ,
  email varchar(255),
  date timestamp with time zone,
  tel varchar(255),
  country integer,
  users varchar(255),
  fee_schedule integer,
  country_code integer,
  authy_requested smallint DEFAULT 0,
  verified_authy smallint DEFAULT 0,
  authy_id integer,
  using_sms smallint DEFAULT 0,
  dont_ask_30_days smallint DEFAULT 0,
  dont_ask_date timestamp with time zone,
  confirm_withdrawal_email_btc smallint DEFAULT 0,
  confirm_withdrawal_2fa_btc smallint DEFAULT 0,
  confirm_withdrawal_2fa_bank smallint DEFAULT 0,
  confirm_withdrawal_email_bank smallint DEFAULT 0,
  notify_deposit_btc smallint DEFAULT 0,
  notify_deposit_bank smallint DEFAULT 0,
  last_update timestamp with time zone,
  no_logins smallint DEFAULT 0,
  notify_login smallint DEFAULT 0,
  deactivated smallint DEFAULT 0,
  locked smallint DEFAULT 0,
  google_2fa_code varchar(255),
  verified_google smallint DEFAULT 0,
  last_lang varchar(255),
  default_currency integer,
  notify_withdraw_btc smallint DEFAULT 0,
  notify_withdraw_bank smallint DEFAULT 0,
  trusted smallint DEFAULT 0,
  CONSTRAINT bimar_site_users_pkey PRIMARY KEY (id)  
);

CREATE INDEX IF NOT EXISTS pass
   ON bimar_site_users (pass ASC NULLS LAST, users ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS users
   ON bimar_site_users (users ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS deactivated
   ON bimar_site_users (deactivated ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS trusted
   ON bimar_site_users (trusted ASC NULLS LAST);

Alter Table bimar_site_users Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_api_keys (
  id serial Not Null,
  key varchar(255),
  secret varchar(255),
  view smallint DEFAULT 1,
  orders smallint DEFAULT 0,
  withdraw smallint DEFAULT 0,
  site_user serial,
  nonce bigint ,
  CONSTRAINT bimar_api_keys_pkey PRIMARY KEY (id),
  CONSTRAINT KEY UNIQUE(key)  
);

CREATE INDEX IF NOT EXISTS site_user
   ON bimar_api_keys (site_user ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS nonce
   ON bimar_api_keys (nonce ASC NULLS LAST);
-- --------------------------------------------------------
Alter Table bimar_api_keys Owner TO bimar;
--
-- Table structure for table app_configuration
--

CREATE TABLE IF NOT EXISTS bimar_app_config (
  id serial Not Null,
  default_timezone varchar(255),
  orders_under_market_percent varchar(255) DEFAULT '35',
  orders_min_usd varchar(255) DEFAULT '5',
  bitcoin_sending_fee varchar(255) DEFAULT '0.0001',
  frontend_baseurl varchar(255),
  frontend_dirroot varchar(255),
  fiat_withdraw_fee varchar(255),
  api_db_debug smallint DEFAULT 1,
  api_dirroot varchar(255),
  support_email varchar(255),
  email_smtp_host varchar(255),
  email_smtp_port varchar(255) DEFAULT '465',
  email_smtp_security varchar(255) DEFAULT 'ssl',
  email_smtp_username varchar(255),
  email_smtp_password varchar(255),
  email_smtp_send_from varchar(255),
  bitcoin_username varchar(255),
  bitcoin_accountname varchar(255),
  bitcoin_passphrase varchar(255),
  bitcoin_host varchar(255),
  bitcoin_port varchar(255) DEFAULT '8332',
  bitcoin_protocol varchar(255) DEFAULT 'http',
  authy_api_key varchar(255),
  helpdesk_key varchar(255),
  exchange_name varchar(255) DEFAULT 'My Exchange',
  mcrypt_key varchar(255) DEFAULT '7QQvcT9Ga7R6QC3',
  currency_conversion_fee varchar(255) DEFAULT '2.5',
  cross_currency_trades smallint DEFAULT 1,
  btc_currency_id varchar(255) DEFAULT '28',
  deposit_bitcoin_desc varchar(255) DEFAULT '4',
  default_fee_schedule_id varchar(255) DEFAULT '1',
  history_buy_id varchar(255) DEFAULT '2',
  history_deposit_id varchar(255) DEFAULT '4',
  history_login_id varchar(255) DEFAULT '1',
  history_sell_id varchar(255) DEFAULT '3',
  history_withdraw_id varchar(255) DEFAULT '5',
  order_type_ask varchar(255) DEFAULT '2',
  request_awaiting_id varchar(255) DEFAULT '4',
  request_cancelled_id varchar(255) DEFAULT '3',
  request_completed_id varchar(255) DEFAULT '2',
  order_type_bid varchar(255) DEFAULT '1',
  request_deposit_id varchar(255) DEFAULT '2',
  request_pending_id varchar(255) DEFAULT '1',
  request_withdrawal_id varchar(255) DEFAULT '1',
  transactions_buy_id varchar(255) DEFAULT '1',
  transactions_sell_id varchar(255) DEFAULT '2',
  withdraw_fiat_desc varchar(255) DEFAULT '1',
  withdraw_btc_desc varchar(255) DEFAULT '2',
  form_email_from varchar(255),
  email_notify_new_users smallint DEFAULT 1,
  pass_regex varchar(255) DEFAULT '/[^0-9a-zA-Z!@#$%&*?\\.\\-\\_]/',
  pass_min_chars varchar(255) DEFAULT '8',
  auth_db_debug smallint DEFAULT 0,
  bitcoin_reserve_min varchar(255) DEFAULT '1',
  bitcoin_reserve_ratio varchar(255) DEFAULT '10',
  bitcoin_warm_wallet_address varchar(255),
  cron_db_debug smallint DEFAULT 1,
  quandl_api_key varchar(255),
  cron_dirroot varchar(255),
  backstage_db_debug smallint DEFAULT 1,
  backstage_dirroot varchar(255),
  email_notify_fiat_withdrawals smallint DEFAULT 1,
  contact_email varchar(255),
  cloudflare_api_key varchar(255),
  google_recaptch_api_key varchar(255),
  cloudflare_blacklist smallint DEFAULT 0,
  cloudflare_email varchar(255),
  google_recaptch_api_secret varchar(255),
  cloudflare_blacklist_attempts integer DEFAULT '150',
  cloudflare_blacklist_timeframe double precision DEFAULT '5.00',
  crypto_capital_pk varchar(255),
  deposit_fiat_desc varchar(255),
  email_notify_fiat_failed smallint DEFAULT 1,
  trading_status varchar(255),
  withdrawals_status varchar(255),
  CONSTRAINT bimar_config_pkey PRIMARY KEY(id)
);
Alter Table bimar_app_config Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_bank_accounts (
  id serial Not Null,
  account_number bigint,
  site_user integer,
  description varchar(255),
  currency integer,
  CONSTRAINT bimar_bank_account_pkey PRIMARY KEY(id)
);

CREATE INDEX IF NOT EXISTS site_user
   ON bimar_bank_accounts (site_user ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS account_number
   ON bimar_bank_accounts (account_number ASC NULLS LAST);

Alter Table bimar_bank_accounts Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_bitcoins_log (
  id serial Not Null,
  transaction_id varchar(255),
  amount double precision ,
  date timestamp with time zone,
  CONSTRAINT bimar_bitcoins_log_pkey PRIMARY KEY(id)
);
Alter Table bimar_bitcoins_log Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_bitcoin_addresses (
  id serial Not Null,
  address varchar(255),
  site_user integer,
  date timestamp with time zone,
  system_address smallint DEFAULT 0,
  hot_wallet smallint DEFAULT 0,
  warm_wallet smallint DEFAULT 0,
  CONSTRAINT bimar_bitcoins_address_pkey PRIMARY KEY(id)
);
CREATE INDEX IF NOT EXISTS address_2
   ON bimar_bitcoin_addresses (address ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS site_user
   ON bimar_bitcoin_addresses (site_user ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS system_address
   ON bimar_bitcoin_addresses (system_address ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS hot_wallet
   ON bimar_bitcoin_addresses (hot_wallet ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS warm_wallet
   ON bimar_bitcoin_addresses (warm_wallet ASC NULLS LAST);

Alter Table bimar_bitcoin_addresses Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_change_settings (
  id serial Not Null,
  request bytea,
  date timestamp with time zone,
  site_user serial,
  CONSTRAINT bimar_change_settings_pkey PRIMARY KEY(id)
);

CREATE INDEX IF NOT EXISTS site_user
   ON bimar_change_settings (site_user ASC NULLS LAST);


CREATE INDEX IF NOT EXISTS date
   ON bimar_change_settings (date ASC NULLS LAST);

Alter Table bimar_change_settings Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_conversions (
  id serial Not Null,
  amount double precision,
  currency integer,
  date timestamp with time zone,
  is_active smallint DEFAULT 0,
  total_withdrawals double precision,
  profit_to_factor double precision,
  factored smallint DEFAULT 0,
  date1 timestamp with time zone,
  CONSTRAINT bimar_conversions_pkey PRIMARY KEY(id)
);

CREATE INDEX IF NOT EXISTS is_active
   ON bimar_conversions (is_active ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS date
   ON bimar_conversions (date ASC NULLS LAST);

Alter Table bimar_conversions Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_currencies (
  id serial Not Null,
  currency varchar(3),
  fa_symbol varchar(255) ,
  account_number bigint ,
  account_name varchar(255),
  is_active smallint DEFAULT 0,
  usd_bid varchar(255),
  usd_ask varchar(255),
  name_en varchar(255),
  name_es varchar(255),
  name_ru varchar(255) ,
  name_zh varchar(255) ,
  CONSTRAINT bimar_currencies_pkey PRIMARY KEY(id)
);

CREATE INDEX IF NOT EXISTS currency
   ON bimar_currencies (currency ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS usd_bid
   ON bimar_currencies (usd_bid ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS usd_ask
   ON bimar_currencies (usd_ask ASC NULLS LAST);

Alter Table bimar_currencies Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_current_stats (
  id serial Not Null,
  total_btc bigint ,
  market_cap bigint ,
  trade_volume bigint ,
  CONSTRAINT bimar_currents_stats_pkey PRIMARY KEY(id)
);

Alter Table bimar_current_stats Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_daily_reports (
  id serial Not Null,
  date timestamp with time zone,
  total_btc double precision ,
  total_fiat_usd double precision ,
  open_orders_btc double precision ,
  btc_per_user double precision ,
  transactions_btc double precision ,
  avg_transaction_size_btc double precision ,
  transaction_volume_per_user double precision ,
  total_fees_btc double precision ,
  fees_per_user_btc double precision ,
  usd_per_user double precision ,
  gross_profit_btc double precision ,
  CONSTRAINT bimar_dayly_reports_pkey PRIMARY KEY(id)
);

CREATE INDEX IF NOT EXISTS date
   ON bimar_daily_reports (date ASC NULLS LAST);

Alter Table bimar_daily_reports Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_emails (
  id serial Not Null,
  key varchar(255),
  title_en varchar(255),
  title_es varchar(255),
  content_en bytea,
  content_es bytea,
  title_ru varchar(255) ,
  title_zh varchar(255) ,
  content_ru bytea,
  content_zh bytea,
  CONSTRAINT bimar_emails_pkey PRIMARY KEY(id)
);
CREATE INDEX IF NOT EXISTS KEY
   ON bimar_emails (key ASC NULLS LAST);

Alter Table bimar_emails Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_fees (
  id serial Not Null,
  fee double precision ,
  date timestamp with time zone,
  CONSTRAINT bimar_fees_pkey PRIMARY KEY (id)
);
CREATE INDEX IF NOT EXISTS date
   ON bimar_fees (date ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS fee
   ON bimar_fees (fee ASC NULLS LAST);

Alter Table bimar_fees Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_fee_schedule (
  id serial Not Null,
  fee double precision,
  from_usd double precision,
  to_usd double precision,
  orders integer,
  fee1 double precision,
  global_btc double precision ,
  CONSTRAINT bimar_fee_schedulle_pkey PRIMARY KEY(id)
);

CREATE INDEX IF NOT EXISTS from_usd
   ON bimar_fee_schedule (from_usd ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS to_usd
   ON bimar_fee_schedule (to_usd ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS global_btc
   ON bimar_fee_schedule (global_btc ASC NULLS LAST);

Alter Table bimar_fee_schedule Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_historical_data (
  id serial Not Null,
  date date,
  usd decimal(6,2) ,
  CONSTRAINT bimar_historical_data_pkey PRIMARY KEY(id)
);
CREATE INDEX IF NOT EXISTS date
   ON bimar_historical_data (date ASC NULLS LAST);

Alter Table bimar_historical_data Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_history (
  id serial Not Null,
  date timestamp with time zone,
  ip varchar(255),
  history_action integer,
  order_id integer,
  bitcoin_address varchar(255),
  request_id integer,
  site_user integer,
  balance_before double precision,
  balance_after double precision,
  CONSTRAINT bimar_history_pkey PRIMARY KEY (id)
);
CREATE INDEX IF NOT EXISTS date
   ON bimar_history (date ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS site_user
   ON bimar_history (site_user ASC NULLS LAST);

Alter Table bimar_history Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_history_actions (
  id serial Not Null,
  name_en varchar(255),
  name_es varchar(255),
  name_ru varchar(255),
  name_zh varchar(255),
  CONSTRAINT bimar_history_action_pkey PRIMARY KEY(id)
);
Alter Table bimar_history_actions Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_ip_access_log (
  ip bigint,
  timesx timestamp with time zone,
  login smallint DEFAULT 0
);
CREATE INDEX IF NOT EXISTS timestamp
   ON bimar_ip_access_log (timesx ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS ip
   ON bimar_ip_access_log (ip ASC NULLS LAST);

Alter Table bimar_ip_access_log Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_iso_countries (
  id integer,
  country_id integer,
  locale varchar(10) DEFAULT 'en',
  code char(2),
  name varchar(200) DEFAULT NULL,
  prefix varchar(50) DEFAULT NULL,
  CONSTRAINT bimar_countries_pkey PRIMARY KEY(id)
);

Alter Table bimar_iso_countries Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_lang (
  id serial Not Null,
  key varchar(255),
  esp varchar(255),
  eng varchar(255),
  orders varchar(255),
  p_id integer,
  zh varchar(255) ,
  ru varchar(255) ,
  CONSTRAINT bimar_lang_pkey PRIMARY KEY(id)
);
CREATE INDEX IF NOT EXISTS p_id
   ON bimar_lang (p_id ASC NULLS LAST);

Alter Table bimar_lang Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_monthly_reports (
  id serial Not Null,
  date timestamp with time zone,
  transactions_btc double precision,
  avg_transaction_size_btc double precision,
  transaction_volume_per_user double precision,
  total_fees_btc double precision,
  fees_per_user_btc double precision,
  gross_profit_btc double precision,
  CONSTRAINT bimar_monthly_report_pkey PRIMARY KEY(id)
);

Alter Table bimar_monthly_reports Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_orders (
  id serial Not Null,
  date timestamp with time zone,
  order_type integer,
  site_user integer,
  btc double precision ,
  fiat double precision,
  currency integer,
  btc_price double precision,
  market_price smallint DEFAULT 0,
  log_id integer,
  stop_price double precision,
  CONSTRAINT bimar_orders_pkey PRIMARY KEY(id)
);
CREATE INDEX IF NOT EXISTS site_user
   ON bimar_orders (site_user ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS currency
   ON bimar_orders (currency ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS btc_price
   ON bimar_orders (btc_price ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS stop_price
   ON bimar_orders (stop_price ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS log_id
   ON bimar_orders (log_id ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS market_price
   ON bimar_orders (market_price ASC NULLS LAST);

Alter Table bimar_orders Owner TO bimar;

DROP TYPE IF EXISTS status_type CASCADE;

CREATE TYPE status_type AS ENUM ('ACTIVE','FILLED','CANCELLED_USER','OUT_OF_FUNDS','REPLACED');  

CREATE TABLE IF NOT EXISTS bimar_order_log (
  id serial Not Null,
  date timestamp with time zone,
  site_user integer,
  order_type integer,
  btc double precision,
  market_price smallint DEFAULT 0,
  btc_price double precision,
  fiat double precision,
  currency integer,
  p_id integer,
  stop_price varchar(255),
  status status_type,
  btc_remaining double precision ,
  CONSTRAINT bimar_order_log_pkey PRIMARY KEY(id)
);
CREATE INDEX IF NOT EXISTS site_user
   ON bimar_order_log (site_user ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS p_id
   ON bimar_order_log (p_id ASC NULLS LAST);

Alter Table bimar_order_log Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_order_types (
  id serial Not Null,
  name_en varchar(255),
  name_ru varchar(255) ,
  name_zh varchar(255) ,
  CONSTRAINT bimar_orders_type_pkey PRIMARY KEY (id)
);

Alter Table bimar_order_types Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_requests (
  id serial Not Null,
  date timestamp with time zone,
  site_user integer,
  currency integer,
  amount double precision ,
  description integer,
  request_status integer,
  request_type integer,
  address_id serial Not Null,
  account bigint ,
  send_address varchar(255),
  transaction_id varchar(255),
  increment double precision,
  done smallint DEFAULT 0,
  crypto_id integer,
  fee double precision,
  net_amount double precision,
  notified integer,
  CONSTRAINT bimar_request_pkey PRIMARY KEY(id)
);
CREATE INDEX IF NOT EXISTS crypto_id
   ON bimar_requests (crypto_id ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS date_2
   ON bimar_requests (date ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS site_user
   ON bimar_requests (site_user ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS currency
   ON bimar_requests (currency ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS request_status
   ON bimar_requests (request_status ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS request_type
   ON bimar_requests (request_type ASC NULLS LAST);         
CREATE INDEX IF NOT EXISTS done
   ON bimar_requests (done ASC NULLS LAST);         
CREATE INDEX IF NOT EXISTS notified
   ON bimar_requests (notified ASC NULLS LAST);         

Alter Table bimar_requests Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_request_descriptions (
  id serial Not Null,
  name_en varchar(255),
  name_es varchar(255),
  name_ru varchar(255) ,
  name_zh varchar(255) ,
  CONSTRAINT bimar_request_description_pkey PRIMARY KEY(id)
);

Alter Table bimar_request_descriptions Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_request_status (
  id serial Not Null,
  name_en varchar(255),
  name_es varchar(255),
  name_ru varchar(255) ,
  name_zh varchar(255) ,
  CONSTRAINT bimar_req_status_pkey PRIMARY KEY(id)
);

Alter Table bimar_request_status Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_request_types (
  id serial Not Null,
  name_en varchar(255),
  name_es varchar(255),
  name_ru varchar(255) ,
  name_zh varchar(255) ,
  CONSTRAINT bimar_req_types_pkey PRIMARY KEY(id)
);

Alter Table bimar_request_types Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_sessions (
  session_id serial Not Null,
  session_start timestamp with time zone DEFAULT now(),
  session_end timestamp with time zone DEFAULT now(),
  session_key varchar(255),
  user_id serial Not Null,
  nonce serial,
  awaiting smallint,
  ip varchar(255),
  CONSTRAINT bimar_sessions_pkey PRIMARY KEY(session_id)
);
Alter Table bimar_sessions Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_site_users_balances (
  id serial Not Null,
  balance double precision,
  site_user integer,
  currency integer,
  CONSTRAINT bimar_site_user_pkey PRIMARY KEY(id)
);
CREATE INDEX IF NOT EXISTS site_user
   ON bimar_site_users_balances (site_user ASC NULLS LAST);         
CREATE INDEX IF NOT EXISTS currency
   ON bimar_site_users_balances (currency ASC NULLS LAST);  

Alter Table bimar_site_users_balances Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_transactions (
  id serial Not Null,
  date timestamp with time zone,
  site_user integer,
  btc double precision ,
  currency serial,
  btc_price double precision,
  fiat double precision,
  fee double precision ,
  transaction_type integer,
  site_user1 integer,
  transaction_type1 integer,
  fee1 double precision ,
  btc_net double precision,
  btc_net1 double precision,
  btc_before1 double precision,
  btc_after1 double precision,
  fiat_before1 double precision,
  fiat_after1 double precision,
  btc_before double precision,
  btc_after double precision,
  fiat_before double precision,
  fiat_after double precision,
  log_id1 integer,
  log_id integer,
  fee_level double precision,
  fee_level1 double precision,
  currency1 integer,
  orig_btc_price double precision,
  conversion_fee double precision,
  convert_amount double precision,
  convert_rate_given double precision,
  convert_system_rate double precision,
  convert_from_currency integer,
  convert_to_currency integer,
  conversion smallint DEFAULT 0,
  bid_at_transaction double precision,
  ask_at_transaction double precision,
  factored smallint DEFAULT 0,
  CONSTRAINT bimar_transaction_pkey PRIMARY KEY(id)
);
CREATE INDEX IF NOT EXISTS date_2
   ON bimar_transactions (date ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS transaction_type
   ON bimar_transactions (transaction_type ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS site_user1
   ON bimar_transactions (site_user1 ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS transaction_type1
   ON bimar_transactions (transaction_type1 ASC NULLS LAST);   
CREATE INDEX IF NOT EXISTS site_user
   ON bimar_transactions (site_user ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS currency
   ON bimar_transactions (currency ASC NULLS LAST);
CREATE INDEX IF NOT EXISTS currency1
   ON bimar_transactions (currency1 ASC NULLS LAST);   
CREATE INDEX IF NOT EXISTS convert_from_currency
   ON bimar_transactions (convert_from_currency ASC NULLS LAST);   
CREATE INDEX IF NOT EXISTS convert_to_currency
   ON bimar_transactions (convert_to_currency ASC NULLS LAST); 
CREATE INDEX IF NOT EXISTS conversion
   ON bimar_transactions (conversion ASC NULLS LAST); 
CREATE INDEX IF NOT EXISTS factored
   ON bimar_transactions (factored ASC NULLS LAST); 
CREATE INDEX IF NOT EXISTS log_id1
   ON bimar_transactions (log_id1 ASC NULLS LAST); 
CREATE INDEX IF NOT EXISTS log_id2
   ON bimar_transactions (log_id ASC NULLS LAST); 
CREATE INDEX IF NOT EXISTS site_user2
   ON bimar_transactions (site_user ASC NULLS LAST,site_user1 ASC NULLS LAST);

Alter Table bimar_transactions Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar_transaction_types (
  id serial Not Null,
  name_en varchar(255),
  name_es varchar(255),
  name_ru varchar(255) ,
  name_zh varchar(255) ,
  CONSTRAINT bimar_transaction_types_pkey PRIMARY KEY (id)
);
Alter Table bimar_transaction_types Owner TO bimar;

INSERT INTO bimar_app_config (id, default_timezone,
    email_smtp_username, email_smtp_password, 
    email_smtp_send_from, frontend_baseurl) VALUES
(1, 'Asia/Jakarta', 'trdcrpt', 'Oraumum1!', 'trdcrpt@gmail.com', 'bimar.io');


INSERT INTO bimar_admin_users 
(id, users, pass, first_name, last_name, company, address, city, country_id, phone, email,
 website, f_id, orders, is_admin, country_code, verified_authy, authy_id) VALUES
(1, 'admin', md5('admin'), 'Admin', 'Temp', '', '', '', 0, '', 'trdcrpt@gmail.com', '', 11, 0, '1', 0, '1', '');

INSERT INTO bimar.bimar_site_users(
            pass, first_name, last_name, email, users, 
            fee_schedule, verified_authy, 
            last_update, deactivated, trusted)
    VALUES (md5('admin'), 'Admin', 'System', 'trdcrpt@gmail.com', 'admin', 
            0, 1, now(), 0, 1);

INSERT INTO bimar_currencies (id, currency, fa_symbol, account_number, account_name, is_active, usd_bid, usd_ask, name_en, name_es, name_ru, name_zh) VALUES
(1, 'EUR', '€', 9120143295, '1BTCXE(EUR)', 1, '1.1348', '1.1349', 'Euro', 'Euro', 'евро', '欧元'),
(2, 'CNY', '¥', 9120083495, '1BTCXE(CNY)', 1, '0.1610', '0.1610', 'Renminbi Yuan', 'Renminbi Yuan', 'юань', '人民币'),
(3, 'GBP', '£', 9120083495, '1BTCXE(GBP)', 1, '1.5875', '1.5876', 'Great British Pound', 'Libras Esterlinas', 'Британский фунт стерлингов', '英镑'),
(4, 'JPY', '¥', 9120083495, '1BTCXE(JPY)', 1, '0.0082', '0.0082', 'Japanese Yen', 'Yen japonés', 'японская иена', '日元'),
(5, 'CAD', 'C$', 9120083495, '1BTCXE(CAD)', 1, '0.8147', '0.8151', 'Canadian Dollar', 'dólar canadiense', 'Канадский доллар', '加拿大元'),
(6, 'BGN', 'лв', 9120083495, '1BTCXE(BGN)', 1, '0.5801', '0.5803', 'Bulgarian Lev', 'Lev búlgaro', 'Болгарский Лейва', '保加利亚莱瓦'),
(7, 'CZK', 'Kč', 9120083495, '1BTCXE(CZK)', 1, '0.0416', '0.0416', 'Czech koruna', 'corona checa', 'Чешская крона', '捷克克朗'),
(8, 'DKK', 'kr', 9120083495, '1BTCXE(DKK)', 1, '0.1521', '0.1521', 'Danish krone', 'corona danesa', 'Датская Крона', '丹麦克朗'),
(9, 'HKD', 'HK$', 9120083495, '1BTCXE(HKD)', 1, '0.1290', '0.1290', 'Hong Kong Dollar', 'Dólar de Hong Kong', 'Гонконгский доллар', '港元'),
(10, 'HRK', 'kn', 9120083495, '1BTCXE(HRK)', 1, '0.1497', '0.1498', 'Croatian Kuna', 'Kuna croata', 'Хорватская куна', '克罗地亚库纳'),
(11, 'HUF', 'Ft', 9120083495, '1BTCXE(HUF)', 1, '0.0036', '0.0036', 'Hungarian Forint', 'Florín húngaro', 'Венгерский форинт', '匈牙利福林'),
(12, 'ILS', '₪', 9120083495, '1BTCXE(ILS)', 1, '0.2614', '0.2615', 'Israeli Shekel', 'Shekel israelí', 'израильский шекель', '以色列谢克尔'),
(13, 'INR', '₹', 9120083495, '1BTCXE(INR)', 1, '0.0158', '0.0158', 'Indian Rupee', 'Rupia india', 'Индийская рупия', '印度卢比'),
(14, 'LTL', 'Lt', 9120083495, '1BTCXE(LTL)', 1, '0.3285', '0.3285', '', '', '', ''),
(15, 'LVL', 'Ls', 9120083495, '1BTCXE(LVL)', 1, '1.6137', '1.6142', '', '', '', ''),
(16, 'MXN', 'Mex$', 9120083495, '1BTCXE(MXN)', 1, '0.0652', '0.0652', 'Mexican peso', 'Peso mejicano', 'мексиканское песо', '墨西哥比索'),
(17, 'NOK', 'kr', 9120083495, '1BTCXE(NOK)', 1, '0.1285', '0.1286', 'Norwegian krone', 'corona noruega', 'Норвежская крона', 'Nuówēi kèlǎng'),
(18, 'NZD', 'NZ$', 9120083495, '1BTCXE(NZD)', 1, '0.6899', '0.6900', 'New Zealand dollar', 'Dólar de Nueva Zelanda', 'Новозеландский доллар', '纽元'),
(19, 'PLN', 'zł', 9120083495, '1BTCXE(PLN)', 1, '0.2712', '0.2715', 'Polish z?oty', 'zloty polaco', 'Польский злотый', 'Bōlán zī luō tí'),
(20, 'RON', 'lei', 9120083495, '1BTCXE(RON)', 1, '0.2524', '0.2527', 'Romanian lei', 'leu rumano', 'Румынский лей', '罗马尼亚列伊'),
(21, 'RUB', 'py6', 9120083495, '1BTCXE(RUB)', 1, '0.0185', '0.0185', 'Russian ruble', 'Rublo ruso', 'русский рубль', '俄罗斯卢布'),
(22, 'SEK', 'kr', 9120083495, '1BTCXE(SEK)', 1, '0.1230', '0.1231', 'Swedish krona', 'corona sueca', 'шведские кроны', '瑞典克朗'),
(23, 'SGD', 'S$', 9120083495, '1BTCXE(SGD)', 1, '0.7486', '0.7487', 'Singapore dollar', 'dólar de Singapur', 'Сингапурский доллар', '新加坡元'),
(24, 'THB', '฿', 9120083495, '1BTCXE(THB)', 1, '0.0297', '0.0298', 'Thai baht', 'baht tailandés', 'Тайский бат', 'Tàizhū'),
(25, 'TRY', 'TL', 9120083495, '1BTCXE(TRY)', 1, '0.3706', '0.3710', 'Turkish lira', 'lira turca', 'Турецкая лира', '土耳其里拉'),
(26, 'ZAR', 'R', 9120083495, '1BTCXE(ZAR)', 1, '0.0822', '0.0823', 'South African rand', 'Rand sudafricano', 'Южноафриканский рэнд', '南非兰特'),
(27, 'USD', '$', 9120083495, '1BTCXE', 1, '1', '1', 'US Dollar', 'dólar estadounidense', 'Доллар США', '美元'),
(28, 'BTC', 'BTC', 9120083495, '', 1, '216.74796', '216.74796', 'Bitcoin', 'Bitcoin', 'биткойн', '比特币'),
(35, 'COP', 'COL$', 9120083495, '', 0, '', '', '', '', '', ''),
(36, 'ARS', 'ARS$', 9120083495, '', 0, '', '', '', '', '', ''),
(37, 'MUR', 'Rs', 9120083495, '1BTCXE(MUR)', 1, '0.0285', '0.0287', 'Mauritian rupee', 'rupia de Mauricio', 'Маврикийская рупия', '毛里求斯卢比'),
(38, 'TWD', 'NT$', 9120083495, '', 0, '', '', '', '', '', ''),
(39, 'AUD', 'A$', 9120083495, '1BTCXE(AUD)', 1, '0.7759', '0.7764', 'Australian Dollar', 'Dólar australiano', 'Австралийский доллар', '澳大利亚元'),
(40, 'CHF', 'CHF', 9120083495, '1BTCXE(CHF)', 1, '1.0862', '1.0866', 'Swiss Franc', 'Franco suizo', 'Швейцарский франк', '瑞士法郎'),
(41, 'AED', 'Dh', 9120143295, '1BTCXE(AED)', 1, '0.2722', '0.2723', 'Arab Emirates Dirham', 'Dírham Emiratos Árabes Unidos', 'Дирхам ОАЭ', '阿联酋迪拉姆'),
(42, 'BRL', 'R$', 9120143295, '1BTCXE(BRL)', 1, '0.3238', '0.3246', 'Brazilian Real', 'Brazilian Real', 'Brazilian Real', 'Brazilian Real');

INSERT INTO bimar_emails (id, key, title_en) VALUES
(11, 'forgot', 'Password Reset Request'),
(22, 'new-deposit', 'New Deposit Received ([amount] [currency])'),
(12, 'contact', 'Contact Form'),
(13, 'request-auth', 'Withdrawal Authorization'),
(14, 'register', 'Your Registration Information'),
(15, 'settings-auth', 'Settings Change Request'),
(16, 'register-notify', 'New User Registered'),
(17, 'login-notify', 'Login Notification'),
(18, 'security-auth', 'Two-factor Authentication Request'),
(19, 'order-cancelled', 'Order Partially Executed'),
(20, 'register-existing', 'Attempt to Register Existing Account'),
(23, 'new-withdrawal', 'New Withdrawal Request ([amount] [currency])'),
(24, 'bruteforce-notify', 'Multiple Failed Login Attempts'),
(25, 'pending-withdrawals', 'Pending Withdrawals'),
(26, 'fiat-deposit-failure', 'Fiat Deposit #[id] Failure');

INSERT INTO bimar_iso_countries (id, country_id, locale, code, name, prefix) VALUES
(1, 1, 'en', 'AF', 'Afghanistan', '+93'),
(2, 2, 'en', 'AL', 'Albania', '+355'),
(3, 3, 'en', 'DZ', 'Algeria', '+213'),
(4, 4, 'en', 'AD', 'Andorra', '+376'),
(5, 5, 'en', 'AO', 'Angola', '+244'),
(6, 6, 'en', 'AG', 'Antigua and Barbuda', '+1-268'),
(7, 7, 'es', 'AR', 'Argentina', '+54'),
(8, 8, 'en', 'AM', 'Armenia', '+374'),
(9, 9, 'en', 'AU', 'Australia', '+61'),
(10, 10, 'de', 'AT', 'Austria', '+43'),
(11, 11, 'en', 'AZ', 'Azerbaijan', '+994'),
(12, 12, 'en', 'BS', 'Bahamas, The', '+1-242'),
(13, 13, 'en', 'BH', 'Bahrain', '+973'),
(14, 14, 'en', 'BD', 'Bangladesh', '+880'),
(15, 15, 'en', 'BB', 'Barbados', '+1-246'),
(16, 16, 'ru', 'BY', 'Belarus', '+375'),
(17, 17, 'fr', 'BE', 'Belgium', '+32'),
(18, 18, 'en', 'BZ', 'Belize', '+501'),
(19, 19, 'en', 'BJ', 'Benin', '+229'),
(20, 20, 'en', 'BT', 'Bhutan', '+975'),
(21, 21, 'es', 'BO', 'Bolivia', '+591'),
(22, 22, 'en', 'BA', 'Bosnia and Herzegovina', '+387'),
(23, 23, 'en', 'BW', 'Botswana', '+267'),
(24, 24, 'en', 'BR', 'Brazil', '+55'),
(25, 25, 'en', 'BN', 'Brunei', '+673'),
(26, 26, 'en', 'BG', 'Bulgaria', '+359'),
(27, 27, 'en', 'BF', 'Burkina Faso', '+226'),
(28, 28, 'en', 'BI', 'Burundi', '+257'),
(29, 29, 'en', 'KH', 'Cambodia', '+855'),
(30, 30, 'en', 'CM', 'Cameroon', '+237'),
(31, 31, 'en', 'CA', 'Canada', '+1'),
(32, 32, 'en', 'CV', 'Cape Verde', '+238'),
(33, 33, 'fr', 'CF', 'Central African Republic', '+236'),
(34, 34, 'fr', 'TD', 'Chad', '+235'),
(35, 35, 'es', 'CL', 'Chile', '+56'),
(36, 36, 'en', 'CN', 'China, People''s Republic of', '+86'),
(37, 37, 'es', 'CO', 'Colombia', '+57'),
(38, 38, 'en', 'KM', 'Comoros', '+269'),
(39, 39, 'fr', 'CD', 'Congo, Democratic Republic of the (Congo ? Kinshasa)', '+243'),
(40, 40, 'fr', 'CG', 'Congo, Republic of the (Congo ? Brazzaville)', '+242'),
(41, 41, 'es', 'CR', 'Costa Rica', '+506'),
(42, 42, 'fr', 'CI', 'Cote d''Ivoire (Ivory Coast)', '+225'),
(43, 43, 'en', 'HR', 'Croatia', '+385'),
(44, 44, 'es', 'CU', 'Cuba', '+53'),
(45, 45, 'en', 'CY', 'Cyprus', '+357'),
(46, 46, 'en', 'CZ', 'Czech Republic', '+420'),
(47, 47, 'en', 'DK', 'Denmark', '+45'),
(48, 48, 'fr', 'DJ', 'Djibouti', '+253'),
(49, 49, 'en', 'DM', 'Dominica', '+1-767'),
(50, 50, 'es', 'DO', 'Dominican Republic', '+1-809 and 1-829'),
(51, 51, 'es', 'EC', 'Ecuador', '+593'),
(52, 52, 'en', 'EG', 'Egypt', '+20'),
(53, 53, 'es', 'SV', 'El Salvador', '+503'),
(54, 54, 'en', 'GQ', 'Equatorial Guinea', '+240'),
(55, 55, 'en', 'ER', 'Eritrea', '+291'),
(56, 56, 'en', 'EE', 'Estonia', '+372'),
(57, 57, 'en', 'ET', 'Ethiopia', '+251'),
(58, 58, 'en', 'FJ', 'Fiji', '+679'),
(59, 59, 'en', 'FI', 'Finland', '+358'),
(60, 60, 'fr', 'FR', 'France', '+33'),
(61, 61, 'en', 'GA', 'Gabon', '+241'),
(62, 62, 'en', 'GM', 'Gambia, The', '+220'),
(63, 63, 'en', 'GE', 'Georgia', '+995'),
(64, 64, 'de', 'DE', 'Germany', '+49'),
(65, 65, 'en', 'GH', 'Ghana', '+233'),
(66, 66, 'en', 'GR', 'Greece', '+30'),
(67, 67, 'en', 'GD', 'Grenada', '+1-473'),
(68, 68, 'es', 'GT', 'Guatemala', '+502'),
(69, 69, 'en', 'GN', 'Guinea', '+224'),
(70, 70, 'en', 'GW', 'Guinea-Bissau', '+245'),
(71, 71, 'en', 'GY', 'Guyana', '+592'),
(72, 72, 'fr', 'HT', 'Haiti', '+509'),
(73, 73, 'es', 'HN', 'Honduras', '+504'),
(74, 74, 'en', 'HU', 'Hungary', '+36'),
(75, 75, 'en', 'IS', 'Iceland', '+354'),
(76, 76, 'en', 'IN', 'India', '+91'),
(77, 77, 'en', 'ID', 'Indonesia', '+62'),
(78, 78, 'en', 'IR', 'Iran', '+98'),
(79, 79, 'en', 'IQ', 'Iraq', '+964'),
(80, 80, 'en', 'IE', 'Ireland', '+353'),
(81, 81, 'he', 'IL', 'Israel', '+972'),
(82, 82, 'en', 'IT', 'Italy', '+39'),
(83, 83, 'en', 'JM', 'Jamaica', '+1-876'),
(84, 84, 'en', 'JP', 'Japan', '+81'),
(85, 85, 'en', 'JO', 'Jordan', '+962'),
(86, 86, 'en', 'KZ', 'Kazakhstan', '+7'),
(87, 87, 'en', 'KE', 'Kenya', '+254'),
(88, 88, 'en', 'KI', 'Kiribati', '+686'),
(89, 89, 'en', 'KP', 'Korea, Democratic People''s Republic of (North Korea)', '+850'),
(90, 90, 'en', 'KR', 'Korea, Republic of  (South Korea)', '+82'),
(91, 91, 'en', 'KW', 'Kuwait', '+965'),
(92, 92, 'en', 'KG', 'Kyrgyzstan', '+996'),
(93, 93, 'en', 'LA', 'Laos', '+856'),
(94, 94, 'en', 'LV', 'Latvia', '+371'),
(95, 95, 'en', 'LB', 'Lebanon', '+961'),
(96, 96, 'en', 'LS', 'Lesotho', '+266'),
(97, 97, 'en', 'LR', 'Liberia', '+231'),
(98, 98, 'en', 'LY', 'Libya', '+218'),
(99, 99, 'de', 'LI', 'Liechtenstein', '+423'),
(100, 100, 'en', 'LT', 'Lithuania', '+370'),
(101, 101, 'fr', 'LU', 'Luxembourg', '+352'),
(102, 102, 'en', 'MK', 'Macedonia', '+389'),
(103, 103, 'en', 'MG', 'Madagascar', '+261'),
(104, 104, 'en', 'MW', 'Malawi', '+265'),
(105, 105, 'en', 'MY', 'Malaysia', '+60'),
(106, 106, 'en', 'MV', 'Maldives', '+960'),
(107, 107, 'en', 'ML', 'Mali', '+223'),
(108, 108, 'en', 'MT', 'Malta', '+356'),
(109, 109, 'en', 'MH', 'Marshall Islands', '+692'),
(110, 110, 'en', 'MR', 'Mauritania', '+222'),
(111, 111, 'en', 'MU', 'Mauritius', '+230'),
(112, 112, 'es', 'MX', 'Mexico', '+52'),
(113, 113, 'en', 'FM', 'Micronesia', '+691'),
(114, 114, 'en', 'MD', 'Moldova', '+373'),
(115, 115, 'en', 'MC', 'Monaco', '+377'),
(116, 116, 'en', 'MN', 'Mongolia', '+976'),
(117, 117, 'en', 'ME', 'Montenegro', '+382'),
(118, 118, 'fr', 'MA', 'Morocco', '+212'),
(119, 119, 'en', 'MZ', 'Mozambique', '+258'),
(120, 120, 'en', 'MM', 'Myanmar (Burma)', '+95'),
(121, 121, 'en', 'NA', 'Namibia', '+264'),
(122, 122, 'en', 'NR', 'Nauru', '+674'),
(123, 123, 'en', 'NP', 'Nepal', '+977'),
(124, 124, 'en', 'NL', 'Netherlands', '+31'),
(125, 125, 'en', 'NZ', 'New Zealand', '+64'),
(126, 126, 'es', 'NI', 'Nicaragua', '+505'),
(127, 127, 'en', 'NE', 'Niger', '+227'),
(128, 128, 'en', 'NG', 'Nigeria', '+234'),
(129, 129, 'en', 'NO', 'Norway', '+47'),
(130, 130, 'en', 'OM', 'Oman', '+968'),
(131, 131, 'en', 'PK', 'Pakistan', '+92'),
(132, 132, 'en', 'PW', 'Palau', '+680'),
(133, 133, 'es', 'PA', 'Panama', '+507'),
(134, 134, 'en', 'PG', 'Papua New Guinea', '+675'),
(135, 135, 'es', 'PY', 'Paraguay', '+595'),
(136, 136, 'es', 'PE', 'Peru', '+51'),
(137, 137, 'en', 'PH', 'Philippines', '+63'),
(138, 138, 'en', 'PL', 'Poland', '+48'),
(139, 139, 'en', 'PT', 'Portugal', '+351'),
(140, 140, 'en', 'QA', 'Qatar', '+974'),
(141, 141, 'en', 'RO', 'Romania', '+40'),
(142, 142, 'ru', 'RU', 'Russia', '+7'),
(143, 143, 'en', 'RW', 'Rwanda', '+250'),
(144, 144, 'en', 'KN', 'Saint Kitts and Nevis', '+1-869'),
(145, 145, 'en', 'LC', 'Saint Lucia', '+1-758'),
(146, 146, 'en', 'VC', 'Saint Vincent and the Grenadines', '+1-784'),
(147, 147, 'en', 'WS', 'Samoa', '+685'),
(148, 148, 'en', 'SM', 'San Marino', '+378'),
(149, 149, 'en', 'ST', 'Sao Tome and Principe', '+239'),
(150, 150, 'en', 'SA', 'Saudi Arabia', '+966'),
(151, 151, 'fr', 'SN', 'Senegal', '+221'),
(152, 152, 'en', 'RS', 'Serbia', '+381'),
(153, 153, 'en', 'SC', 'Seychelles', '+248'),
(154, 154, 'en', 'SL', 'Sierra Leone', '+232'),
(155, 155, 'en', 'SG', 'Singapore', '+65'),
(156, 156, 'en', 'SK', 'Slovakia', '+421'),
(157, 157, 'en', 'SI', 'Slovenia', '+386'),
(158, 158, 'en', 'SB', 'Solomon Islands', '+677'),
(159, 159, 'en', 'SO', 'Somalia', '+252'),
(160, 160, 'en', 'ZA', 'South Africa', '+27'),
(161, 161, 'es', 'ES', 'Spain', '+34'),
(162, 162, 'en', 'LK', 'Sri Lanka', '+94'),
(163, 163, 'en', 'SD', 'Sudan', '+249'),
(164, 164, 'en', 'SR', 'Suriname', '+597'),
(165, 165, 'en', 'SZ', 'Swaziland', '+268'),
(166, 166, 'en', 'SE', 'Sweden', '+46'),
(167, 167, 'fr', 'CH', 'Switzerland', '+41'),
(168, 168, 'en', 'SY', 'Syria', '+963'),
(169, 169, 'en', 'TJ', 'Tajikistan', '+992'),
(170, 170, 'en', 'TZ', 'Tanzania', '+255'),
(171, 171, 'en', 'TH', 'Thailand', '+66'),
(172, 172, 'en', 'TL', 'Timor-Leste (East Timor)', '+670'),
(173, 173, 'en', 'TG', 'Togo', '+228'),
(174, 174, 'en', 'TO', 'Tonga', '+676'),
(175, 175, 'en', 'TT', 'Trinidad and Tobago', '+1-868'),
(176, 176, 'fr', 'TN', 'Tunisia', '+216'),
(177, 177, 'en', 'TR', 'Turkey', '+90'),
(178, 178, 'en', 'TM', 'Turkmenistan', '+993'),
(179, 179, 'en', 'TV', 'Tuvalu', '+688'),
(180, 180, 'en', 'UG', 'Uganda', '+256'),
(181, 181, 'ru', 'UA', 'Ukraine', '+380'),
(182, 182, 'en', 'AE', 'United Arab Emirates', '+971'),
(183, 183, 'en', 'GB', 'United Kingdom', '+44'),
(184, 184, 'en', 'US', 'United States', '+1'),
(185, 185, 'es', 'UY', 'Uruguay', '+598'),
(186, 186, 'en', 'UZ', 'Uzbekistan', '+998'),
(187, 187, 'en', 'VU', 'Vanuatu', '+678'),
(188, 188, 'en', 'VA', 'Vatican City', '+379'),
(189, 189, 'es', 'VE', 'Venezuela', '+58'),
(190, 190, 'en', 'VN', 'Viet Nam', '+84'),
(191, 191, 'en', 'YE', 'Yemen', '+967'),
(192, 192, 'en', 'ZM', 'Zambia', '+260'),
(193, 193, 'en', 'ZW', 'Zimbabwe', '+263'),
(195, 195, 'en', 'TW', 'China, Republic of (Taiwan)', '+886'),
(236, 236, 'es', 'PR', 'Puerto Rico', '+1-787 and 1-939'),
(249, 249, 'en', 'HK', 'Hong Kong', '+852'),
(265, 265, 'en', 'PS', 'Palestinian Territories (Gaza Strip and West Bank)', '+970');

/*
INSERT INTO bimar_lang (id, key, esp, eng, orders, p_id, zh, ru) VALUES
(9, 'home', 'Inicio', 'Home', '', 0, ' 首页', 'Home'),
(10, 'home-title', 'Compre y venda Bitcoin con 30 monedas diferentes', 'Buy and Sell Bitcoin Using 30 Different Currencies', '', 9, '购买和出售比特币使用30种不同的货币', 'Покупка и Продажа Биткойн в 30 Различных Валютах'),
(11, 'order-book', 'Libreta de órdenes', 'Order Book', '', 0, '订货簿', 'Книга Заявок'),
(12, 'what-are-bitcoins', '¿Qué son los Bitcoins?', 'What Are Bitcoins?', '', 0, '什么是比特币？', 'Что такое Биткойн ?'),
(13, 'how-bitcoin-works', 'Cómo funciona Bitcoin?', 'How Bitcoin Works', '', 12, '比特币是如何工作的？', 'Как работает Биткойн ?'),
(14, 'trading-bitcoins', 'Compraventa de Bitcoins', 'Trading Bitcoins', '', 12, '贸易比特币', 'Торговля Биткойн'),
(15, 'how-to-register', '¿Cómo funciona?', 'How Does It Work?', '', 0, ' 它是如何工作的？', 'Как это работает ?'),
(16, 'fee-schedule', 'Tarifas', 'Fees', '', 0, ' 费', 'Комиссия'),
(17, 'account', 'Mi Cuenta', 'My Account', '', 0, '我的帐户', 'Мой Аккаунт'),
(18, 'open-orders', 'Órdenes abiertas', 'Open Orders', '', 17, '未结订单', 'Открытые Заявки'),
(19, 'transactions', 'Transacciones', 'Transactions', '', 17, '交易', 'Транзакции'),
(20, 'security', 'Seguridad', 'Security', '', 0, '安全', 'Безопасность'),
(21, 'buy-sell', 'Comprar/vender', 'Buy/Sell', '', 0, '买|卖', 'Покупка/Продажа'),
(22, 'deposit', 'Depositar', 'Deposit', '', 0, '充值', 'Ввод средств'),
(23, 'withdraw', 'Retirar', 'Withdraw', '', 0, '提现', 'Вывод средств'),
(24, 'help', 'Ayuda / FAQ', 'Help / FAQ', '', 0, '帮助/FAQ   ', 'Помощь/ FAQ'),
(25, 'terms', 'Términos de uso', 'Terms of Use', '', 0, '服务条款', 'Условия Использования'),
(26, 'contact', 'Contáctenos', 'Contact Us', '', 0, '联系我们', 'Связаться с Нами'),
(27, 'home-login', 'Login', 'Login', '', 0, ' 登录    ', 'Вход'),
(28, 'home-register', 'Registrarse', 'Register', '', 9, '注册 ', 'Регистрация'),
(29, 'home-trading', '¿Preocupado por seguridad? Lea acerca de las medidas que tomamos.', 'Concerned about security? Read more about our security measures.', '', 9, '担心安全  吗?了解更多关于我们的安全措施', 'Беспокоитесь о безопасности? Читайте больше о наших мерах безопасности.'),
(30, 'home-more', 'Leer Más', 'Read More', '', 9, '阅读更多', 'Читать больше'),
(31, 'home-bitcoin-market', 'Resumen de Mercado <strong>Bitcoin</strong>', 'Bitcoin Market <strong>Overview</strong>', '', 9, '比特币市场 <strong>概述</strong>', 'Обзор Рынка<strong>Биткойн</strong>'),
(32, 'home-bitcoin-market-explain', 'Estadí­sticas clave sobre el mercado de compraventa de Bitcoin', 'Key statistics about the Bitcoin trading market', '', 9, '关于比特币交易市场的关键统计数据', 'Основные статистические данные о рынке Биткойн'),
(33, 'home-stats-last-price', 'Último precio', 'Last Price', '', 9, '最新成交价', 'Текущий курс'),
(34, 'home-stats-daily-change', 'Cambio diario', 'Daily Change', '', 9, '日变化', 'Изменения за День'),
(35, 'home-stats-days-range', 'Rango del dia', 'Day''s Range', '', 9, '今日区间', 'Диапазон Дня'),
(36, 'home-stats-todays-open', 'Apertura hoy', 'Today''s Open', '', 9, '今日开', 'Цена Открытия'),
(37, 'home-stats-24h-volume', 'Volumen 24h', '24h Volume', '', 9, '24小时的成交量', 'Суточный Объем'),
(38, 'home-stats-market-cap', 'Capit. mercado', 'Market Cap', '', 9, '市值', 'Рыночная Кап.'),
(39, 'home-stats-total-btc', 'BTC totales', 'Total BTC', '', 9, '总BTC', ' Всего ВТС'),
(40, 'home-stats-global-volume', 'Volumen global', 'Global Volume', '', 9, '总体体积', 'Общий объем'),
(41, 'javascript-date-format', '%e/%m/%y', '%m/%e/%y', '', 42, '%y/%m/%e', '%e/%m/%y'),
(42, 'dates', 'Fechas', 'Dates', '', 0, '日期', 'Даты'),
(43, 'sun', 'Dom', 'Sun', '', 42, '星期日', 'Вскр'),
(44, 'mon', 'Lun', 'Mon', '', 42, '星期一', 'Пн'),
(45, 'tue', 'Mar', 'Tue', '', 42, '星期二', 'Вт'),
(46, 'wed', 'Mié', 'Wed', '', 42, '星期三', 'Ср'),
(47, 'thu', 'Jue', 'Thu', '', 42, '星期四', 'Чт'),
(48, 'fri', 'Vie', 'Fri', '', 42, '星期五', 'Пт'),
(49, 'sat', 'Sáb', 'Sat', '', 42, '星期六', 'Сб'),
(50, 'jan', 'Ene', 'Jan', '', 42, '一月', 'Янв'),
(51, 'feb', 'Feb', 'Feb', '', 42, '二月', 'Фев'),
(52, 'mar', 'Mar', 'Mar', '', 42, '三月', 'Мрт'),
(53, 'apr', 'Abr', 'Apr', '', 42, '四月', 'Апр'),
(54, 'may', 'May', 'May', '', 42, '五月', 'Май'),
(55, 'jun', 'Jun', 'Jun', '', 42, '六月', 'Июн'),
(56, 'jul', 'Jul', 'Jul', '', 42, '七月', 'Июл'),
(57, 'aug', 'Ago', 'Aug', '', 42, '八月', 'Авг'),
(58, 'sep', 'Sep', 'Sep', '', 42, '九月', 'Сент'),
(59, 'oct', 'Oct', 'Oct', '', 42, '十月', 'Окт'),
(60, 'nov', 'Nov', 'Nov', '', 42, '十一月', 'Нояб'),
(61, 'dec', 'Dic', 'Dec', '', 42, '十二月', 'Дек'),
(62, 'date-format', 'j/n/y', 'n/j/y', '', 42, 'y/n/j', 'j/n/y'),
(63, 'transactions-time-since', 'Efectuado hace', 'Time Since', '', 17, '由于时间', 'Прошло Времени'),
(64, 'transactions-amount', 'Cantidad', 'Amount', '', 17, '金额', 'Количество'),
(65, 'transactions-price', 'Precio', 'Price', '', 17, '价格', 'Цена'),
(67, 'home-live-trades', 'Últimas transacciones', 'Latest Trades', '', 9, '最新的交易', 'Последние Сделки'),
(68, 'home-live-orders', 'Órdenes abiertas', 'Open Orders', '', 9, '未结订单', 'Открытые Заявки'),
(69, 'currency', 'Moneda', 'Currency', '', 9, '货币', 'Валюта'),
(70, 'transactions-no', 'No hay transacciones.', 'No transactions to show.', '', 17, '没有交易。', 'Сделок нет.'),
(71, 'orders-bid', 'Oferta', 'Bid', '', 11, '委买', 'Покупка'),
(72, 'orders-ask', 'Demanda', 'Ask', '', 11, '委卖', 'Продажа'),
(73, 'orders-no-bid', 'No hay ofertas.', 'No bids to show.', '', 11, '没有委买', 'Предложений нет.'),
(74, 'orders-no-ask', 'No hay demanda.', 'No asks to show.', '', 11, '没有委卖', 'Спроса нет.'),
(75, 'news', 'Noticias', 'News', '', 0, '新闻', 'Новости'),
(76, 'news-latest', 'Últimas <strong>noticias</strong>', 'Latest <strong>News</strong>', '', 75, '最新<strong>消息</strong>', 'Последние <strong>Новости</strong>'),
(77, 'news-explain', 'Noticias relevantes sobre Bitcoin y este sitio en particular.', 'Relevant news about Bitcoin and this site in particular.', '', 75, '关于比特币和这个网站相关的新闻。', 'Освещение новостей Биткойн и нашего сайта в частности. '),
(82, 'home-basic-nav', 'Navegación', 'Navigation', '', 9, '导航', 'Навигация'),
(83, 'home-about-bitcoin', 'Info. sobre Bitcoin', 'Bitcoin Info', '', 9, '关于比特币的信息', 'Информация о Биткойн'),
(84, 'home-account-functions', 'Mi Cuenta', 'Account', '', 9, '账户', 'Аккаунт'),
(85, 'news-see-all', 'Ver más noticias', 'Read more news', '', 75, '多读一点新闻', 'Читать больше новостей'),
(86, 'order-book-see', 'Ver libreta de órdenes', 'Go to order book', '', 9, '查看订货簿', 'Просмотреть  книгу заявок'),
(87, 'orders-price', 'Precio', 'Price', '', 11, '价格', 'Цена'),
(88, 'orders-amount', 'Cantidad', 'Amount', '', 11, '金额', 'Количество'),
(89, 'orders-value', 'Valor', 'Value', '', 11, '值', 'Значение'),
(90, 'forgot-ask', '¿Olvidó su información de login?', 'Forgot your login information?', '', 27, '忘记登录信息？', 'Забыли данные для входа ?'),
(91, 'login-dont-have', '¿No tiene cuenta? Regístrese aquí.', 'Don''t have an account? Register here.', '', 27, '还没有账号？在这里注册。', 'Нет аккаунта ? Зарегистрируйтесь здесь.'),
(92, 'login-user-empty-error', 'Debe ingresar un usuario.', 'You must provide a username.', '', 27, '您必须输入用户名。', 'Вы должны ввести имя пользователя.'),
(93, 'login-password-empty-error', 'Debe ingresar una contraseña.', 'You must provide a password.', '', 27, '您必须输入用密码。', 'Вы должны ввести пароль.'),
(94, 'login-invalid-login-error', 'Información de login inválido.', 'Invalid login information.', '', 27, '登录信息无效。', 'Неправильная информация входа.'),
(95, 'login-forgot', 'Olvidó contraseña', 'Forgot Password', '', 27, '忘了密码', 'Забыли Пароль'),
(96, 'forgot-explain', '¿Olvidó su información de login? Ingrese su email para reestablecer su contraseña.', 'Forgot your login information? Enter your email to reset your password.', '', 27, '忘记了您的登录信息吗? 输入您的电邮地址重置设定您的密码。', 'Забыли свои учетные данные ? Введите адрес электронной почты для сброса пароля.'),
(97, 'login-forgot-send-new', 'Generar solicitud', 'Generate Request', '', 27, '生成请求', 'Создать запрос'),
(98, 'login-remembered', '¿Recordó su info? Pulse aquí para regresar a login.', 'Remembered your info? Click here to return to login.', '', 27, '记住您的信息？点击此处返回登录。', 'Вспомнили свои данные? Нажмите здесь, чтобы вернуться к авторизации.'),
(99, 'email-send-error', 'Ocurrió un error y el email no se pudo enviar. Por favor inténtelo más tarde.', 'An error occurred and the email could not be sent. Please try again later.', '', 27, '发生错误，电子邮件无法发送。请稍后再试。', 'Произошла ошибка и письмо не может быть отправлено. Пожалуйста, повторите попытку позже.'),
(100, 'email-address-error', 'Email inválido.', 'Invalid email address.', '', 27, '电子邮件地址无效。', 'Неверный адрес электронной почты.'),
(101, 'email-sent-message', 'El email fué enviado exitosamente.', 'The email was sent successfully.', '', 27, '电子邮件已成功发送。', 'Письмо было успешно отправлено.'),
(102, 'login-password-sent-message', 'Se le ha enviado una solicitud para reestablecer su contraseña. Por favor revise su email.', 'You have been sent a password reset request. Please check your email.', '', 27, '您已发送密码重置请求。请检查您的电子邮件。', 'Вы отправили запрос на сброс пароля. Пожалуйста, проверьте вашу электронную почту.'),
(103, 'login-account-not-found', 'No existe ninguna cuenta vinculada al email que especificó.', 'There is no account for the email address you have specified.', '', 27, '没有与电子邮件地址相关联的帐户。', 'Не существует аккаунта связанного с указанным адресом электронной почты.'),
(104, 'settings', 'Preferencias', 'Settings', '', 0, '设置', 'Настройки'),
(105, 'account-nav', 'Navegación de cuenta', 'Account Navigation', '', 17, '帐户导航', 'Навигация по Аккаунту'),
(106, 'account-welcome', 'Bienvenido', 'Welcome', '', 17, '欢迎光临', 'Добро пожаловать'),
(107, 'account-balance', 'Balances disponibles', 'Available Balances', '', 17, '可用余额', 'Доступные Остатки'),
(109, 'account-on-hold', 'Balances retenidos', 'Balances On Hold', '', 17, '保留余额', 'Остатки на Удержании'),
(110, 'account-available', 'disponible', 'available', '', 17, '可用的', 'Доступно'),
(111, 'account-on-order', 'en órdenes abiertas', 'in open orders', '', 17, '在未结订单', 'в открытых заявках'),
(112, 'account-on-widthdrawal', 'esperando retiro', 'waiting for withdrawal', '', 17, '等候货币提现', 'в ожидании вывода'),
(113, 'account-fee-structure', 'Nivel de tarifa y volumen', 'Fee Level and Volume', '', 17, '收费水平及成交量', 'Уровень Комиссии и Объем'),
(114, 'account-fee-bracket', 'Tasa de comisión (tomador)', 'Commission rate (taker)', '', 17, '佣金率（接受者）', 'Комиссионная ставка (берущий)'),
(115, 'account-30-day-vol', 'Volumen en USD a 30 dias', '30 day volume in USD', '', 17, '30天内美元量', '30 дневный объем в USD'),
(116, 'account-view-fee-schedule', 'Ver tabla de tarifas', 'View fee schedule', '', 17, '查看费用说明', 'Посмотреть прейскурант комиссионных сборов'),
(117, 'orders-edit', 'Editar esta órden', 'Edit this order', '', 9, '编辑这个订单', 'Редактировать эту заявку'),
(118, 'orders-filter-currency', 'Filtrar por moneda', 'Filter by currency', '', 17, '按币种过滤器', 'Фильтровать по валюте'),
(119, 'orders-order-by', 'Ordenar por', 'Order By', '', 17, '订单', 'Заявка '),
(120, 'orders-order-by-btc-price', 'Precio BTC', 'BTC Price', '', 17, 'BTC 价格', 'Цена BTC'),
(121, 'orders-order-by-date', 'Fecha/hora de órden', 'Order date/time', '', 17, '订单日期/时间', 'Дата/время заявки'),
(122, 'orders-order-by-fiat', 'Valor de la Orden', 'Order Value', '', 17, ' 订单值', 'Стоимость Заявки'),
(123, 'transactions-type', 'Tipo', 'Type', '', 17, '类型', 'Тип'),
(124, 'transactions-time', 'Hora y Fecha', 'Date and Time', '', 17, '日期和时间', 'Дата и Время'),
(125, 'transactions-btc', 'Monto BTC', 'BTC Amount', '', 17, '比特币金额', 'Количество BTC'),
(126, 'transactions-fiat', 'Valor', 'Value', '', 17, '值', 'Значение'),
(127, 'transactions-price', 'Precio BTC', 'BTC Price', '', 17, 'BTC 价格', 'Цена BTC'),
(128, 'transactions-fee', 'Comisión', 'Fee', '', 17, '费用', 'Комиссия'),
(129, 'transactions-any', 'Cualquiera', 'Any', '', 17, '别的', 'Прочее'),
(130, 'transactions-pagination', 'Mostrando [results] transacciones en [num_pages] páginas.', 'Showing [results] transactions on [num_pages] pages.', '', 17, '显示[results] 交易在[num_pages]页面上', 'Отображение [results] сделок на [num_pages] страницах.'),
(131, 'orders-bid-top-10', '10 mejores ofertas', 'Top 10 Bids', '', 17, '排名前10的委买', '10 Лучших Предложений'),
(132, 'orders-ask-top-10', '10 mejores ventas', 'Top 10 Sellers', '', 17, '排名前10的卖家', '10 Лучших Продаж'),
(133, 'buy-bitcoins', 'Comprar Bitcoin', 'Buy Bitcoin', '', 17, '买比特币', 'Покупка Биткойнов'),
(134, 'sell-bitcoins', 'Vender Bitcoin', 'Sell Bitcoin', '', 17, '卖比特币', 'Продажа Биткойн'),
(135, 'click-to-start', 'Pulsar para iniciar', 'Click to start', '', 17, '点击启', 'Нажмите, чтобы начать'),
(136, 'buy-amount', 'Cantidad a comprar', 'Amount to Buy', '', 17, '购买金额', 'Количество для Покупки'),
(137, 'buy-with-currency', 'Moneda a utilizar', 'Currency to use', '', 17, '使用货币', 'Использовать валюту'),
(138, 'buy-price', 'Precio', 'Price', '', 17, '成交价', 'Цена'),
(139, 'sell-amount', 'Cantidad a vender', 'Amount to Sell', '', 17, '购卖金额', 'Количество для Продажи'),
(140, 'buy-subtotal', 'Subtotal', 'Subtotal', '', 17, '小计', 'Промежуточный итог'),
(141, 'buy-fee', 'Comisión', 'Fee', '', 17, '费用', 'Комиссия'),
(142, 'buy-total', 'BTC a recibir', 'BTC to Receive', '', 17, '以接收BTC', 'Получить BTC'),
(143, 'sell-total', '[currency] total a recibir', '[currency] to receive', '', 17, '以接收[currency]', 'получить [currency] '),
(144, 'buy-market-price', 'Comprar a precio del mercado', 'Buy at market price', '', 17, '购买市场价格', 'Купить по рыночной цене'),
(145, 'sell-market-price', 'Vender a precio del mercado', 'Sell at market price', '', 17, '购卖市场价格', 'Продать по рыночной цене'),
(146, 'buy-market-rates-info', 'Pulse para más información.', 'Click for info on how this works.', '', 17, '点击了解更多信息', 'Нажмите, чтобы получить больше информации'),
(147, 'buy-total-approx', '[currency] approx a gastar', 'Approx. [currency] to spend', '', 17, '大约花[currency] ', 'Приблизительно [currency] потратить '),
(148, 'sell-total-approx', '[currency] approx a recibir', 'Approx [currency] to receive', '', 17, '大约接受[currency]', 'Приблизительно [currency] получить'),
(149, 'buy-errors-no-amount', 'Debe comprar una cantidad mayor a cero.', 'You must buy an amount greater than zero.', '', 17, '购买金额必须是大于0的整数。', 'Вы должны купить в количестве больше нуля. '),
(150, 'buy-errors-no-price', 'Debe especificar el precio al que desea comprar.', 'You must specify a price at which to buy.', '', 17, '您必须指定您想要购买的价格。', 'Вы должны указать цену, по которой хотите купить.'),
(151, 'sell-errors-no-price', 'Debe especificar el precio al que desea vender.', 'You must specify a price at which to sell.', '', 17, '您必须指定你想卖的价格。', 'Вы должны указать цену, по которой хотите продать.'),
(152, 'buy-errors-no-currency', 'Debe escoger una moneda.', 'No currency specified.', '', 17, '没有指定一种货币', 'Никакая валюта не указана.'),
(153, 'buy-errors-balance-too-low', 'No tiene suficiente de la moneda especificada.', 'You do not have enough of the specified currency.', '', 17, '您没有足够的这种货币。', 'У вас нет достаточного количества указанной валюты.'),
(154, 'sell-errors-balance-too-low', 'No tiene suficientes Bitcoins.', 'You do not have enough Bitcoins.', '', 17, '您没有足够的金额比特币。', 'У вас нет достаточного количества биткоинов.'),
(155, 'log-out', 'Logout', 'Logout', '', 9, '注销', 'Выйти'),
(156, 'account-nothing-on-hold', 'No tiene órdenes ni solicitudes de retiro abiertas.', 'You have no open orders or withdrawal requests.', '', 17, '您没有未结订单或取款请求。', 'У вас нет открытых заявок или запросов на вывод.'),
(157, 'buy-fiat-available', '[currency] disponibles', 'Available [currency]', '', 17, '可用的[currency]', 'Доступные [currency]'),
(158, 'sell-btc-available', 'BTC disponibles', 'Available BTC', '', 17, '可用的BTC', 'Доступные BTC'),
(159, 'transactions-done-message', 'Se realizaron [transactions] nuevas transacciones.', 'You have made [transactions] new transactions.', '', 17, '您已经做了[transactions]新交易。', 'Вы совершили [transactions] новые транзакции.'),
(160, 'transactions-orders-done-message', 'Se ha creado una nueva órden y [transactions] nuevas transacciones.', 'You have created a new order and realized [transactions] new transactions.', '', 17, '您已经创建了一个新的订单和实现了[transactions]新的交易。', 'Вы создали новую заявку и совершили [transactions] новые транзакции. '),
(161, 'edit-order', 'Editar órden', 'Edit Order', '', 17, '编辑订单', 'Редактировать Заявку'),
(162, 'transactions-orders-new-message', 'Se ha creado una nueva órden.', 'You have created a new order.', '', 17, '您已经创建了一个新的订单。', 'Вы создали новую заявку.'),
(163, 'transactions-orders-edit-message', 'Su órden fué editada exitósamente.', 'Your order has been edited successfully.', '', 17, '您的订单已成功编辑。', 'Ваша заявка была отредактирована успешно.'),
(164, 'transactions-orders-done-edit-message', 'Se ha editado una órden y efectuado [transactions] nuevas transacciones.', 'You have edited your order and realized [transactions] new transactions.', '', 17, '您已经编辑了一个新的订单和实现了[transactions]新的交易。', 'Вы отредактировали заявку и совершили [transactions] новые транзакции.'),
(165, 'home-your-order', 'Usted es el autor de ésta órden', 'You made this order', '', 9, '你做这个订单', 'Вы подали эту заявку'),
(166, 'home-overview', 'Resúmen', 'Overview', '', 9, '概观', 'Обзор'),
(167, 'security-enable-two-factor', 'Habilitar autenticación de dos factores', 'Enable Two-Factor Authentication', '', 20, '启用双因素身份验证。', 'Включить двухфакторную аутентификацию'),
(168, 'security-country', 'Su país', 'Your Country', '', 20, '您的国家', 'Ваша Страна'),
(169, 'security-cell', 'Su num. celular', 'Your Cellphone #', '', 20, '您的手机号码', 'Ваш номер мобильного телефона'),
(170, 'security-enable', 'Habilitar Google Authenticator', 'Enable 2FA', '', 20, '启用 2FA', 'Включить 2FA'),
(171, 'security-no-cell', 'Debe ingresar un número de celular.', 'You must provide a cellphone number.', '', 20, '需要您提供一个手机号码。', 'Необходимо ввести номер мобильного телефона.'),
(172, 'security-no-cc', 'Debe seleccionar un paí­s.', 'You must select a country.', '', 20, '需要您选择一个国家。', 'Необходимо выбрать страну.'),
(173, 'security-com-error', 'No nos pudimos comunicar con el servidor de Authy. Por favor inténtelo más tarde.', 'The Authy server was unreachable. Please try again later.', '', 20, 'Authy服务器是无法访问。请稍后再试。', 'Сервер Authy недоступен. Пожалуйста, повторите попытку позже.'),
(174, 'security-enter-token', 'Ingresar código', 'Enter Token', '', 20, '输入令牌', 'Введите код'),
(175, 'security-token', 'Su código', 'Your Token', '', 20, '您的令牌', 'Ваш код'),
(176, 'security-validate', 'Validar', 'Validate', '', 20, '批准', 'Подтвердить'),
(177, 'security-no-token', 'Por favor ingrese el pin desde su aplicación de 2FA.', 'Please enter the token from your 2FA app.', '', 20, '请输入令牌从您的应用程序2FA。', 'Пожалуйста, введите код вашего приложения 2FA.'),
(178, 'security-success-message', 'Se ha configurado autenticación de dos factores exitósamente.', 'Congratulations. You are now set up to use two-factor authentication.', '', 20, '恭喜。您现在设置为使用双因素认证。', 'Поздравляем. Настройка двухфакторной аутентификации прошла успешно.'),
(179, 'security-verified', 'Verificado', 'Verified', '', 20, '验证', 'Проверено'),
(180, 'security-change-number', 'Cambiar de número', 'Change Number', '', 20, '更改号码', 'Изменить Номер'),
(181, 'security-send-sms', 'Enviar por SMS', 'Send By SMS', '', 20, '发送短信', 'Отправить сообщением (SMS)'),
(182, 'security-dont-ask', 'No me pidan esto por 30 di­as', 'Don''t ask me for this for 30 days', '', 20, '在30天内不要问我这个', 'Не спрашивать меня об этом в течение 30 дней'),
(183, 'verify-token', 'Autenticación', 'Authentication', '', 20, '身份验证', 'Аутентификация'),
(184, 'login-required-error', 'es un campo requerido.', 'is a required field.', '', 104, '必填字段。', 'это обязательное поле.'),
(185, 'login-email-error', 'Debe ingresar un email válido.', 'You must enter a valid email.', '', 104, '您必须输入一个有效的电子邮件地址。', 'Вы должны ввести действительный адрес электронной почты.'),
(186, 'login-password-error', 'Su contraseña debe tener al menos 8 caracteres. La mayorÃ­a de los caracteres especiale son permitidos.', 'Your password must have at least 8 characters. Most special chars are permitted.', '', 104, '您的密码必须至少有8个字符。允许大部分特别的字符。', 'Ваш пароль должен содержать как минимум 8 символов. Большинство специальных символов допускаются.'),
(187, 'settings-personal-info', 'Información personal', 'Personal Info', '', 104, '个人信息', 'Личная Информация'),
(188, 'settings-pass', 'Cambiar Contraseña', 'Change Password', '', 104, '修改密码', 'Изменить Пароль'),
(189, 'settings-pass-confirm', 'Confirmar Contraseña', 'Confirm Password', '', 104, '确认密码', 'Подтвердить Пароль'),
(190, 'settings-first-name', 'Nombre', 'First Name', '', 104, '名字', 'Имя'),
(191, 'settings-last-name', 'Apellido', 'Last Name', '', 104, '姓名', 'Фамилия'),
(192, 'settings-country', 'País', 'Country', '', 104, '国家', 'Страна'),
(193, 'settings-email', 'Email', 'Email', '', 104, '电子邮箱', 'Email'),
(194, 'settings-save-info', 'Guardar info', 'Save Info', '', 104, '保存信息', 'Сохранить Информацию'),
(195, 'settings-personal-message', 'Su información personal ha sido actualizada.', 'Your personal info has been updated.', '', 104, '您的个人信息已被更新。', 'Ваша личная информация была обновлена.'),
(196, 'settings-withdrawal-2fa-btc', 'Confirmar retiros de BTC con autenticación de dos factores.', 'Confirm BTC withdrawals with two-factor authentication.', '', 104, '确认取款BTC使用双因素认证。', 'Подтверждать вывод BTC с помощью двухфакторной аутентификации.'),
(197, 'settings-withdrawal-email-btc', 'Confirmar retiros de BTC por email.', 'Confirm BTC withdrawals by email.', '', 104, '确认取款BTC使用电子邮件。', 'Подтверждать выводы BTC по электронной почте.'),
(198, 'settings-withdrawal-2fa-bank', 'Confirmar retiros bancarios con autenticación de dos factores.', 'Confirm bank withdrawals with two-factor authentication.', '', 104, '确认银行取钱使用双因素认证。', 'Подтверждать банковские переводы с помощью двухфакторной аутентификации.'),
(199, 'settings-withdrawal-email-bank', 'Confirmar retiros bancarios por email.', 'Confirm bank withdrawals by email.', '', 104, '确认银行取钱使用电子邮件。', 'Подтверждать банковские переводы по электронной почте.'),
(200, 'settings-notify-deposit-btc', 'Notificar por email cuando se deposita BTC.', 'Notify by email when BTC deposits are made.', '', 104, '比特币存款时, 通过电子邮件通知。', 'Уведомлять по электронной почте, когда депозиты BTC сделаны.'),
(201, 'settings-notify-deposit-bank', 'Notificar por email cuando se deposita por banca.', 'Notify by email when bank deposits are made.', '', 104, '银行存款时, 通过电子邮件通知。', 'Уведомлять по электронной почте, когда банковские депозиты сделаны.'),
(202, 'settings-conf', 'Seguridad y notificaciones', 'Security and Notifications', '', 104, '安全和通知', 'Безопасность и Уведомления'),
(203, 'settings-save-settings', 'Guardar preferencias', 'Save Settings', '', 104, '保存设置', 'Сохранить Настройки'),
(204, 'fee-schedule-fee', 'Comisión de tomador (taker)', 'Taker Fee', '', 16, '', ''),
(205, 'fee-schedule-volume', 'Volumen a 30 di­as en', '30 day volume in', '', 16, '30天内量', '30 дневный объем в'),
(206, 'contact-inquiries', 'Para consultas generales', 'For General Inquiries', '', 26, '一般查询', 'По Общим Вопросам'),
(207, 'settings-company', 'Compañí­a', 'Company', '', 26, '公司', 'Компания'),
(208, 'settings-subject', 'Tema', 'Subject', '', 26, '主题', 'Тема'),
(209, 'settings-message', 'Su mensaje', 'Your Message', '', 26, '您的留言', 'Ваше Сообщение'),
(210, 'settings-capcha', 'Por favor copiar el texto de verificación', 'Please copy the verification text', '', 26, '请复制验证文', 'Пожалуйста, скопируйте проверочный текст'),
(211, 'contact-send', 'Enviar mensaje', 'Send Message', '', 26, '发信', 'Отправить Сообщение'),
(212, 'login-capcha-error', 'El texto de verificación no concuerda con la imágen.', 'The verification text does not match the image.', '', 26, '验证字和图像不匹配。', 'Проверочный текст не совпадает с изображением.'),
(213, 'email-send-error', 'Ocurrió un error y el email no se pudo enviar. Por favor inténtelo más tarde.', 'An error occurred and your message could not be sent. Please try again later.', '', 26, '发生了错误，您的消息无法发送。请稍后再试。', 'Произошла ошибка, и ваше сообщение не может быть отправлено. Пожалуйста, повторите попытку позже.'),
(214, 'contact-message', 'Gracias por contactarnos. Le responderemos lo más rápido posible.', 'Thank you for getting in contact with us. We will respond to your message as soon as possible.', '', 26, '感谢您与我们联系。我们会尽快回复您的邮件。', 'Спасибо, что связались с нами. Мы ответим на ваше сообщение как можно скорее.'),
(215, 'bank-accounts', 'Cuentas Bancarias', 'Bank Accounts', '', 0, '银行账户', 'Банковские Счета'),
(216, 'bank-accounts-add', 'Agregar Cuenta', 'Add Account', '', 215, '添加帐户', 'Добавить Аккаунт'),
(217, 'bank-accounts-account', 'Num. de cuenta', 'Account Num.', '', 215, '帐号', 'Номер Счета'),
(218, 'bank-accounts-description', 'Descripción', 'Description', '', 215, '说明', 'Описание'),
(219, 'bank-accounts-remove', 'Eliminar', 'Remove', '', 215, '拆除', 'Удалить'),
(220, 'bank-accounts-no', 'No hay cuentas bancarias asociadas a su perfil.', 'There are no bank accounts associated with your profile.', '', 215, '没有与您的个人配置文件相关联的银行账户。', 'Нет банковских счетов, связанных с вашим профилем.'),
(221, 'bank-accounts-add-label', 'Agregar cuenta Crypto Capital', 'Add Crypto Capital Account', '', 215, '添加Crypto Capital帐户', 'Добавить Crypto Capital Аккаунт'),
(222, 'bank-accounts-account-cc', 'Número de cuenta Crypto Capital', 'Crypto Capital Account Number', '', 215, 'Crypto Capital 帐户号码', 'Номер Аккаунта Crypto Capital '),
(223, 'bank-accounts-add-account', 'Afiliar Cuenta', 'Add Account', '', 215, '添加帐户', 'Добавить Аккаунт'),
(224, 'bank-accounts-already-exists', 'La cuenta especificada ya está asociada con su perfil.', 'That account is already associated with your profile.', '', 215, '这个帐户已经与您的个人配置文件相关联。', 'Эта учетная запись уже связана с вашим профилем.'),
(225, 'bank-accounts-invalid-number', 'Número de cuenta inválido.', 'Invalid account number.', '', 215, '无效账户号码。', 'Неверный номер счета.'),
(226, 'bank-accounts-crypto-label', 'Cuenta Crypto Capital', 'Crypto Capital Account', '', 215, 'Crypto Capital帐户', 'Crypto Capital Аккаунт'),
(227, 'bank-accounts-added-message', 'La cuenta ha sido afiliada exitósamente.', 'The account has been added successfully.', '', 215, '帐户添加成功。', 'Учетная запись была успешно добавлена.'),
(228, 'bank-accounts-remove-error', 'La cuenta especificada no está asociada con su perfil.', 'The specified account is not associated with your profile.', '', 215, '指定的帐户没有与您的个人配置文件相关联。', 'Указанная учетная запись не связана с вашим профилем.'),
(229, 'bank-accounts-removed-message', 'La cuenta ha sido eliminada.', 'The account was removed successfully.', '', 215, '帐户删除成功。', 'Аккаунт был успешно удален.'),
(230, 'account-functions', 'Funciones', 'Account Functions', '', 17, '帐户功能', 'Функции'),
(231, 'bitcoin-addresses', 'Direcciones Bitcoin', 'Bitcoin Addresses', '', 0, '比特币地址', 'Биткойн-Адреса'),
(232, 'bitcoin-addresses-add', 'Obtener nueva dirección', 'Get New Address', '', 231, '获取新的地址', 'Получить Новый Адрес'),
(233, 'bitcoin-addresses-date', 'Fecha y hora', 'Date and Time', '', 231, '日期和时间', 'Дата и Время'),
(234, 'bitcoin-addresses-address', 'Dirección', 'Address', '', 231, '地址', 'Адрес'),
(235, 'bitcoin-addresses-no', 'No tiene direcciones Bitcoin. Por favor genere uno nuevo.', 'You have no Bitcoin addresses. Please generate a new one.', '', 231, '您没有比特币地址。请生成一个新的。', 'У вас нет Биткойн адресов. Пожалуйста, создайте новый.'),
(236, 'bitcoin-addresses-added', 'Una nueva dirección fue agregada.', 'A new address was added.', '', 231, '一个新的地址添加了。', 'Новый адрес был добавлен.'),
(237, 'bitcoin-addresses-too-soon', 'Sólo se puede agregar una nueva dirección de Bitcoin cada 24 horas.', 'You can only add one new Bitcoin address every 24 hours.', '', 231, '您只能添加一个新的比特币地址每24小时。', 'Вы можете добавить только один новый Биткойн-адрес за каждые 24 часа.'),
(238, 'bank-accounts-no-currency', 'Debe especificar la moneda que utiliza la cuenta.', 'You must specify the account currency.', '', 215, '您必须指定账户货币。', 'Вы должны указать валюту счета.'),
(239, 'bank-accounts-currency', 'Moneda', 'Currency', '', 215, '货币', 'Валюта'),
(240, 'bank-accounts-already-associated', 'La cuenta especificada ya está asociada con otro perfil.', 'That account is already associated with another profile.', '', 215, '此帐户已与其他配置文件相关联。', 'Эта учетная запись уже связана с другим профилем.'),
(241, 'deposit-bitcoins', 'Depositar bitcoins', 'Deposit Bitcoins', '', 22, '比特币充值', 'Ввод Биткоинов'),
(242, 'deposit-send-to-address', 'Enví­e bitcoins a esta dirección', 'Send Bitcoins to This Address', '', 22, '发送比特币到这个地址', 'Отправить биткоины по этому адресу'),
(243, 'deposit-manage-addresses', 'Administrar direcciones Bitcoin', 'Manage Bitcoin addresses', '', 22, '管理比特币地址', 'Управление Биткоин-адресами'),
(244, 'deposit-fiat-instructions', 'Depositar monedas fiduciarias', 'Deposit Fiat Currency', '', 22, '菲亚特充值', 'Ввод Фиатных Валют'),
(245, 'deposit-fiat-account', 'Cuenta bancaria para utilizar', 'Bank Account to Use', '', 22, '您用的银行账户', 'Используемый Банковский Счет'),
(246, 'deposit-manage-bank-accounts', 'Administrar cuentas bancarias', 'Manage bank accounts', '', 22, '管理银行账户', 'Управление банковскими счетами'),
(247, 'deposit-recent', 'Depósitos recientes', 'Recent Deposits', '', 22, '最后的充值', 'Последние Вводы'),
(248, 'deposit-date', 'Fecha y hora', 'Date and Time', '', 22, '日期和时间', 'Дата и Время'),
(249, 'deposit-description', 'Descripción', 'Description', '', 22, '描述', 'Описание'),
(250, 'deposit-amount', 'Cantidad', 'Amount', '', 22, '金额', 'Количество'),
(251, 'deposit-status', 'Estatus', 'Status', '', 22, '状态', 'Статус'),
(252, 'withdraw-bitcoins', 'Retirar bitcoins', 'Withdraw Bitcoins', '', 23, '比特币提现', 'Вывод Биткоинов'),
(253, 'withdraw-send-to-address', 'Dirección a enviar', 'Send to Address', '', 23, '发送到地址', 'Адрес для отправки'),
(254, 'withdraw-send-amount', 'Cantidad a enviar', 'Amount to Send', '', 23, '发送总和', 'Сумма к отправке'),
(255, 'withdraw-send-bitcoins', 'Enviar bitcoins', 'Send Bitcoins', '', 23, '发送比特币', 'Отправка биткоинов'),
(256, 'withdraw-fiat', 'Retirar moneda fiduciaria', 'Withdraw Fiat Currency', '', 23, '菲亚特货币提现', 'Вывод Фиатных Валют'),
(257, 'withdraw-fiat-account', 'Cuenta bancaria receptora', 'Receiving Bank Account', '', 23, '获得一个银行账户', 'Получение Банковского Счета'),
(258, 'withdraw-currency', 'Moneda a retirar', 'Withdrawal Currency', '', 23, '货币提现', 'Вывод Валют'),
(259, 'withdraw-amount', 'Cantidad a retirar', 'Amount to Withdraw', '', 23, '提现总和', 'Сумма к Выводу'),
(260, 'withdraw-withdraw', 'Retirar dinero', 'Withdraw Currency', '', 23, '货币提现', 'Вывод Валюты'),
(261, 'deposit-no', 'No se han hecho depósitos a esta cuenta.', 'You have not made any deposits.', '', 22, '您还没有作出任何存款。', 'Вы не сделали никаких вводов.'),
(262, 'withdraw-no', 'No se han efectuado retiros desde esta cuenta.', 'No withdrawals have been made.', '', 23, '取款无已。', 'Вывода средств произведено не было. '),
(263, 'withdraw-no-account', 'Debe especificar a que cuenta desea retirar.', 'You must specify the withdrawal account.', '', 23, '您必须指定取款账户。', 'Вы должны указать счет вывода.'),
(264, 'withdraw-account-not-found', 'La cuenta especificada no existe.', 'The specified account does not exist.', '', 23, '指定的帐户不存在。', 'Указанная учетная запись не существует.'),
(265, 'withdraw-amount-zero', 'Por favor especificar una cantidad mayor a cero para retirar.', 'Please specify an amount to withdraw greater than zero.', '', 23, '提现金额必须是大于0的整数。', 'Пожалуйста, укажите сумму вывода больше нуля.'),
(266, 'withdraw-account-not-associated', 'La cuenta especificada no está asociada con su perfil.', 'The specified account is not associated with your profile.', '', 23, '指定的帐户未与您的个人配置文件相关联。', 'Указанная учетная запись не связана с вашим профилем.'),
(267, 'withdraw-too-much', 'La cantidad a retirar excede los fondos disponibles.', 'The withdrawal amount exceeds your available funds.', '', 23, '取款金额超过了您的可用资金。', 'Сумма вывода превышает ваши доступные средства.'),
(268, 'withdraw-email-notice', 'Se requiere acción de su parte para autorizar este retiro. Por favor siga el vínculo que recibió en su email.', 'Further action is required to authorize this request. Please follow the link that you received in your email to authorize this transaction.', '', 23, '进一步的行动是必需要授权申请。请按照您电子邮件获得的授权本次交易的链接。', 'Необходимы действия с вашей стороны, чтобы разрешить этот вывод. Пожалуйста, перейдите по ссылке которую вы получили на вашу электронную почту, чтобы разрешить эту сделку.'),
(269, 'withdraw-sms-sent', 'Se le ha enviado un código de autorización por SMS.', 'An SMS message has been sent to you with the authorization code.', '', 23, '短信跟您的授权码已发送到您。', 'Вам было отправлено сообщение SMS с кодом авторизации.'),
(270, 'withdraw-2fa-success', 'Su solicitud de retiro ha sido autorizada exitósamente.', 'Your withdrawal request has been authorized successfully.', '', 23, '您的取款请求已成功授权。', 'Ваш запрос на вывод был успешно разрешен.'),
(271, 'security-resend-sms', 'Reenviar SMS', 'Resend SMS', '', 20, '重新发送短信', 'Отправить повторное SMS'),
(272, 'withdraw-address-invalid', 'La dirección Bitcoin especificada es inválida.', 'You have specified an invalid Bitcoin address.', '', 23, '您指定了一个无效的比特币地址。', 'Вы указали неверный Биткоин-адрес.'),
(273, 'withdraw-btc-request', 'Su solicitud de retiro se ha creado. Sus bitcoins serán enviados próximamente.', 'Your withdrawal request has been created successfully. Your bitcoins will be sent shortly.', '', 23, '您的取款请求已成功创建。您的比特币将很快送到。', 'Ваш запрос на вывод был успешно создан. Ваши биткоины будут отправлены в ближайшее время.'),
(274, 'withdraw-btc-com-error', 'Ocurrió un error de sistema y la transacción no se pudo enviar. Por favor inténtelo de nuevo más tarde.', 'An error ocurred and your transaction could not be sent. Please try again later.', '', 23, '发生了错误，您的交易无法发送。请稍后再试。', 'Произошла ошибка и ваша транзакция не может быть отправлена. Пожалуйста, повторите попытку позже.'),
(275, 'withdraw-btc-success', 'Su retiro de bitcoins ha sido enviada exitósamente.', 'Your Bitcoin withdrawal was sent successfully.', '', 23, '您的比特币提款已成功发送。', 'Ваш Биткойн-вывод был успешно отправлен.'),
(276, 'withdraw-success', 'Su solicitud de retiro se ha creado exitósamente.', 'Your withdrawal request has been created successfully.', '', 23, '您的取款请求已成功创建。', 'Ваш запрос на вывод был создан успешно.'),
(277, 'settings-user', 'Usuario', 'Username', '', 104, '用户名', 'Имя пользователя'),
(278, 'settings-unique-error', 'Ya existe una cuenta asociada al email especificado.', 'There is already an account associated with that email address.', '', 104, '已经有与该电子邮件地址相关联的帐户。', 'Существует уже Аккаунт, связанный с этим адресом электронной почты.'),
(279, 'settings-registration-info', 'Información de registro', 'Registration Info', '', 104, '注册信息', 'Регистрационная Информация'),
(280, 'register-success', 'Gracias por registrarse para utilizar [exchange_name]. Sus nombre de usuario y contraseña fueron enviados a su email.', 'Thank you for registering to use [exchange_name]! Your username and password have been sent to you by email.', '', 27, '感谢您注册在[exchange_name]！您的用户名和密码已经发送到您的邮箱。', 'Благодарим вас за регистрацию на сайте [exchange_name]! Ваше имя пользователя и пароль были отправлены на электронную почту.'),
(281, 'settings-request-expired', 'Error: La solicitud ha expirado.', 'Error: Request has expired.', '', 104, '错误：请求超时。', 'Ошибка: Время ожидания запроса истекло.'),
(282, 'settings-settings-message', 'Sus preferencias personales se han actualizado.', 'Your personal settings have been updated.', '', 104, '您的个人设置已更新。', 'Ваши персональные настройки были обновлены.'),
(283, 'account-security-notify', 'Se recomienda configurar autenticación de dos factores para su cuenta utilizando su celular. Para configurarlo, por favor visite la  <a href="security.php">página de seguridad</a>.', 'It is recommended that you set up two-factor authentication for your account using your cellphone. To do so, please visit <a href="security.php">the security page</a>.', '', 17, '建议您使用您的手机设置双因素身份验证对您的帐户。为了这个, 请访问 <a href=\\"security.php\\">安全页面</a>。', 'Рекомендуется настроить двухфакторную аутентификацию для вашего аккаунта с помощью мобильного телефона. Для этого, пожалуйста, посетите <a href="security.php"> страницу безопасности </a>.'),
(284, 'sell-errors-no-amount', 'Debe ingresar una cantidad mayor a cero para vender.', 'You must enter an amount greater than zero to sell.', '', 17, '出售金额必须是大于0的整数。', 'Вы должны ввести сумму больше, чем ноль, чтобы продать.  '),
(285, 'settings-change-notice', 'Se requiere acción de su parte para autorizar los cambios en su cuenta. Por favor siga el vínculo que recibió en su email.', 'Further action is required to authorize these changes. Please follow the link that you received in your email to authorize.', '', 104, '这些变化的进一步的行动是必需要授权。请按照您电子邮件收到的授权的链接。', 'Необходимы действия с вашей стороны, чтобы разрешить внести эти изменения. Пожалуйста, перейдите по ссылке которую вы получили на вашу электронную почту.'),
(286, 'login-password-compare', 'La contraseña ingresada no concuerda con la verificación.', 'The password you have entered does not match the verification.', '', 104, '您输入的密码与验证不匹配。', 'Введенный пароль не соответствует проверке.'),
(287, 'transactions-download', 'Bajar transacciones en archivo CSV', 'Download transactions CSV file', '', 17, '下载交易CSV文件', 'Загрузить сделки в формате CSV-файла'),
(288, 'first-login', 'Primer login', 'First Login', '', 104, '先登录', 'Сначала Войти'),
(289, 'settings-save-password', 'Cambiar contraseña', 'Set Password', '', 104, '设置密码', 'Установить Пароль'),
(290, 'settings-personal-password', 'Personalizar contraseña', 'Personalize Password', '', 104, '个性化密码', 'Пароль: персонализация'),
(291, 'settings-pass-explain', 'Por favor especifique una contraseña para su nueva cuenta. Si no lo hace, lo puede hacer luego en la página de preferencias. ', 'Please specify a password for your new account. If you do not do so, you can still change it later in the settings page.', '', 104, '请注明密码为您的新帐户。如果您不这样做，以后您仍然可以在设置页面中进行更改。', 'Пожалуйста, укажите пароль для новой учетной записи. Если вы этого не сделаете, вы можете изменить его позже на странице настроек.'),
(292, 'settings-notify-login', 'Notificar por email cuando alguien hace login a su cuenta.', 'Notify by email when somebody logs into your account.', '', 104, '当有人登录到您的帐户，通过电子邮件通知。', 'Уведомлять по электронной почте, когда кто-то входит в ваш аккаунт.'),
(293, 'orders-delete', 'Cancelar esta órden', 'Cancel this order', '', 9, '取消此订单', 'Отменить эту заявку'),
(294, 'orders-not-yours', 'Usted no puede cancelar una órden que no es suya.', 'You cannot cancel somebody else''s order.', '', 9, '您不能取消别人的订单', 'Вы не можете отменить чью-либо заявку.'),
(295, 'orders-order-cancelled', 'La órden especificada ha sido cancelada.', 'The selected order has been cancelled.', '', 9, '选定的订单已被取消。', 'Выбранная заявка была отменена.'),
(296, 'orders-order-doesnt-exist', 'La órden especificada ya ha sido ejecutada o no existe.', 'The order you have specified has been executed already or does not exist.', '', 9, '您所指定的订单已被执行或不存在。', 'Указанная заявка уже была выполнена или не существует.'),
(297, 'confirm-transaction', 'Confirmar detalles de transacción', 'Confirm Transaction Details', '', 17, '确认交易详情', 'Подтвердить Детали Сделки'),
(298, 'confirm-buy', 'Confirmar compra', 'Confirm Buy', '', 17, '确认购买', 'Подтвердить Покупку'),
(299, 'confirm-sale', 'Confirmar venta', 'Confirm Sale', '', 17, '确认购卖', 'Подтвердить Продажу'),
(300, 'confirm-back', 'Atrás', 'Back', '', 17, '返回', 'Назад');

INSERT INTO bimar_lang (id, key, esp, eng, orders, p_id, zh, ru) VALUES
(301, 'settings-terms-accept', 'Acepto los <a href="terms.php">términos y condiciones</a> de uso de sitio.', 'I accept the <a href="terms.php">terms and conditions</a> of use of this site.', '', 104, '我接受<a href=\\"terms.php\\">条款和</a>使用本网站的条件。', 'Я принимаю правила <a href="terms.php"> и условия </a> использования этого сайта.'),
(302, 'settings-terms-error', 'Debe aceptar los <a href="terms.php">términos de uso</a> para registrarse como usuario.', 'You must accept the <a href="terms.php">terms of use</a> to register.', '', 104, '您必须接受使用 <a href=\\"terms.php\\">条款</a>进行注册。', 'Вы должны принять <a href="terms.php"> условия использования </a> чтобы зарегистрироваться.'),
(303, 'settings-delete-account', 'Desactivar mi cuenta', 'Deactivate My Account', '', 104, '停用我的帐户', 'Деактивировать Мой Аккаунт'),
(304, 'settings-delete-account-explain', 'Usted puede desactivar su cuenta pulsando en este botón. Si decide que desea reactivar su cuenta, lo puede hacer en cualquier momento ingresando a su cuenta y pulsando sobre el botón de reactivación.', 'By clicking this button, you can deactivate your account. If you change your mind, you can reactivate your account at any time by logging in.', '', 104, '通过点击这个按钮，您的帐户会被停用。但如果您改变了主意，您可以在任何时候重新激活您的账号。', 'Вы можете деактивировать свой аккаунт, нажав на эту кнопку. Если вы передумаете, вы можете активировать свой аккаунт в любое время, войдя в свою учетную запись и нажав на кнопку реактивировать.'),
(305, 'settings-account-deactivated', 'Su cuenta ha sido desactivada. Recuerde que lo puede reactivar de nuevo desde esta misma página.', 'Your account has been deactivated. Remember that you can reactivated again from this page if you decide to do so..', '', 104, '您的帐户已被停用。请记住，您可在任何时候重新激活您的账号直接从该页面。', 'Ваша учетная запись-деактивирована. Помните, что вы можете реактивировать ее снова с этой страницы, если решите сделать это.'),
(306, 'settings-deactivate-error', 'Las cuentas que todavía mantienen fondos en BTC o cualquiér otra moneda no se pueden desactivar.', 'Accounts that still have funds in BTC or any other currency cannot be deactivated.', '', 104, '账户不能被停用如果还存比特币在或任何其他货币资金。', 'Счета, на которых до сих пор имеются средства в BTC или любой другой валюте не могут быть деактивированы.'),
(307, 'settings-reactivate-account', 'Reactivar mi cuenta', 'Reactivate My Account', '', 104, '重新激活我的帐户', 'Реактивировать Мой Аккаунт'),
(308, 'settings-reactivate-account-explain', 'Al pulsar en este botón, podrá reactivar su cuenta desactivada, lo cual le dará acceso a toda funcionalidad otra vez.', 'By clicking this button, you can reactivate your deactivated account, enabling all functionality once again.', '', 104, '通过点击这个按钮，就可以重新激活您停用的帐户，所有的功能再次启用。', 'При нажатии на эту кнопку, вы можете активировать свой деактивированный счет, это позволит восстановить доступ ко всем функциям снова.'),
(309, 'settings-account-reactivated', 'Su cuenta se ha reactivado exitósamente.', 'Your account has been reactivated successfully.', '', 104, '您的帐户已成功激活。', 'Ваша учетная запись успешно реактивирована.'),
(310, 'settings-lock-account', 'Bloquear mi cuenta', 'Lock My Account', '', 104, '锁定我的帐户', 'Заблокировать Мой Аккаунт'),
(311, 'settings-lock-account-explain', 'Al pulsar en este botón, podrá bloquear su cuenta para deshabilitar toda funcionalidad. Podrá desbloquear lo luego ingresando a esta misma página.', 'By clicking this button, you can lock your account so that all actions will be disabled. You can unlock it from this same page.', '', 104, '通过点击这个按钮，您的帐户会被锁定，使所有的功能将被禁用。您可在任何时候解锁您的账号直接从该页面。', 'При нажатии на эту кнопку, вы можете заблокировать вашу учетную запись, при этом все функции будут отключены. Вы можете ее разблокировать с этой же страницы.'),
(312, 'settings-unlock-account', 'Desbloquear mi cuenta', 'Unlock My Account', '', 104, '解开我的帐户', 'Разблокировать Мой Аккаунт'),
(313, 'settings-unlock-account-explain', 'Pulsando en este botón podrá desbloquear su cuenta y reactivar toda funcionalidad.', 'By clicking this button you can unlock your account and re-enable all functionality.', '', 104, '通过点击这个按钮，您的帐户会被解锁，所有的功能再次启用。', 'При нажатии на эту кнопку вы можете разблокировать свой аккаунт и повторно включить все функции.'),
(314, 'settings-account-locked', 'Su cuenta ha sido bloqueada. Lo puede desbloquear visitando esta misma página.', 'Your account has been locked. You can unlock it by visiting this same page.', '', 104, '您的帐户已被锁定。您可解锁您的账号直接从该页面。', 'Ваша учетная запись была заблокирована. Вы можете разблокировать ее, посетив эту же страницу.'),
(315, 'settings-account-unlocked', 'Su cuenta ha sido desbloqueada.', 'Your account is now unlocked.', '', 104, '您的帐户被解锁了。', 'Ваш Аккаунт Разблокирован.'),
(316, 'securing-account', '¿Cómo asegurar su cuenta?', 'Securing Your Account', '', 9, '保护您的帐户', 'Защита Вашего Аккаунта'),
(317, 'funding-account', '¿Cómo obtener fondos?', 'Funding Your Account', '', 9, '资助您的账户', 'Пополнение Вашего Аккаунта'),
(318, 'withdrawing-account', '¿Cómo retirar fondos?', 'Withdrawing Funds', '', 9, '提现资金', 'Вывод Средств'),
(319, 'buy-errors-no-bank-account', 'Por favor recuerde abrir una cuenta en [currency] para poder retirar el dinero que recibirá de esta transacción.', 'Please remember to open an account in [currency] so that you can withdraw the currency you will receive from this transaction.', '', 17, '请记得打开在[currency]的一个账号，这样就可以收回，你会收到来自该交易的货币。', 'Пожалуйста, не забудьте открыть счет в [currency], чтобы вывести деньги, которые вы получите от этой сделки.'),
(320, 'all-currencies', 'Todos', 'All', '', 17, '都', 'Все'),
(321, 'withdrawal-recent', 'Retiros recientes', 'Recent Withdrawals', '', 23, '最新的提现', 'Последние Выводы'),
(329, 'reset-2fa', 'Reiniciar 2FA', 'Resetting 2FA', '', 9, '重置双因素身份验证', 'Сброс 2FA'),
(327, 'security-disable', 'Inhabilitar Autenticación', 'Disable 2FA', '', 20, '关闭双因素身份验证', 'Отключить 2FA'),
(328, 'security-disabled-message', 'Su cuenta ya no esta protegida por autenticación de dos factores.', 'Your account is no longer protected by two-factor authentication.', '', 20, '您的帐户被双因素身份验证不再受保护。', 'Ваша учетная запись более не защищена двухфакторной аутентификацией.'),
(322, 'security-enable-google', 'Habilitar Google Authenticator', 'Enable Google Authenticator', '', 20, '启用谷歌身份验证器', 'Включить Google Authenticator'),
(326, 'security-incorrect-token', 'PIN incorrecto.', 'Incorrect token.', '', 20, '不正确的令牌。', 'Неправильный код.'),
(325, 'security-optional-google', 'Opcional para Google Auth.', 'Optional for Google Auth.', '', 20, '可选的谷歌身份验证器。', 'Опционально для Google Auth.'),
(323, 'security-scan-qr', 'Verificar Google Authenticator', 'Verify Google Authenticator', '', 20, '检查谷歌身份验证器', 'Проверить Google Authenticator'),
(324, 'security-secret-code', 'Código secreto', 'Secret Code', '', 20, '密码', 'Секретный Код'),
(330, 'buy-errors-no-compatible', 'No es posible crear una órden de mercado porque no hay órdenes compatibles en el sistema.', 'You cannot create a market order because there are no compatible orders available.', '', 17, '您不能创造一个市场订单，因为没有可用的兼容的订单。', 'Вы не можете создать рыночную заявку, потому что нет совместимых заявок в системе.'),
(331, 'buy-errors-too-little', 'La cantidad mínima para una órden es [fa_symbol][amount].', 'The minimum order quantity is [fa_symbol][amount].', '', 17, '最低金额为订单是[fa_symbol][amount]。', 'Минимальная сумма для заявки [fa_symbol][amount].'),
(332, 'history', 'Historial', 'History', '', 0, '历史', 'История'),
(333, 'history-ip', 'Dirección IP', 'IP Address', '', 332, 'IP 地址', 'IP Адрес'),
(334, 'buy-errors-outbid-self', 'No se le permite comprar a mayor precio de lo que está vendiendo, o vender a menor precio de lo que está comprando.', 'You cannot buy higher than you are selling, or sell lower than you are buying.', '', 17, '不得购买以更高的价格比您卖，或出售以较低的价格比您所购买的。', 'Вы не можете купить по цене выше, чем вы продаете, или продать по цене ниже, чем вы покупаете.'),
(335, 'buy-stop-lower-ask', 'El precio del stop debe mayor al precio de demanda actual.', 'The stop price must be higher than the current ask.', '', 17, '止损价应该高比目前的 ask。', 'Стоп-цена должна быть выше, чем текущая ask.'),
(336, 'sell-stop-higher-bid', 'El precio del stop debe ser menor al de la mejor oferta actual.', 'The stop price must be lower than the current bid.', '', 17, '止损价应该低比目前的 bid。', 'Стоп-цена должна быть ниже, чем текущая bid. '),
(337, 'buy-stop-price', 'Precio de Stop', 'Stop Price', '', 17, '止损价', 'Стоп-Цена'),
(338, 'buy-stop', 'Órden stop', 'Stop Order', '', 17, '止损单', 'Стоп-Ордер'),
(339, 'buy-limit', 'Órden limitada', 'Limit Order', '', 17, '限价单', 'Лимитный Ордер'),
(340, 'buy-limit-price', 'Precio límite', 'Limit Price', '', 17, '限价', 'Лимитная Цена'),
(341, 'buy-stop-lower-price', 'El precio del stop debe ser mayor al precio límite.', 'The stop price must be higher than the limit price.', '', 17, '止损价应该高比限价。', 'Стоп-цена должна быть выше, чем лимитная цена.'),
(342, 'sell-stop-lower-price', 'El precio del stop debe ser menor al precio límite.', 'The stop price must be lower than the limit price.', '', 17, '止损价应该低比限价。', 'Стоп-цена должна быть ниже, чем лимитная цена.'),
(343, 'buy-errors-no-stop', 'Debe ingresar un precio para el stop.', 'You must enter a stop price.', '', 17, '您必须输入一个止损价。', 'Вы должны ввести стоп-цену.'),
(344, 'buy-notify-two-orders', 'Usted está creando dos órdenes ligadas. La ejecución de uno cancelará el otro.', 'You are placing two linked orders. Execution of one will cancel the other (OCO).', '', 17, '您放置两个相关的订单。一个取消另外一个（OCO）。', 'Вы размещаете две связанные заявки. Выполнение одной отменит другую (ОСО).'),
(345, 'buy-errors-under-market', 'Su órden está demasiado por debajo del precio del mercado.', 'Your limit order is too far from market price.', '', 17, '您的限价单定得与市价太远。', 'Ваш лимитный ордер слишком далек от рыночной цены.'),
(348, 'about', 'Acerca de [exchange_name]', 'About [exchange_name]', '', 0, '关于[exchange_name]', 'О [exchange_name]'),
(346, 'buy-limit-under-stops', 'No se permite comprar a menor precio que sus órdenes stop de venta.', 'You cannot place a limit order to buy under the price of your sell stops.', '', 17, '您不能放置限价单买入在低于您的卖出止损的价格。', 'Вы не можете разместить лимитный ордер на покупку ниже цены ваших sell stops.'),
(347, 'sell-limit-under-stops', 'No se permite poner una órden stop a mayor precio del que está comprando.', 'You cannot place a stop order to sell at a higher price than you are buying.', '', 17, '您不能放置止损单卖出以更高于您买的价格。', 'Вы не можете разместить стоп-ордер на продажу по более высокой цене, чем вы покупаете.'),
(350, 'orders-converted-from', 'Esta órden ha sido convertida desde [currency].', 'Converted from [currency].', '', 17, '转换从[currency]。', 'Преобразовано из  [currency].'),
(351, 'account-fee-bracket1', 'Tasa de comisión (fijador)', 'Commission rate (maker)', '', 17, '佣金率（制造商）', 'Комиссионная ставка (производитель)'),
(352, 'account-global-btc', 'Vol. BTC 24h en casa de cambio', 'Exchange-wide 24h BTC volume', '', 17, '所24小时的交流比特币的体积', 'Объем всего суточного обмена BTC'),
(353, 'limit-min-price', 'El precio mínimo es [price] debido a una órden suya en otra moneda.', 'The minimum price is [price] because of your order in a different currency.', '', 17, '最小价是[price]因为您的订单在不同的货币。', 'Минимальная цена [price] по причине вашей заявки в другой валюте.'),
(354, 'limit-max-price', 'El precio máximo es [price] debido a una órden suya en otra moneda.', 'The maximum price is [price] because of your order in a different currency.', '', 17, '最大价是[price]因为您的订单在不同的货币。', 'Максимальная цена [price] по причине вашей заявки в другой валюте.'),
(355, 'fee-schedule-fee1', 'Comisión de fijador (maker)', 'Maker Fee', '', 16, '', ''),
(356, 'fee-schedule-flc', 'CTC* - Volúmen global 24h', 'FLC* - 24h Global Volume', '', 16, 'FLC* - 24小时的总体体积', 'FLC*- Суточный Мировой Объем'),
(357, 'our-security', 'Nuestra seguridad', 'Our Security', '', 0, '我们的安全战略', 'Безопасность'),
(358, 'default-currency', 'Moneda por defecto', 'Default Currency', '', 27, '默认货币', 'Валюта по умолчанию'),
(359, 'trading-competition', 'Competencia de trading', 'Trading Challenge', '', 0, '交易挑战', 'Торговый Вызов'),
(360, 'trading-competition-banner', 'Competencia de trading inicia en [time]. Regístrese ahora para ganar hasta<strong>5 bitcoins</strong>!', 'Our trading challenge is starting in [time]. Register now to win up to <strong>5 bitcoins</strong>!', '', 359, '我们的交易挑战 [time] 开始。立即注册并赢 <strong> 5比特币</strong>!', 'Наш торговый вызов начинается в  [time]. Зарегистрируйтесь сейчас, чтобы выиграть до <strong> 5 биткоинов</strong>!'),
(361, 'trading-competition-started', '[time] hasta el final de la competencia de trading!', '[time] until the end of our trading competition!', '', 359, '[time] 到我们的 交易挑战结束！', '[time] до конца нашего торгового вызова!'),
(422, 'pre-registration', 'Pre-Registro', 'Pre-Registration', '', 0, '预登记', 'Предварительная Регистрация'),
(423, 'pre-open', '¡El pre-registro ya está abierto!', 'Pre-Registration Is Now Open!', '', 422, '预登记已开放！', 'Предварительная Регистрация Открыта!'),
(362, 'contest-ranking', 'Ver ranking actual', 'View Current Ranking', '', 359, '查看当前排名', 'Просмотреть Текущий Рейтинг'),
(363, 'trading-competition-hello', '¡Bienvenido a la <strong>competencia de trading 1BTCXE</strong>!', 'Welcome to the <strong>1BTCXE Trading Challenge!</strong>', '', 359, '欢迎到<strong>1BTCXE 交易挑战！</strong>', 'Добро пожаловать на <strong>1BTCXE Торговый Вызов!</strong>'),
(364, 'competition-starting-in', 'Comenzando en', 'Starting in', '', 359, '开始', 'Начинается в'),
(365, 'competition-prize', 'Premio: <span class="prize">1 BTC</span>', 'Prize: <span class="prize">1 BTC</span>', '', 359, '奖品: <span class=\\"prize\\">1 BTC</span>', 'Приз: <span class="prize">1 BTC</span>'),
(366, 'competition-typo', 'Errs. ortog./links rotos:', 'Typos/broken links:', '', 359, '错别字/断开链接：', 'Опечатки / неработающие ссылки:'),
(367, 'competition-func', 'Funcionalidad', 'Functionality', '', 359, '功能', 'Функциональность'),
(368, 'competition-exploits', 'Exploits de seguridad:', 'Security exploits:', '', 359, '安全漏洞：', 'Защита от эксплойтов:'),
(369, 'competition-time-left', 'Terminando en', 'Time left', '', 359, '剩余时间', 'Оставшееся время'),
(370, 'competition-finished', 'Finalizado', 'Finished', '', 359, '结束了', 'Завершен'),
(371, 'trading-competition-board', '<strong>Estatus</strong> de la competencia de trading', 'Trading Competition <strong>Status</strong>', '', 359, '交易大赛的<strong>状态</strong>', '<strong>Состояние</strong>Торгового вызова '),
(372, 'competition-top-10', 'Mejores 10 usuarios', 'Top 10 Users', '', 359, '10个最好的使用者', '10 Лучших Пользователей'),
(373, 'competition-profit', 'Ganancia USD', 'USD Profit', '', 359, '奖 USD', 'Выигрыш USD'),
(374, 'competition-rewards', 'Recompensas', 'Rewards', '', 359, '奖励', 'Награды'),
(375, 'competition-my-rank', 'Mi rango', 'My Rank', '', 359, '我的等级', 'Мой Ранг'),
(376, 'competition-not-started', 'La competencia no ha empezado todavía. ¡Por favor regrese más tarde!', 'The competition has not started yet. Please check back later!', '', 359, '比赛还没开始。请稍后再试！', 'Конкурс еще не начался. Пожалуйста, проверьте позже!'),
(377, 'trading-competition-status', 'Estatus de competencia de trading', 'Trading Competition Status', '', 359, '交易大赛的状态', 'Состояние Торгового Состязания'),
(378, 'competition-feature-disabled', 'Esta función está desactivada hasta el final de nuestra <a href="contest-status.php">competencia de trading</a>.', 'This feature is disabled until the end of our <a href="contest-status.php">trading competition</a>.', '', 359, '此功能被禁用，直到我们的<a href=\\"contest-status.php\\">交易大赛</a>结束。', 'Эта функция отключена до конца нашего <a href="contest-status.php">торгового состязания</a>.'),
(379, 'competition-feature-before-start', 'Esta función está desactivada hasta el inicio de nuestra <a href="contest-status.php">competencia de trading</a>.', 'This feature is disabled until the start of our <a href="contest-status.php">trading competition</a>.', '', 359, '此功能被禁用，直到我们的<a href=\\"contest-status.php\\">交易大赛</a>开始。', 'Эта функция отключена до начала нашего <a href="contest-status.php">торгового состязания</a>.'),
(380, 'simulation', 'Simulación', 'Simulation', '', 359, '模拟', 'Моделирование'),
(381, 'competition-status', 'Ver estatus de la competencia.', 'View competition status.', '', 359, '查看大赛的状态。', 'Посмотреть статус состязания.'),
(382, 'competition-rules', 'Ver las reglas de la competencia', 'View Competition Rules', '', 359, '查看比赛规则', 'Посмотреть Правила Конкурса'),
(383, 'competition-first-place', 'Primer lugar', 'First Place', '', 359, '第一名', 'Первое Место'),
(384, 'competition-second-place', 'Segundo lugar', 'Second Place', '', 359, '第二名', 'Второе Место'),
(385, 'competition-third-place', 'Tercer lugar', 'Third Place', '', 359, '第三名', 'Третье Место'),
(386, 'sponsored-by', 'Patrocinado por', 'Sponsored by', '', 359, '赞助', 'Спонсор'),
(387, '404', 'Página no encontrada', 'Page Not Found', '', 0, '找不到网页', 'Страница Не Найдена'),
(388, '404-desc', 'La página que busca no se pudo encotrar.', 'The page you are looking for could not be found.', '', 387, '无法显示网页您正在查找的页当前不可用。', 'Страница, которую вы ищете, не найдена.'),
(389, '404-back', 'Regresar al inicio', 'Go back to home', '', 387, '返回首页', 'Вернуться на домашнюю страницу'),
(390, 'contest-join', '¡Participe en nuestra competencia de trading!', 'Join Our Trading Challenge!', '', 359, '参加我们的交易挑战！', 'Примите Участие в Нашем Торговом Вызове!'),
(391, 'contest-started', '¡Nuestra competencia de trading ha iniciado!', 'Our Trading Challenge Has Begun!', '', 359, '我们的交易挑战开始了！', 'Наш торговый Вызов Начался!'),
(392, 'contest-catchline', 'Estamos regalando <strong><i class="fa fa-btc"></i>10</strong> en premios!', 'We are giving away <strong><i class="fa fa-btc"></i>10</strong> in cash and prizes!', '', 359, '我们赠送<strong><i class=\\"fa fa-btc\\"></i>10个</strong>在现金和奖品！', 'Мы раздаем <strong><i class="fa fa-btc"></i>10</strong> наличными деньгами и призами!'),
(393, 'contest-get-started', '¡Empezar ahora!', 'Get Started!', '', 359, '开始！', 'Начать!'),
(394, 'competition-fourth', '4° a 10°', '4th-10th', '', 359, '从第四到第十', 'с 4 по 10'),
(395, 'competition-fourth-desc', '<a href="http://bitcointrezor.com">Trezor</a> hardware wallet para estas posiciones.', '<a href="http://bitcointrezor.com">Trezor</a> hardware wallet for runners up 4-10.', '', 359, '<a href=\\"http://bitcointrezor.com\\">Trezor</a> 硬件钱包对第4-10名。', '<a href="http://bitcointrezor.com">Trezor</a> аппаратный кошелек для позиций от 4-10.'),
(396, 'competition-dates', 'Del <b>9</b> al <b>19</b> de septiembre', 'From the <b>9th</b> to the <b>19th</b> of September', '', 359, '从<b>9月9号</b> 到<b>9月19号</b> ', 'С <b>9</b> по <b>19</b> Сентября'),
(397, 'competition-contingency', 'Las recompensas se reparten al primero en reportar el problema.', 'Rewards are contingent on being the first to report the issue.', '', 359, '奖项分布取决谁是第一个报告问题。', 'Награды распределяются в зависимости от того, кто будучи первым сообщит о проблеме.'),
(398, 'home-landing-currency', 'Cambiar [currency] por <strong>BTC</strong>', 'Exchange [currency] to <strong>BTC</strong>', '', 9, '转换[currency]到 <strong>比特币</strong>', 'Обменять [currency] на <strong>BTC</strong>'),
(399, 'home-landing-currency-explain', '[currency] a <strong>Bitcoin</strong> precio histórico y libreta de órdenes.', '[currency] to <strong>Bitcoin</strong> historical prices and order book.', '', 9, '[currency]到<strong>比特币</strong>的历史价格和定貨簿。', '[currency] на <strong>Биткойн</strong> исторические цены и кнага заявок. '),
(400, 'home-landing-sign-up', 'Regístrese para cambiar [currency] a BTC', 'Sign Up to Exchange [currency] to BTC', '', 9, '注册对转换[currency]到比特币', 'Зарегистрируйтесь, чтобы обменять [currency] to BTC'),
(401, 'login-pass-chars-error', 'Algunos de los caracteres que utilizó en su contraseña no están permitidos ([characters]).', 'Some of the characters in your password are not permitted ([characters]).', '', 27, '一些您密码中的字符不允许([characters])。', 'Некоторые из символов, используемых в пароле не разрешаются ([characters]).'),
(424, 'pre-catchline', '¡Abra una cuenta para comprar y vender Bitcoin en 30 monedas diferentes!', 'Sign up now and start trading Bitcoin in 30 different currencies!', '', 422, '立即注册并开始在30个不同的货币交易比特币！', 'Зарегистрируйтесь сейчас и начните торговлю Биткойн в 30 разных валютах!'),
(425, 'pre-get-account', 'Abrir Cuenta', 'Open Account', '', 422, '开账户', 'Открыть Счет'),
(426, 'pre-time-left', 'Tiempo restante', 'Time left', '', 422, '剩余时间', 'Оставшееся время'),
(427, 'pre-feature-disabled', 'Esta función está desactivada hasta el inicio de nuestras operaciones.', 'This feature is disabled until the start of operations.', '', 422, '此功能被禁用直到操作开始。', 'Эта функция отключена до начала операций.'),
(428, 'home-github', 'Ver en GitHub', 'View on GitHub', '', 9, '查看在 GitHub', 'Посмотреть на GitHub'),
(429, 'halted', 'detenido', 'halted', '', 9, '暂停', 'остановлен'),
(430, 'buy-trading-disabled', 'Trading ha sido suspendido temporalmente por motivos de mantenimiento. Se reanudará en breve.', 'Trading has been suspended temporarily for maintenance purposes. It will resume shortly.', '', 17, '交易已被暂停以便维护。它在短期内恢复。', 'Торговля была временно приостановлена по причине технического обслуживания. Она возобновится в ближайшее время.'),
(431, 'withdrawal-suspended', 'Los retiros han sido suspendidos temporalmente por motivos de mantenimiento. Volverán a funcionar en breve.', 'Withdrawals are temporarily suspended for maintenance purposes. They will function again shortly.', '', 23, '提现已被暂停以便维护。它在短期内恢复。', 'Выводы временно приостановлены по причине технического обслуживания. Они будут возобновлены в ближайшее время.'),
(432, 'order-book-last-price-explain', '¿Último pricio fuera de rango? Pulse para ver por qué.', 'Last transaction occured in the currency within the parentheses', '', 11, '最后一笔交易发生在货币括号内', 'Последняя цена вне диапазона? Нажмите, чтобы понять, почему.'),
(433, 'api-docs', 'Documentación del API', 'API Documentation', '', 0, 'API文档', 'API Документация'),
(434, 'api-access', 'Acceso al API', 'API Access', '', 402, 'API 访问权限', 'Доступ к API '),
(435, 'api-go-to-docs', 'Para documentación acerca del uso de este API, por favor acceder a <u><a href="api-docs.php">la documentación del API</a></u>.', 'For documentation on how to use this API, please see the <u><a href="api-docs.php">API documentation</a></u>.', '', 402, '有关如何使用此API，请参阅<u><a href=\\"api-docs.php\\"> API文档</a></u>。', 'Для документации об использовании этого API обратитесь к <u><a href="api-docs.php"> API документации </a> </ U>.'),
(402, 'api', 'API', 'API', '', 0, 'API', 'API'),
(403, 'api-add-message', 'Su clave de API se ha generado exitósamente.', 'Your API key has been successfully generated.', '', 402, '您的API密钥已生成成功了。', 'Ваш ключ API был успешно создан.'),
(404, 'api-add-show-secret', 'Your <b>clave secreta de API</b> ess:<br/><span class="bigger">[secret]</span><br/>¡Por favor anote esta clave, ya que no se podrá recuperarse en otra ocasión!', 'Your <b>API secret</b> is:<br/><span class="bigger">[secret]</span><br/>Please right this value down, as it cannot be recovered later! ', '', 402, '您的<b> API秘密钥</b>是：<br/><span class=\\"bigger\\">[secret]</span><br/>请写下来，因为它不能稍后再恢复！', 'Ваш <b>API секретный ключ</b> :<br/><span class="bigger">[secret]</span><br/>Пожалуйста, запишите его, т.к. в другой раз вы не сможете его восстановить!'),
(405, 'api-edit-message', 'Sus permisos de API se guardaron exitósamente.', 'Your API permissions were edited successfully.', '', 402, '您的API权限已成功编辑。', 'Ваши API полномочия были успешно отредактированы.'),
(406, 'api-delete-message', 'La clave API seleccionada ha sido eliminada.', 'The selected API key has been deleted.', '', 402, '所选择的API密钥已被删除。', 'Выбранный ключ API был удален.'),
(407, 'api-delete-error', 'La clave API seleccionada no pudo ser eliminada.', 'The selected API key could not be deleted.', '', 402, '所选择的API密钥无法删除。', 'Выбранный ключ API не может быть удален.'),
(408, 'api-access-setup', 'Configuración de claves API', 'API Keys Setup', '', 402, '设置API密钥', 'Настройка Ключей API'),
(411, 'api-setup-security', 'Habilitar 2FA', 'Setup 2FA', '', 402, '设置2FA', 'Настройка  2FA'),
(412, 'api-add-new', 'Generar nueva clave', 'Generate New Key', '', 402, '产生新的密钥', 'Сгенерировать Новый Ключ'),
(413, 'api-add-save', 'Guardar permisos de API', 'Save API Permissions', '', 402, '保存API权限', 'Сохранить API Полномочия'),
(414, 'api-keys', 'Claves API activas', 'Available API Keys', '', 402, '可用的API密钥', 'Доступные Ключи API'),
(415, 'api-key', 'Clave API', 'API Key', '', 402, 'API 密钥', 'Ключ API'),
(416, 'api-permissions', 'Permisos', 'Permissions', '', 402, '权限', 'Полномочия'),
(417, 'api-permission_view', 'Ver info. de cuenta', 'View account info.', '', 402, '查看帐户信息', 'Просмотреть информацию аккаунта'),
(418, 'api-permission_orders', 'Crear/editar órdenes', 'Create/edit orders', '', 402, '订单修改|创建', 'Создать/редактировать заявки'),
(419, 'api-permission_withdraw', 'Retirar fondos', 'Withdraw funds', '', 402, '提现资金', 'Вывод средств'),
(420, 'api-keys-no', 'No hay claves API para mostrar.', 'There are no API keys to show.', '', 402, '没有API钥匙显示。', 'Нет ключей API для отображения.'),
(409, 'api-disabled', 'Configuración de API desactivada', 'API Setup Disabled', '', 402, 'API设置被禁用', 'Установка API Отключена'),
(410, 'api-disabled-explain', 'Debe activar 2FA (autenticación de dos factores) para proceder.', 'You must enable 2FA on your account to proceed.', '', 402, '为了继续，您需要激活2FA。', 'Для продолжения, вам необходимо активировать 2FA.'),
(436, 'settings-notify-withdraw-btc', 'Notificar por email cuando se retira BTC.', 'Notify by email when BTC withdrawals are made.', '', 104, '提现比特币时, 通过电子邮件通知。', 'Уведомлять по электронной почте, когда выводы BTC сделаны.'),
(437, 'settings-notify-withdraw-bank', 'Notificar por email cuando se retira a una cuenta bancaria.', 'Notify by email when withdrawals to bank accounts are made.', '', 104, '银行账户取款时, 通过电子邮件通知。', 'Уведомлять по электронной почте, когда производится снятие с банковского счета.'),
(438, 'withdraw-network-fee', 'Cuota del Blockchain', 'Blockchain Fee', '', 23, '区块链费', 'Сборы Блокчейна'),
(439, 'withdraw-network-fee-explain', 'La cuota que cobra la red por transacciones.', 'The fee the network charges for transactions.', '', 23, '收取网络交易的费用。', 'Плата, взимаемая сетью за транзакции.'),
(440, 'withdraw-btc-total', 'BTC a recibir', 'BTC to Receive', '', 23, '以接收BTC', 'BTC к получению'),
(441, 'withdraw-total', '[currency] a recibir', '[currency] to Receive', '', 23, '[currency]以接收', '[currency] к получению'),
(442, 'withdraw-net-amount', 'Monto Neto', 'Net Amount', '', 23, '净额', 'Чистая Сумма'),
(443, 'last-page', 'Último', 'Last', '', 17, '最后一页', 'Последняя'),
(444, 'first-page', 'Primero', 'First', '', 17, '第一页', 'Первая'),
(445, 'google-recaptcha-error', 'Por favor seleccione la casilla en el Google ReCaptcha!', 'Please check the Google ReCaptcha!', '', 26, '请检查Google ReCaptcha!', 'Пожалуйста, проверьте Google ReCaptcha!'),
(446, 'google-recaptcha-connection', 'No hay conección al servicio de reCaptcha.', 'Could not connect to the reCaptcha service.', '', 26, '无法连接到reCaptcha服务。', 'Не удалось подключиться к службе reCaptcha.'),
(447, 'login-timeout', 'Por favor espere [timeout] antes de intentar hacer login.', 'Please wait [timeout] until your next login attempt.', '', 27, '请稍候[timeout]，直到您下次登录尝试。', 'Пожалуйста, подождите [timeout] до следующей попытки входа в систему.'),
(448, 'orders-order-cancelled-all', 'Se han cancelado todas sus órdenes.', 'You have cancelled all your orders.', '', 9, '您已取消所有订单。', 'Вы отменили все ваши заявки.'),
(449, 'orders-order-cancelled-error', 'Ocurrió un error al cancelar las órdenes.', 'An error ocurred when cancelling your orders.', '', 9, '取消订单的时发生了错误。', 'Произошла ошибка при отмене ваших заявок.'),
(450, 'order-cancel-all', 'Cancelar todas las órdenes', 'Cancel All Orders', '', 9, '取消所有订单', 'Отменить все заявки'),
(451, 'order-cancel-all-conf', 'Cancelar todas las órdenes?', 'Cancel all orders?', '', 9, '取消所有订单吗？', 'Отменить все заявки ?'),
(452, 'withdraw-amount-one', 'No se puede retirar menos de una unidad de la moneda.', 'You cannot withdraw less than one unit of currency.', '', 23, '您不能提款不到一个单位货币。', 'Сумма вывода должна составлять не менее одной единицы валюты.'),
(453, 'change-password', 'Cambiar contraseña', 'Change Password', '', 0, '更改密码', 'Изменить пароль.'),
(454, 'change-password-explain', 'Por favor especifique una nueva contraseña para su cuenta.', 'Please specify a new password for your account.', '', 453, '请为您的帐户指定一个新密码。', 'Пожалуйста, укажите новый пароль для вашей учетной записи.'),
(455, 'account-welcome-anon', 'Bienvenido a la cuenta', 'Welcome to account', '', 17, '欢迎来到账户', 'Добро пожаловать на счет');

*/

INSERT INTO bimar_order_types (id, name_en, name_ru, name_zh) VALUES
(1, 'Bid',  'Покупка', '委买'),
(2, 'Ask',  'Продажа', '委卖');

INSERT INTO bimar_request_descriptions (id, name_en, name_es, name_ru, name_zh) VALUES
(1, 'Withdrawal to Crypto Capital account', 'Retiro a cuenta Crypto Capital', 'Вывод средств на счет Crypto Capital ', '提现到Crypto Capital账户'),
(2, 'Bitcoin withdrawal', 'Retiro de Bitcoin', 'Биткойн-вывод', '比特币提款'),
(3, 'Deposit from Crypto Capital account', 'DepÃ³sito desde cuenta Crypto Capital', 'Ввод с аккаунта Crypto Capital ', '从Crypto Capital账户存款'),
(4, 'Bitcoin deposit', 'DepÃ³sito de Bitcoin', 'Биткойн-ввод', '比特币存款');

INSERT INTO bimar_request_status (id, name_en, name_es, name_ru, name_zh) VALUES
(1, 'Pending', 'Pendiente', 'Ожидаемый', '未决状态'),
(2, 'Completed', 'Completo', 'Завершен', '完成'),
(3, 'Canceled', 'Cancelado', 'Отменен', '取消'),
(4, 'Awaiting Authorization', 'Esperando autorizaciÃ³n', 'В ожидании Авторизации', '等待授权');


INSERT INTO bimar_request_types (id, name_en, name_es, name_ru, name_zh) VALUES
(1, 'Withdrawal', 'Retiro', 'Выводы', '提现'),
(2, 'Deposit', 'Depósito', 'Вводы', '充值');


INSERT INTO bimar_transaction_types (id, name_en, name_es, name_ru, name_zh) VALUES
(1, 'Buy', 'Compra', 'Покупка', '买'),
(2, 'Sell', 'Venta', 'Продажа', '卖');



REVOKE ALL ON SCHEMA bimar FROM bimar;
GRANT ALL ON SCHEMA bimar TO bimar;