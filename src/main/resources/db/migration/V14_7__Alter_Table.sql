/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  afes
 * Created: Apr 20, 2018
 */


DROP TRIGGER IF EXISTS updatebalancewd  ON bimar.bimar_requests CASCADE; 
CREATE TRIGGER updatebalancewd
  AFTER INSERT
  ON bimar.bimar_requests
  FOR EACH ROW
  WHEN ((new.request_type = 1) AND (new.request_status = 4))
  EXECUTE PROCEDURE bimar.updatebalancewd();

INSERT INTO bimar.bimar_request_types(
             id, name_en, name_es, name_ru, name_zh)
    VALUES ( 3, 'BUY', 'Aĉeti', 'купить',''),
    ( 4, 'SELL', 'Vendu', 'ПРОДАТЬ','')  ON CONFLICT DO NOTHING;


CREATE OR REPLACE FUNCTION bimar.createhistorysellexchange()
  RETURNS trigger AS
$BODY$
DECLARE
   vFromCoin integer;
   vToCoin integer;
   vBuyPendingId integer;
   vPendAmt double precision;
   vTmpAmountSell double precision;   
begin
	select b.id into vFromCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_from_currency)
	where a.id = NEW.id;

	select b.id into vToCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_to_currency)
	where a.id = NEW.id;

	/*INSERT INTO bimar.bimar_transactions(
            date, site_user, btc, currency, btc_price, fiat, fee, transaction_type, 
            site_user1, transaction_type1, fee1, btc_net, btc_net1, btc_before1, 
            btc_after1, fiat_before1, fiat_after1, btc_before, btc_after, 
            fiat_before, fiat_after, log_id1, log_id, fee_level, fee_level1, 
            currency1, orig_btc_price, conversion_fee, convert_amount, convert_rate_given, 
            convert_system_rate, convert_from_currency, convert_to_currency, 
            conversion, bid_at_transaction, ask_at_transaction, factored)
	VALUES (NEW.date, NEW.site_user, NEW.btc, NEW.currency, NEW.btc_price, NEW.fiat, NEW.fee, 1, 
            NEW.site_user1, NEW.transaction_type1, NEW.fee1, NEW.btc_net, NEW.btc_net1, NEW.btc_before1, 
            NEW.btc_after1, NEW.fiat_before1, NEW.fiat_after1, NEW.btc_before, NEW.btc_after, 
            NEW.fiat_before, NEW.fiat_after, NEW.log_id1, NEW.id, NEW.fee_level, NEW.fee_level1, 
            NEW.currency1, NEW.orig_btc_price, NEW.conversion_fee, NEW.convert_amount, NEW.convert_rate_given, 
            NEW.convert_system_rate, NEW.convert_from_currency, NEW.convert_to_currency, 
            NEW.conversion, NEW.bid_at_transaction, NEW.ask_at_transaction, NEW.factored);

        */
	--simpan ke tabel orders
        INSERT INTO bimar.bimar_orders(
        date, order_type, site_user, btc, fiat, currency, btc_price, 
        market_price, log_id, stop_price, fee1)
	select a.date,1,a.site_user,a.btc,0,a.currency,a.btc_price,
	a.btc_price,0,a.btc_price,a.fee from bimar.bimar_transactions as a
	where id = NEW.id;

	--simpan ke tabel history perubahan coin from
        INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vFromCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceBefore,
	(select (balance - trans.btc) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--simpan ke tabel history perubahan coin to
	INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vToCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceBefore,
	(select (balance + trans.btc_net) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;
	
	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (balance - a.btc_net) as afterBalace from bimar.bimar_transactions as a where id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vFromCoin;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (balance + a.btc_net) as afterBalace from bimar.bimar_transactions as a where id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vToCoin;
return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bimar.createhistorysellexchange()
  OWNER TO bimar;

CREATE OR REPLACE FUNCTION bimar.createhistorybuyexchange()
  RETURNS trigger AS
$BODY$
DECLARE
   vFromCoin integer;
   vToCoin integer;
   vSellPendingId integer;
   vPendAmt double precision;
   vTmpAmountBuy double precision;   
begin
	select b.id into vFromCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_from_currency)
	where a.id = NEW.id;

	select b.id into vToCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_to_currency)
	where a.id = NEW.id;

	/*INSERT INTO bimar.bimar_transactions(
            date, site_user, btc, currency, btc_price, fiat, fee, transaction_type, 
            site_user1, transaction_type1, fee1, btc_net, btc_net1, btc_before1, 
            btc_after1, fiat_before1, fiat_after1, btc_before, btc_after, 
            fiat_before, fiat_after, log_id1, log_id, fee_level, fee_level1, 
            currency1, orig_btc_price, conversion_fee, convert_amount, convert_rate_given, 
            convert_system_rate, convert_from_currency, convert_to_currency, 
            conversion, bid_at_transaction, ask_at_transaction, factored)
	VALUES (NEW.date, NEW.site_user, NEW.btc, NEW.currency, NEW.btc_price, NEW.fiat, NEW.fee, 2, 
            NEW.site_user1, NEW.transaction_type1, NEW.fee1, NEW.btc_net, NEW.btc_net1, NEW.btc_before1, 
            NEW.btc_after1, NEW.fiat_before1, NEW.fiat_after1, NEW.btc_before, NEW.btc_after, 
            NEW.fiat_before, NEW.fiat_after, NEW.log_id1, NEW.id, NEW.fee_level, NEW.fee_level1, 
            NEW.currency1, NEW.orig_btc_price, NEW.conversion_fee, NEW.convert_amount, NEW.convert_rate_given, 
            NEW.convert_system_rate, NEW.convert_from_currency, NEW.convert_to_currency, 
            NEW.conversion, NEW.bid_at_transaction, NEW.ask_at_transaction, NEW.factored);
        */
	--simpan ke tabel orders
        INSERT INTO bimar.bimar_orders(
        date, order_type, site_user, btc, fiat, currency, btc_price, 
        market_price, log_id, stop_price, fee1)
	select a.date,1,a.site_user,a.btc,0,a.currency,a.btc_price,
	a.btc_price,0,a.btc_price,a.fee from bimar.bimar_transactions as a
	where id = NEW.id;

	--simpan ke tabel history perubahan coin from
        INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vFromCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceBefore,
	(select (balance + trans.btc) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--simpan ke tabel history perubahan coin to
	INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vToCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceBefore,
	(select (balance - trans.btc_net) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;		

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (a.balance + sub.btc_net) as afterBalace from bimar.bimar_transactions as sub where sub.id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vFromCoin;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (a.balance - sub.btc_net) as afterBalace from bimar.bimar_transactions as sub where sub.id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vToCoin;

	

return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bimar.createhistorybuyexchange()
  OWNER TO bimar;

ALTER TABLE bimar.bimar_requests drop column if exists lastprice; 

alter table bimar.bimar_requests 
    add column lastprice double precision default 0;
