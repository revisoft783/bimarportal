/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  afes
 * Created: Apr 19, 2018
 */

CREATE OR REPLACE FUNCTION bimar.updatebalancewd()
RETURNS trigger AS
$BODY$
begin
    
	--simpan ke tabel history perubahan balance
    INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        values
		(new.date,'',1,0,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = new.site_user and id_coins = new.crypto_id),new.id,
	new.site_user,
	(select balance from bimar.bimar_site_users_balances where site_user = new.site_user and coinid = new.crypto_id),
	(select (balance - new.net_amount) as fixAmt from bimar.bimar_site_users_balances where site_user = new.site_user and coinid = new.crypto_id));
			
	--update table  balance user
	update bimar.bimar_site_users_balances as a
	set balance = a.balance - new.net_amount
	where site_user = new.site_user
	and coinid = new.crypto_id;
    
	
return new;
end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER updatebalancewd ON bimar.bimar_requests CASCADE;

CREATE TRIGGER updatebalancewd
  AFTER INSERT
  ON bimar.bimar_requests
  FOR EACH ROW
  EXECUTE PROCEDURE bimar.updatebalancewd();