/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  afes
 * Created: Mar 22, 2018
 */

Drop Table bimar.bimar_api_keys;

CREATE TABLE IF NOT EXISTS bimar.bimar_api_coins (
  id serial Not Null,
  name varchar(255),  
  key varchar(255),
  secret varchar(255),
  website varchar(255),  
  CONSTRAINT bimar_api_coins_pkey PRIMARY KEY (id),
  CONSTRAINT KEY UNIQUE(name, key)  
);
Alter Table bimar.bimar_api_coins Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar.bimar_api_address (
  id_address serial Not Null,
  site_user integer,
  id_coins integer,
  address varchar(255),
  label varchar(255),
  CONSTRAINT bimar_api_address_pkey PRIMARY KEY (id_address)
);
Alter Table bimar.bimar_api_address Owner TO bimar;

CREATE INDEX IF NOT EXISTS siteuser
   ON bimar.bimar_api_address (site_user ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS idcoins
   ON bimar.bimar_api_address (id_coins ASC NULLS LAST);

ALTER TABLE ONLY bimar.bimar_api_address
    ADD CONSTRAINT bimar_api_address_fk FOREIGN KEY (site_user) REFERENCES bimar.bimar_site_users(id) ON UPDATE CASCADE;

ALTER TABLE ONLY bimar.bimar_api_address
    ADD CONSTRAINT bimar_api_coins_fk FOREIGN KEY (id_coins) REFERENCES bimar.bimar_api_coins(id) ON UPDATE CASCADE;
  