/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  afes
 * Created: Mar 20, 2018
 */

Drop Table bimar.bimar_admin_users;

Alter Table bimar.bimar_site_users
    Drop COLUMN  authy_id ;  

Alter Table bimar.bimar_site_users
    Add column is_admin smallint DEFAULT 0,
    Add column date_authy_id timestamp without time zone,
    Add column orders smallint Default 0,
    Add column website character varying(50) default '',
    add column address character varying(50) default '',
    add column city character varying(50) default '',
    add column phone character varying(20) default '',
    add authy_id character varying(255);        

ALTER TABLE bimar.bimar_api_keys
  ADD COLUMN coins_name character varying(255);
