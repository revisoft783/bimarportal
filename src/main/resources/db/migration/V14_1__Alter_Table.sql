/**
 * Author:  yusril
 */
ALTER TABLE bimar.bimar_requests drop column if exists fcurrency; 
ALTER TABLE bimar.bimar_requests drop column if exists tcurrency;

alter table bimar.bimar_requests 
    add column fcurrency int default 0,
    add column tcurrency int default 0 ;


CREATE OR REPLACE FUNCTION bimar.createhistorybuy()
  RETURNS trigger AS
$BODY$
DECLARE
   vFromCoin integer;
   vToCoin integer;
   vSellPendingId integer;
   vPendAmt double precision;
   vTmpAmountBuy double precision;
   cSellPending CURSOR FOR select id,net_amount from bimar.bimar_requests 
	where request_status = 1 and request_type = 4
	order by net_amount DESC;
begin
	select b.id into vFromCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_from_currency)
	where a.id = NEW.id;

	select b.id into vToCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_to_currency)
	where a.id = NEW.id;
	

	--simpan ke tabel orders
        INSERT INTO bimar.bimar_orders(
        date, order_type, site_user, btc, fiat, currency, btc_price, 
        market_price, log_id, stop_price, fee1)
	select a.date,1,a.site_user,a.btc,0,a.currency,a.btc_price,
	a.btc_price,0,a.btc_price,a.fee from bimar.bimar_transactions as a
	where id = NEW.id;

	--simpan ke tabel history perubahan coin from
        INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vFromCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceBefore,
	(select (balance - trans.btc) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--simpan ke tabel history perubahan coin to
	INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vToCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceBefore,
	(select (balance + trans.btc_net) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--cari transaksi sell yang pending
	select coalesce(id,0),coalesce(net_amount,0) into vSellPendingId,vPendAmt from bimar.bimar_requests 
	where request_status = 1 and request_type = 4
	and net_amount = NEW.btc_net
	limit 1;

	if(vSellPendingId <> 0)then
		update bimar.bimar_requests set request_status = 2
		where id = vSellPendingId;
		
		select (vTmpAmountBuy - vPendAmt) into vTmpAmountBuy;
		--stok bimar harus dikurangi
		update bimar.bimar_stock_coins as a
		set amount = (a.amount - vPendAmt) 
		where id = vToCoin;
	else
		select NEW.btc_net into vTmpAmountBuy;
		OPEN cSellPending;
		LOOP
			FETCH cSellPending INTO vSellPendingId,vPendAmt;
			EXIT WHEN NOT FOUND;

			if(vTmpAmountBuy >= vPendAmt)then
				update bimar.bimar_requests set request_status = 2
				where id = vSellPendingId;
			
				select (vTmpAmountBuy - vPendAmt) into vTmpAmountBuy;
				--stok bimar harus dikurangi
				update bimar.bimar_stock_coins as a
				set amount = (a.amount - vPendAmt) 
				where id = vToCoin;
			end if;

			
			
		END LOOP;
		CLOSE cSellPending;
		
	end if;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (a.balance - sub.btc) as afterBalace from bimar.bimar_transactions as sub where sub.id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vFromCoin;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (a.balance + sub.btc_net) as afterBalace from bimar.bimar_transactions as sub where sub.id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vToCoin;

	

return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bimar.createhistorybuy()
  OWNER TO bimar;


-- Function: bimar.createhistorysell()

-- DROP FUNCTION bimar.createhistorysell();

CREATE OR REPLACE FUNCTION bimar.createhistorysell()
  RETURNS trigger AS
$BODY$
DECLARE
   vFromCoin integer;
   vToCoin integer;
   vBuyPendingId integer;
   vPendAmt double precision;
   vTmpAmountSell double precision;
   cBuyPending CURSOR FOR select id,net_amount from bimar.bimar_requests 
	where request_status = 1 and request_type = 3
	order by net_amount DESC;
begin
	select b.id into vFromCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_from_currency)
	where a.id = NEW.id;

	select b.id into vToCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_to_currency)
	where a.id = NEW.id;
	

	--simpan ke tabel orders
        INSERT INTO bimar.bimar_orders(
        date, order_type, site_user, btc, fiat, currency, btc_price, 
        market_price, log_id, stop_price, fee1)
	select a.date,1,a.site_user,a.btc,0,a.currency,a.btc_price,
	a.btc_price,0,a.btc_price,a.fee from bimar.bimar_transactions as a
	where id = NEW.id;

	--simpan ke tabel history perubahan coin from
        INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vFromCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceBefore,
	(select (balance - trans.btc) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--simpan ke tabel history perubahan coin to
	INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vToCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceBefore,
	(select (balance + trans.btc_net) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--cari transaksi buy yang pending
	select coalesce(id,0),coalesce(net_amount,0) into vBuyPendingId,vPendAmt from bimar.bimar_requests 
	where request_status = 1 and request_type = 3
	and net_amount = NEW.btc_net
	limit 1;

	if(vBuyPendingId <> 0)then
		update bimar.bimar_requests set request_status = 2
		where id = vBuyPendingId;
		
		select (vTmpAmountSell - vPendAmt) into vTmpAmountSell;
		--stok bimar harus dikurangi
		update bimar.bimar_stock_coins as a
		set amount = (a.amount - vPendAmt) 
		where id = vToCoin;
		
	else
		select NEW.btc_net into vTmpAmountSell;
		OPEN cBuyPending;
		LOOP
			FETCH cBuyPending INTO vBuyPendingId,vPendAmt;
			EXIT WHEN NOT FOUND;

			if(vTmpAmountSell >= vPendAmt)then
				update bimar.bimar_requests set request_status = 2
				where id = vBuyPendingId;
			
				select (vTmpAmountSell - vPendAmt) into vTmpAmountSell;
				--stok bimar harus dikurangi
				update bimar.bimar_stock_coins as a
				set amount = (a.amount - vPendAmt) 
				where id = vToCoin;
			end if;
		END LOOP;
		CLOSE cBuyPending;
	end if;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (balance - a.btc) as afterBalace from bimar.bimar_transactions as a where id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vFromCoin;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (balance + a.btc_net) as afterBalace from bimar.bimar_transactions as a where id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vToCoin;
return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bimar.createhistorysell()
  OWNER TO bimar;

-- Function: bimar.updatebuybalance()

-- DROP FUNCTION bimar.updatebuybalance();

CREATE OR REPLACE FUNCTION bimar.updatebuybalance()
  RETURNS trigger AS
$BODY$
DECLARE
   vFCoin integer;
   vFCoinDesc varchar(10);
   vUser integer;
begin
	select b.id,a.site_user,name into vFCoin,vUser,vFCoinDesc from bimar.bimar_requests as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.FCurrency)
	where a.id = NEW.id;

	--balance user dikurangi
	update bimar.bimar_site_users_balances  as a
	set balance = (select (a.balance - sub.net_amount) as afterBalace from bimar.bimar_requests as sub where sub.id = NEW.id) 
	where coinid = vFCoin and site_user = vUser;

	--balance bimar ditambah
	update bimar.bimar_stock_coins as a 
	set amount = ((select (a.amount + sub.net_amount) as afterBalace from bimar.bimar_requests as sub where sub.id = NEW.id))
	where nama_coins = vFCoinDesc;
return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bimar.updatebuybalance()
  OWNER TO bimar;

-- Function: bimar.updatesellbalance()

-- DROP FUNCTION bimar.updatesellbalance();

CREATE OR REPLACE FUNCTION bimar.updatesellbalance()
  RETURNS trigger AS
$BODY$
DECLARE
   vFCoin integer;
   vFCoinDesc varchar(10);
   vUser integer;
begin
	select b.id,a.site_user,name into vFCoin,vUser,vFCoinDesc from bimar.bimar_requests as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.FCurrency)
	where a.id = NEW.id;

	--balance user dikurangi
	update bimar.bimar_site_users_balances  as a
	set balance = (select (a.balance - sub.net_amount) as afterBalace from bimar.bimar_requests as sub where sub.id = NEW.id) 
	where coinid = vFCoin and site_user = vUser;

	--balance bimar ditambah
	update bimar.bimar_stock_coins as a 
	set amount = ((select (a.amount + sub.net_amount) as afterBalace from bimar.bimar_requests as sub where sub.id = NEW.id))
	where nama_coins = vFCoinDesc;
return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bimar.updatesellbalance()
  OWNER TO bimar;

DROP TRIGGER after_buy ON bimar.bimar_transactions CASCADE;
CREATE TRIGGER after_buy
  AFTER INSERT
  ON bimar.bimar_transactions
  FOR EACH ROW
  WHEN ((new.transaction_type = 1))
  EXECUTE PROCEDURE bimar.createhistorybuy();

DROP TRIGGER after_sell ON bimar.bimar_transactions CASCADE;
CREATE TRIGGER after_sell
  AFTER INSERT
  ON bimar.bimar_transactions
  FOR EACH ROW
  WHEN ((new.transaction_type = 2))
  EXECUTE PROCEDURE bimar.createhistorysell();

DROP TRIGGER after_pendingbuy ON bimar.bimar_requests CASCADE;
CREATE TRIGGER after_pendingbuy
  AFTER INSERT
  ON bimar.bimar_requests
  FOR EACH ROW
  WHEN (((new.request_status = 1) AND (new.request_type = 3)))
  EXECUTE PROCEDURE bimar.updatebuybalance();

DROP TRIGGER after_pendingsell ON bimar.bimar_requests CASCADE;
CREATE TRIGGER after_pendingsell
  AFTER INSERT
  ON bimar.bimar_requests
  FOR EACH ROW
  WHEN ((new.request_type = 4))
  EXECUTE PROCEDURE bimar.updatesellbalance();