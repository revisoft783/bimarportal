/**
 * Author:  yusril
 */

CREATE OR REPLACE FUNCTION bimar.createhistorybuy()
  RETURNS trigger AS
$BODY$
DECLARE
   vFromCoin integer;
   vToCoin integer;
begin
	select b.id into vFromCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_from_currency)
	where a.id = NEW.id;

	select b.id into vToCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_to_currency)
	where a.id = NEW.id;
	

	--simpan ke tabel orders
        INSERT INTO bimar.bimar_orders(
        date, order_type, site_user, btc, fiat, currency, btc_price, 
        market_price, log_id, stop_price, fee1)
	select a.date,1,a.site_user,a.btc,0,a.currency,a.btc_price,
	a.btc_price,0,a.btc_price,a.fee from bimar.bimar_transactions as a
	where id = NEW.id;

	--simpan ke tabel history perubahan coin from
        INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vFromCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceBefore,
	(select (balance - trans.btc) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--simpan ke tabel history perubahan coin to
	INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vToCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceBefore,
	(select (balance + trans.btc_net) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (balance - a.btc) as afterBalace from bimar.bimar_transactions as a where id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vFromCoin;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (balance + a.btc_net) as afterBalace from bimar.bimar_transactions as a where id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vToCoin;
return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bimar.createhistorybuy()
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION bimar.createhistorysell()
  RETURNS trigger AS
$BODY$
DECLARE
   vFromCoin integer;
   vToCoin integer;
begin
	select b.id into vFromCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_from_currency)
	where a.id = NEW.id;

	select b.id into vToCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_to_currency)
	where a.id = NEW.id;
	

	--simpan ke tabel orders
        INSERT INTO bimar.bimar_orders(
        date, order_type, site_user, btc, fiat, currency, btc_price, 
        market_price, log_id, stop_price, fee1)
	select a.date,1,a.site_user,a.btc,0,a.currency,a.btc_price,
	a.btc_price,0,a.btc_price,a.fee from bimar.bimar_transactions as a
	where id = NEW.id;

	--simpan ke tabel history perubahan coin from
        INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vFromCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceBefore,
	(select (balance - trans.btc) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--simpan ke tabel history perubahan coin to
	INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vToCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceBefore,
	(select (balance + trans.btc_net) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (balance - a.btc) as afterBalace from bimar.bimar_transactions as a where id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vFromCoin;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (balance + a.btc_net) as afterBalace from bimar.bimar_transactions as a where id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vToCoin;
return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bimar.createhistorysell()
  OWNER TO postgres;

CREATE TRIGGER after_buy
  AFTER INSERT
  ON bimar.bimar_transactions
  FOR EACH ROW
  WHEN ((new.transaction_type = 1))
  EXECUTE PROCEDURE bimar.createhistorybuy();

CREATE TRIGGER after_sell
  AFTER INSERT
  ON bimar.bimar_transactions
  FOR EACH ROW
  WHEN ((new.transaction_type = 2))
  EXECUTE PROCEDURE bimar.createhistorysell();