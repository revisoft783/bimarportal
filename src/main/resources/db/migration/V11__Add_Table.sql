/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  afes
 * Created: Apr 9, 2018
 */

CREATE TABLE IF NOT EXISTS bimar.bimar_api_mcoins_chart (
  id serial Not Null,
  fromcoins varchar(255),  
  tocoins varchar(255),  
  last_price double precision,
  last_update double precision,
  last_volume double precision, 
  last_volume_to double precision, 
  last_trade_id double precision, 
  volume_24h double precision, 
  volume_24hto double precision,
  open_24h double precision,
  hight_24hto double precision,
  low_24hto double precision,
  change_pct24h double precision,
  change_24h double precision,
  last_modified timestamp with time zone,
  CONSTRAINT bimar_api_mcoins_chart_pkey PRIMARY KEY (id)
);
Alter Table bimar.bimar_api_mcoins_chart Owner TO bimar;