/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  afes
 * Created: Mar 30, 2018
 */

ALTER TABLE bimar.bimar_api_mcoins
  ADD COLUMN coins2 integer;

ALTER TABLE ONLY bimar.bimar_api_mcoins
    ADD CONSTRAINT bimar_api_mcoins_fk FOREIGN KEY (coins2) REFERENCES bimar.bimar_api_coins(id) ON UPDATE CASCADE;