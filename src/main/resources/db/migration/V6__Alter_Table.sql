/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  afes
 * Created: Mar 30, 2018
 */

ALTER TABLE bimar.bimar_api_mcoins
  ADD COLUMN price double precision,
  ADD COLUMN lastupdate double precision,
  ADD COLUMN changepct24h double precision,
  ADD COLUMN changepct1h double precision;
  