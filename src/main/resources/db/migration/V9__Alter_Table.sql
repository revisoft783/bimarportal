
/**
 * Author:  yusril
 * Created: Mar 31, 2018
 */

alter table bimar.bimar_currencies  
alter column currency type varchar(10);

alter table bimar.bimar_api_coins 
add column currencyID integer;

ALTER TABLE bimar.bimar_api_mcoins
  DROP CONSTRAINT KEY_Bimar_Api_mcoins
, ADD  CONSTRAINT KEY_Bimar_Api_mcoins UNIQUE(name_skt, coins2) ;  