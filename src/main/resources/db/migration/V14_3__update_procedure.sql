CREATE OR REPLACE FUNCTION bimar.updatebuybalance()
  RETURNS trigger AS
$BODY$
DECLARE
   vFCoin integer;
   vFCoinDesc varchar(10);
   vTCoin integer;
   vTCoinDesc varchar(10);
   vUser integer;
   vExistsFcoin integer;
   vExistsTcoin integer;
begin
	select b.id,a.site_user,name into vFCoin,vUser,vFCoinDesc from bimar.bimar_requests as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.FCurrency)
	where a.id = NEW.id;

	select b.id,a.site_user,name into vTCoin,vUser,vTCoinDesc from bimar.bimar_requests as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.TCurrency)
	where a.id = NEW.id;

	--pengecekan jika coin belum ada(from coin)
	select count(id) into vExistsFcoin from bimar.bimar_stock_coins where nama_coins = vFCoinDesc;
	if(vExistsFcoin = 0)then
		INSERT INTO bimar.bimar_stock_coins(nama_coins, amount)
		VALUES (vFCoinDesc, 0);
	end if;
	--pengecekan jika coin belum ada(to coin)
	select count(id) into vExistsTcoin from bimar.bimar_stock_coins where nama_coins = vTCoinDesc;
	if(vExistsTcoin = 0)then
		INSERT INTO bimar.bimar_stock_coins(nama_coins, amount)
		VALUES (vTCoinDesc, 0);
	end if;

	--balance user dikurangi
	update bimar.bimar_site_users_balances  as a
	set balance = (select (a.balance - sub.net_amount) as afterBalace from bimar.bimar_requests as sub where sub.id = NEW.id) 
	where coinid = vFCoin and site_user = vUser;

	--balance bimar ditambah
	update bimar.bimar_stock_coins as a 
	set amount = ((select (a.amount + sub.net_amount) as afterBalace from bimar.bimar_requests as sub where sub.id = NEW.id))
	where nama_coins = vFCoinDesc;
return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bimar.updatebuybalance()
  OWNER TO bimar;


CREATE OR REPLACE FUNCTION bimar.updatesellbalance()
  RETURNS trigger AS
$BODY$
DECLARE
   vFCoin integer;
   vFCoinDesc varchar(10);
   vTCoin integer;
   vTCoinDesc varchar(10);
   vUser integer;
   vExistsFcoin integer;
   vExistsTcoin integer;
begin
	select b.id,a.site_user,name into vFCoin,vUser,vFCoinDesc from bimar.bimar_requests as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.FCurrency)
	where a.id = NEW.id;

	select b.id,a.site_user,name into vTCoin,vUser,vTCoinDesc from bimar.bimar_requests as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.TCurrency)
	where a.id = NEW.id;

	--pengecekan jika coin belum ada(from coin)
	select count(id) into vExistsFcoin from bimar.bimar_stock_coins where nama_coins = vFCoinDesc;
	if(vExistsFcoin = 0)then
		INSERT INTO bimar.bimar_stock_coins(nama_coins, amount)
		VALUES (vFCoinDesc, 0);
	end if;
	--pengecekan jika coin belum ada(to coin)
	select count(id) into vExistsTcoin from bimar.bimar_stock_coins where nama_coins = vTCoinDesc;
	if(vExistsTcoin = 0)then
		INSERT INTO bimar.bimar_stock_coins(nama_coins, amount)
		VALUES (vTCoinDesc, 0);
	end if;

	--balance user dikurangi
	update bimar.bimar_site_users_balances  as a
	set balance = (select (a.balance - sub.net_amount) as afterBalace from bimar.bimar_requests as sub where sub.id = NEW.id) 
	where coinid = vFCoin and site_user = vUser;

	--balance bimar ditambah
	update bimar.bimar_stock_coins as a 
	set amount = ((select (a.amount + sub.net_amount) as afterBalace from bimar.bimar_requests as sub where sub.id = NEW.id))
	where nama_coins = vFCoinDesc;
return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bimar.updatesellbalance()
  OWNER TO bimar;
