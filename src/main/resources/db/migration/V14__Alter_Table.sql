/**
 * Author:  yusril
 */
CREATE OR REPLACE FUNCTION bimar.createhistorybuy()
  RETURNS trigger AS
$BODY$
DECLARE
   vFromCoin integer;
   vToCoin integer;
   vSellPendingId integer;
   vPendAmt double precision;
   vTmpAmountBuy double precision;
   cSellPending CURSOR FOR select id,net_amount from bimar.bimar_requests 
	where request_status = 1 and request_type = 4
	order by net_amount DESC;
begin
	select b.id into vFromCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_from_currency)
	where a.id = NEW.id;

	select b.id into vToCoin from bimar.bimar_transactions as a
	join bimar.bimar_api_coins as b on(b.currencyid = a.convert_to_currency)
	where a.id = NEW.id;
	

	--simpan ke tabel orders
        INSERT INTO bimar.bimar_orders(
        date, order_type, site_user, btc, fiat, currency, btc_price, 
        market_price, log_id, stop_price, fee1)
	select a.date,1,a.site_user,a.btc,0,a.currency,a.btc_price,
	a.btc_price,0,a.btc_price,a.fee from bimar.bimar_transactions as a
	where id = NEW.id;

	--simpan ke tabel history perubahan coin from
        INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vFromCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceBefore,
	(select (balance - trans.btc) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vFromCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--simpan ke tabel history perubahan coin to
	INSERT INTO bimar.bimar_history(
        date, ip, history_action, order_id, bitcoin_address, request_id, 
        site_user, balance_before, balance_after)
        select trans.date,'',1,NEW.id,(select a.address from bimar.bimar_api_address as a
	join bimar.bimar_api_coins as b on(b.id = a.id_coins)
	where site_user = trans.site_user and id_coins = vToCoin) as address,0,
	trans.site_user,(select balance from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceBefore,
	(select (balance + trans.btc_net) as fixAmt from bimar.bimar_site_users_balances where site_user = trans.site_user and coinid = vToCoin)
	as balanceAfter
	from bimar.bimar_transactions as trans
	where trans.id = NEW.id;

	--cari transaksi sell yang pending
	select coalesce(id,0),coalesce(net_amount,0) into vSellPendingId,vPendAmt from bimar.bimar_requests 
	where request_status = 1 and request_type = 4
	and net_amount = NEW.btc_net
	limit 1;

	if(vSellPendingId <> 0)then
		update bimar.bimar_requests set request_status = 2
		where id = vSellPendingId;
		
		select (vTmpAmountBuy - vPendAmt) into vTmpAmountBuy;
		--stok bimar harus dikurangi
		update bimar.bimar_stock_coins as a
		set amount = (a.amount - vPendAmt) 
		where id = vToCoin;
	else
		select NEW.btc_net into vTmpAmountBuy;
		OPEN cSellPending;
		LOOP
			FETCH cSellPending INTO vSellPendingId,vPendAmt;
			EXIT WHEN NOT FOUND;

			if(vTmpAmountBuy >= vPendAmt)then
				update bimar.bimar_requests set request_status = 2
				where id = vSellPendingId;
			
				select (vTmpAmountBuy - vPendAmt) into vTmpAmountBuy;
				--stok bimar harus dikurangi
				update bimar.bimar_stock_coins as a
				set amount = (a.amount - vPendAmt) 
				where id = vToCoin;
			end if;

			
			
		END LOOP;
		CLOSE cSellPending;
		
	end if;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (a.balance - sub.btc) as afterBalace from bimar.bimar_transactions as sub where sub.id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vFromCoin;

	--Ubah balance coin sumber
	update bimar.bimar_site_users_balances as a
	set balance = (select (a.balance + sub.btc_net) as afterBalace from bimar.bimar_transactions as sub where sub.id = NEW.id)
	where site_user = (select site_user as user from bimar.bimar_transactions where id = NEW.id)
	and coinid = vToCoin;

	

return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION bimar.createhistorybuy()
  OWNER TO bimar;


DROP TRIGGER after_buy ON bimar.bimar_transactions;

CREATE TRIGGER after_buy
  AFTER INSERT
  ON bimar.bimar_transactions
  FOR EACH ROW
  WHEN ((new.transaction_type = 1))
  EXECUTE PROCEDURE bimar.createhistorybuy();
