/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  afes
 * Created: Apr 15, 2018
 */
Alter Table bimar.bimar_site_users
    Add column affliate integer DEFAULT 0;

Alter Table bimar.bimar_app_config
    Add column affliate_pct integer DEFAULT 0,
    add column affliate_active smallint DEFAULT 0;

CREATE TABLE IF NOT EXISTS bimar.bimar_mst_affliate (
  id serial Not Null,
  name varchar(255),  
  is_active smallint Default 0,  
  CONSTRAINT bimar_mst_affliate_pkey PRIMARY KEY (id),
  CONSTRAINT KEY_Bimar_mst_affliate UNIQUE(name)  
);
Alter Table bimar.bimar_mst_affliate Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar.bimar_history_affliate (
  id serial Not Null,
  tgl_transaksi timestamp,
  id_user integer,
  id_user_afflite integer,
  amount double precision,  
  amount_pct_affliate double precision,  
  CONSTRAINT bimar_history_affliate_pkey PRIMARY KEY (id)
);
Alter Table bimar.bimar_history_affliate Owner TO bimar;

CREATE TABLE IF NOT EXISTS bimar.bimar_stock_coins (
  id serial Not Null,
  nama_coins varchar(255),
  amount double precision,    
  CONSTRAINT bimar_stock_coins_pkey PRIMARY KEY (id)
);
Alter Table bimar.bimar_stock_coins Owner TO bimar;
