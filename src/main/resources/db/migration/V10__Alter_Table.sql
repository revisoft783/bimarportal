
/**
 * Author:  yusril
 * Created: Mar 31, 2018
 */

ALTER TABLE bimar.bimar_site_users_balances 
DROP COLUMN IF EXISTS currency;

alter table bimar.bimar_site_users_balances
add column coinId integer;