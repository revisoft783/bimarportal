package com.id.bimar.dao;

import com.id.bimar.entity.BimarEmails;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yusril
 */
public interface BEmailRepository extends JpaRepository<BimarEmails, Integer>{
    BimarEmails findByKey(String id);
}
