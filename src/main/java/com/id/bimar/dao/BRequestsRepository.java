package com.id.bimar.dao;

import com.id.bimar.entity.BimarRequests;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author afes
 */
public interface BRequestsRepository extends JpaRepository<BimarRequests, Long> {

    BimarRequests findByTransactionId(String transactionId);

    @Query("select coalesce(sum(a.netAmount),0) from BimarRequests a "
            + "where a.requestStatus = 1 and a.requestType = 3 "
            + "and ?1 >= a.netAmount")
    float getSumPendingBuyNetAmount(double netAmount);
    
    @Query("select coalesce(sum(a.netAmount),0) from BimarRequests a "
            + "where a.requestStatus = 1 and a.requestType = 4 "
            + "and ?1 >= a.netAmount")
    float getSumPendingSellNetAmount(double netAmount);

    public List<BimarRequests> findBySiteUserAndRequestType(int userid, int i);
    
}
