package com.id.bimar.dao;

import com.id.bimar.entity.BimarAppConfig;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yusril
 */
public interface BAppConfigRepository extends JpaRepository<BimarAppConfig, Integer>{
//    BimarAppConfig findById(Integer id);
}
