/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.dao;

import com.id.bimar.entity.BimarApiCoins;
import com.id.bimar.entity.BimarApiMcoins;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author afes
 */
public interface BMCoinRepository extends JpaRepository<BimarApiMcoins, Long>{
    BimarApiMcoins findByName(String name);
    
    List<BimarApiMcoins> findByIdCoins(BimarApiCoins idCoins);
}
