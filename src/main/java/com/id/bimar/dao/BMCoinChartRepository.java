/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.dao;

import com.id.bimar.entity.BimarApiMcoinsChart;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author afes
 */
public interface BMCoinChartRepository extends JpaRepository<BimarApiMcoinsChart, Long>{
    BimarApiMcoinsChart findByLastUpdate(int lastUpdate);
    
    List<BimarApiMcoinsChart> findByFromcoinsAndTocoins(String fsym, String tsym);
}
