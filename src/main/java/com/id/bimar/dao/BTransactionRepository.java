package com.id.bimar.dao;

import com.id.bimar.entity.BimarTransactions;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yusril
 */
public interface BTransactionRepository extends JpaRepository<BimarTransactions, Long> {

}
