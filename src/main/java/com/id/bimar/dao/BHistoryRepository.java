/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.dao;

import com.id.bimar.entity.BimarHistory;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author afes
 */
public interface BHistoryRepository extends JpaRepository<BimarHistory, Long>{
    BimarHistory findByDateAndSiteUser(Date date, Integer siteUser);
}
