package com.id.bimar.dao;

import com.id.bimar.entity.BimarSiteUsersBalances;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author afes
 */
public interface BSiteUserBalanceRepository extends JpaRepository<BimarSiteUsersBalances, Long> {

    BimarSiteUsersBalances findBySiteUser(int siteUser);

    List<BimarSiteUsersBalances> findAllBySiteUser(int siteUser);
    
    BimarSiteUsersBalances findBySiteUserAndCoinid(Integer siteUser, Integer Coinid);        
}
