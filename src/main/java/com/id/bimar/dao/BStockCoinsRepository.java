package com.id.bimar.dao;

import com.id.bimar.entity.BimarStockCoins;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yusril
 */
public interface BStockCoinsRepository extends JpaRepository<BimarStockCoins, Long> {

    BimarStockCoins getByNamaCoins(String coinName);
}
