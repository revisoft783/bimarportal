package com.id.bimar.dao;

import com.id.bimar.entity.BimarCurrencies;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yusril
 */
public interface BCurrencyRepository extends JpaRepository<BimarCurrencies, Long>{
    BimarCurrencies findByCurrency(String currency);
}
