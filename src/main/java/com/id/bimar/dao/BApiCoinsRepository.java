package com.id.bimar.dao;

import com.id.bimar.entity.BimarApiCoins;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author afes
 */
public interface BApiCoinsRepository extends JpaRepository<BimarApiCoins, Integer>{

    @Override
    public List<BimarApiCoins> findAll();
    
    BimarApiCoins findByName(String name);        
}
