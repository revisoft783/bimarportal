
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.dao;

import com.id.bimar.entity.BimarApiAddress;
import com.id.bimar.entity.BimarApiCoins;
import com.id.bimar.entity.BimarSiteUsers;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
/**
 *
 * @author afes
 */
public interface BApiAddressRepository extends JpaRepository<BimarApiAddress, Long>{
    
    List<BimarApiAddress> findByIdCoinsAndSiteUser(BimarApiCoins idCoins, BimarSiteUsers siteUser);
    
    BimarApiAddress getByIdCoinsAndSiteUser(BimarApiCoins idCoins, BimarSiteUsers siteUser);
    
}

