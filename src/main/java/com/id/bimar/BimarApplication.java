package com.id.bimar;

import com.id.bimar.util.Format;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BimarApplication {
    
    private static final Logger log = LoggerFactory.getLogger(BimarApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BimarApplication.class, args);
    }

    public void run(String... strings) throws Exception {       
        log.info("Look in application.yml for the  directory path. Files will be created every 30 seconds.");
    }
}
