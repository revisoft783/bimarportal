package com.id.bimar.util;

import com.id.bimar.dao.BAdminSiteUserRepository;
import com.id.bimar.dao.BApiCoinsRepository;
import com.id.bimar.dao.BSiteUserBalanceRepository;
import com.id.bimar.entity.BimarApiCoins;
import com.id.bimar.entity.BimarSiteUsers;
import com.id.bimar.entity.BimarSiteUsersBalances;
import java.math.BigDecimal;
import java.security.MessageDigest;
import org.springframework.mail.MailSender;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import me.joshmcfarlin.CryptoCompareAPI.Market;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author yusril
 */
public class Utilities {

    @Autowired
    private BAdminSiteUserRepository userRepository;
    
    @Autowired
    private BApiCoinsRepository coinsRepository;
    
    @Autowired
    private BSiteUserBalanceRepository balanceRepository;
    /**
     * @param password
     * @return String password encrypted
     */
    public String setEncrypt(String password) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error saat enkripsi password");
        }
        return generatedPassword;
    }

    private void sendHtmlEmail(String host, String port,
            final String userName, final String password, String toAddress,
            String subject, String message) throws AddressException,
            MessagingException {

        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };

        Session session = Session.getInstance(properties, auth);

        // creates a new e-mail message
        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());
        // set plain text message
        msg.setContent(message, "text/html");

        // sends the e-mail
        Transport.send(msg);

    }

    public void sendMail(MailSender mailSender, String toEmail,
            String fromEmail, String subject, String sentence) {
        // SMTP server information
        String host = "smtp.gmail.com";
        String port = "587";
        String mailFrom = "trdcrpt@gmail.com";
        String password = "Oraumum1!";

        try {
            sendHtmlEmail(host, port, mailFrom, password, toEmail,
                    subject, sentence);
            System.out.println("Email sent.");
        } catch (Exception ex) {
            System.out.println("Failed to sent email.");
            ex.printStackTrace();
        }
    }
    
    public BigDecimal getPriceConvert(String fromCcy,String toCcy){
        BigDecimal amount = BigDecimal.ZERO;
        if(fromCcy.isEmpty()){
            fromCcy = "BTC";
        }
        try {
            amount = new BigDecimal(Market.getPrice(fromCcy, toCcy).get(toCcy));
        } catch (Exception e) {
            System.out.println("Failed to convert.");
            e.printStackTrace();
            amount = BigDecimal.ZERO;
        }
        return amount;
    }
    
    /**
     * @param email
     * @param coinName
     * @return real time balance from API(User)
     */
    public BigDecimal getLastBalance(String email, String coinName) {
        BigDecimal balance = BigDecimal.ZERO;
        BimarSiteUsers user = null;
        BimarApiCoins coin = null;
        try {
            user = userRepository.findByEmail(email);
            coin = coinsRepository.findByName(coinName);
            if (null != user && null != coin) {
                //cari dulu berdasarkan nama user
                List<BimarSiteUsersBalances> userBalances = balanceRepository.findAllBySiteUser(user.getId());
                if (null != userBalances) {
                    for (BimarSiteUsersBalances bal : userBalances) {
                        //cari balance yang cocok dengan inputan coin user
                        if (bal.getCoinid().equals(coin.getId())) {
                            balance = new BigDecimal(bal.getBalance());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            balance = BigDecimal.ZERO;
            System.err.println("error getLastBalance() class DepositController");
        }
        return balance;
    }
}
