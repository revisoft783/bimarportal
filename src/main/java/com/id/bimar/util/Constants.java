/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.util;

/**
 *
 * @author afes
 */
public class Constants {
    public static final String BASE_URL_YOBIT = "https://yobit.io/en/api/";
    public static final String BASE_URL_COINMARKETCAP = "https://api.coinmarketcap.com/v1/";
    public static final String API_VERSION = "3";
    public static final String API_VERSION_COINMARKET = "v1";

    public static class Methods {                
        public static final String GET_TICKER = "get_ticker";
        public static final String GET_INFO = "get_info";
        public static final String GET_DEPTH = "get_depth";
        public static final String GET_TRADES = "get_trades";
    }

    public static class Params {
        public static final String API_KEY = "api_key";
        public static final String LABEL = "label";
        public static final String LABELS = "labels";
        public static final String ADDRS = "addresses";
        public static final String FROM_LABELS = "from_labels";
        public static final String TO_LABELS = "to_labels";
        public static final String FROM_ADDRS = "from_addresses";
        public static final String TO_ADDRS = "to_addresses";
        public static final String FROM_USERIDS = "from_user_ids";
        public static final String TO_USERIDS = "to_user_ids";
        public static final String AMOUNTS = "amounts";
        public static final String PIN = "pin";
        public static final String SIG_DATA = "signature_data";

        public static final String PRICE_BASE = "price_base";
        public static final String TX_IDS = "transaction_ids";
        public static final String TYPE = "type";
        public static final String BEFORE_TX = "before_tx";
        public static final String USER_IDS = "user_ids";

    }

    public static class Values {
        public static final String TYPE_SENT = "sent";
        public static final String TYPE_RECEIVED = "received";
    }

    public static String buildUri(String method) {
        return buildUri("",method, false);
    }
    
    public static String buildUriYobit(String method) {
        return buildUri(BASE_URL_YOBIT,method, false);
    }
    
    public static String buildUriCoinMarket(String method) {
        return buildUri(BASE_URL_COINMARKETCAP,method, false);
    }

    public static String buildUri(String BaseURL, String method, boolean removeTrailingSlash) {
        String s = BaseURL + API_VERSION + "/" + method;
        return removeTrailingSlash ? s : s + "/";
    }
}
