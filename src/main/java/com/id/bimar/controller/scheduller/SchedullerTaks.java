/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.controller.scheduller;

import com.id.bimar.controller.MarketController;
import com.id.bimar.entity.BimarApiMcoinsChart;
import java.time.format.DateTimeFormatter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import me.joshmcfarlin.CryptoCompareAPI.Utils.OutOfCallsException;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author afes
 */
@Component
public class SchedullerTaks {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(SchedullerTaks.class);
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    
    @Autowired
    MarketController marketController;
    
    // set this to false to disable this job; set it it true by
    @Value("${example.scheduledJob.enabled:false}")
    private boolean scheduledJobEnabled;

//	@Value("${example.incoming.comments.dir}")
//	private String commentsDir;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
            "HH:mm:ss");

    @Scheduled(fixedRate = 86400000)  // every one days
    public void scheduleTaskWithFixedRate() throws OutOfCallsException {
//        if (!scheduledJobEnabled) {
//            return;
//        }
//        int min = 1;
//        int max = 500;
//        Random r = new Random();
//        int id = r.nextInt((max - min) + 1) + min;
//		String outFileName = createDataFile(id);
//        RestTemplate restTemplate = new RestTemplate();
        // pull data from example REST API
        //BimarApiMcoinsChart chart = restTemplate.getForObject("http://localhost:9080/api/generateAvg", BimarApiMcoinsChart.class);
        marketController.getGenerateAvg();
//        log.info("Pulled chart #" + chart + " at " + dateFormat.format(new Date()));
//		log.info("Writing to " + outFileName);

//		 try(PrintStream ps = new PrintStream(outFileName)) { ps.println( chart.toString()); }
//		 catch (FileNotFoundException e) {
//			 log.error("Couldn't write comment file.", e);
//		 }
    }

//	private String createDataFile(int id) {
//
//		File dir = new File(commentsDir);
//		if (!dir.exists()) {
//			try {
//				dir.mkdir();
//			} catch (SecurityException se) {
//				throw se;
//			}
//		}
//
//		String name = "comment_" + String.format("%1$03d.", id)
//				+ new Date().getTime();
//		return dir.getAbsolutePath() + File.separator + name;
//	}
    
    // examples of other CRON expressions
    // * "0 0 * * * *" = the top of every hour of every day.
    // * "*/10 * * * * *" = every ten seconds.
    // * "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
    // * "0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30 and 10 o'clock every day.
    // * "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
    // * "0 0 0 25 12 ?" = every Christmas Day at midnight
    
//    @Scheduled(fixedDelay = 2000)
//    public void scheduleTaskWithFixedDelay() {
//        log.info("Fixed Delay Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
//        try {
//            TimeUnit.SECONDS.sleep(5);
//        } catch (InterruptedException ex) {
//            log.error("Ran into an error {}", ex);
//            throw new IllegalStateException(ex);
//        }
//    }
//
//    @Scheduled(fixedRate = 2000, initialDelay = 5000)
//    public void scheduleTaskWithInitialDelay() {
//        log.info("Fixed Rate Task with Initial Delay :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
//    }
//
//    @Scheduled(cron = "0 * * * * ?")
//    public void scheduleTaskWithCronExpression() {
//        log.info("Cron Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
//    }

}
