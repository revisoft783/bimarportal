/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.controller;

import com.id.bimar.dao.BAdminSiteUserRepository;
import com.id.bimar.dao.BApiCoinsRepository;
import com.id.bimar.dao.BAppConfigRepository;
import com.id.bimar.dao.BRequestsRepository;
import com.id.bimar.dao.BSiteUserBalanceRepository;
import com.id.bimar.entity.BimarApiCoins;
import com.id.bimar.entity.BimarAppConfig;
import com.id.bimar.entity.BimarRequests;
import com.id.bimar.entity.BimarSiteUsers;
import com.id.bimar.entity.BimarSiteUsersBalances;
import com.id.bimar.entity.CustomJsonClass;
import com.id.bimar.util.Utilities;
import io.block.api.utils.BlockIOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author afes
 */
@RestController
@RequestMapping("/api")
public class WithdrawController {
    
    @Autowired
    private BRequestsRepository requestsRepository;
    
    @Autowired
    private BAppConfigRepository configRepository;
    
    @Autowired
    private BAdminSiteUserRepository userRepository;
    
    @Autowired
    private BApiCoinsRepository coinsRepository;
    
    @Autowired
    private BSiteUserBalanceRepository balanceRepository;
    /**     
     * @param email
     * @param namaCoins
     * @return 
     */
    private String getWithdrawByUsers(String email, String namaCoins, Double amtWithdraw, String addressWd) {
        BimarSiteUsers bsu = userRepository.findByEmail(email);
        BimarApiCoins bac = coinsRepository.findByName(namaCoins);
        BigDecimal lastBalance = BigDecimal.ZERO;
        String msg="";
        SimpleDateFormat sd = new SimpleDateFormat("yyyymm");
        // Ambil last balance terakhir dari database        
        BimarSiteUsersBalances balances = balanceRepository.findBySiteUserAndCoinid(bsu.getId(), bac.getId());
        if (balances!=null) {
            try {
           lastBalance = BigDecimal.valueOf(balances.getBalance());
           if (amtWithdraw > lastBalance.doubleValue()) {
               msg ="Not enough balance";
               return msg;
           }
           BigDecimal fixFee = BigDecimal.ZERO;
           BigDecimal fixFeeHtg = BigDecimal.ZERO;
           Optional<BimarAppConfig> config = configRepository.findById(1);
           if (null != config) {
               fixFee = new BigDecimal(config.get().getFiatWithdrawFee());
               fixFeeHtg = BigDecimal.valueOf((amtWithdraw * fixFee.doubleValue() / 100));
           }
           
            BimarRequests newRequest = new BimarRequests();
            newRequest.setSendAddress(addressWd);
            newRequest.setCurrency(bac.getCurrencyid());
            newRequest.setSiteUser(bsu.getId());
            newRequest.setCryptoId(bac.getId());
            newRequest.setFee(fixFeeHtg.doubleValue());
            newRequest.setAmount(amtWithdraw);
            newRequest.setNetAmount(amtWithdraw - fixFeeHtg.doubleValue());
            newRequest.setDate(new Date());
            newRequest.setRequestType(1);
            newRequest.setRequestStatus(4);
            newRequest.setTransactionId(sd.format(new Date())+"-WD");
            requestsRepository.save(newRequest);
            
            
            msg = "Withdraw success with status awaiting authorization";
            }catch(Exception ex) {
                msg = "Withdraw unsuccess";
                ex.printStackTrace();
            }
        }else{
            msg ="Not enough balance";
        }
        return msg;
    }
    
    @RequestMapping(value = "/withdraw", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<?> getWithdrawUsers(@RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "namaCoins", defaultValue = "") String namaCoins,
            @RequestParam(value = "amount", defaultValue = "") String amount,
            @RequestParam(value = "addressWd", defaultValue = "") String addressWd) throws BlockIOException {                        
        String msgWithdraw = getWithdrawByUsers(email, namaCoins, Double.valueOf(amount), addressWd);
        return new ResponseEntity<Object>(new CustomJsonClass("withdraw", msgWithdraw), HttpStatus.OK);
    }
}
