package com.id.bimar.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.id.bimar.dao.BAdminSiteUserRepository;
import com.id.bimar.dao.BRequestsRepository;
import com.id.bimar.dao.BTransactionRepository;
import com.id.bimar.entity.BimarRequests;
import com.id.bimar.entity.BimarSiteUsers;
import com.id.bimar.entity.BimarTransactions;
import com.id.bimar.entity.CustomHistoryJsonClass;
import com.id.bimar.entity.CustomJsonClass;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author afes
 */
@RestController
@RequestMapping("/api")
public class HistoryController {

    @Autowired
    private BTransactionRepository trRepository;
    
    @Autowired
    private BRequestsRepository reqRepository;
    
    @Autowired
    private BAdminSiteUserRepository userRepository;
    
    @RequestMapping(value = "/history", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<Object> getHistory() {
        List<BimarTransactions> trList = trRepository.findAll();
        List<CustomHistoryJsonClass> jsonClassList = new ArrayList<CustomHistoryJsonClass>();
        int loop = 0;
        try {
            for (BimarTransactions tmp : trList) {
                if (loop == 100) {
                    return new ResponseEntity<Object>(jsonClassList, HttpStatus.OK);
                }
                CustomHistoryJsonClass jsonClass = new CustomHistoryJsonClass();
                jsonClass.setFromCoin(tmp.getConvertFromCurrency());
                jsonClass.setToCoin(tmp.getConvertToCurrency());
                if (tmp.getTransactionType() == 1) {
                    jsonClass.setType("Buy");
                }else if(tmp.getTransactionType() == 2){
                    jsonClass.setType("Sell");
                }
                jsonClass.setQty(new BigDecimal(tmp.getBtc()));
                jsonClass.setPrice(new BigDecimal(tmp.getBtcPrice()));
                jsonClass.setAmount(new BigDecimal(tmp.getBtcNet()));
                jsonClass.setTrdatetime(tmp.getDate());
                jsonClassList.add(jsonClass);

                loop++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error getHistory() class HistoryController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "get history error"), HttpStatus.OK);
        }

        return new ResponseEntity<Object>(jsonClassList, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/history/pending", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<Object> getHistoryPending() {
        List<BimarRequests> trList = reqRepository.findAll();
        List<CustomHistoryJsonClass> jsonClassList = new ArrayList<CustomHistoryJsonClass>();
        int loop = 0;
        try {
            for (BimarRequests tmp : trList) {
                if (loop == 100) {
                    return new ResponseEntity<Object>(jsonClassList, HttpStatus.OK);
                }
                CustomHistoryJsonClass jsonClass = new CustomHistoryJsonClass();
                jsonClass.setFromCoin(tmp.getFcurrency());
                jsonClass.setToCoin(tmp.getTcurrency());
                if (tmp.getRequestType() == 1 && tmp.getRequestStatus()== 1) {
                    jsonClass.setType("Unsettled Buy");
                }else if (tmp.getRequestType() == 2 && tmp.getRequestStatus()== 1) {
                    jsonClass.setType("Unsettled Sell");
                }
                jsonClass.setQty(new BigDecimal(tmp.getAmount()));
                jsonClass.setPrice(new BigDecimal(tmp.getLastPrice()));
                jsonClass.setAmount(new BigDecimal(tmp.getNetAmount()));
                jsonClass.setTrdatetime(tmp.getDate());
                jsonClassList.add(jsonClass);

                loop++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error getHistory() class HistoryController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "get history error"), HttpStatus.OK);
        }

        return new ResponseEntity<Object>(jsonClassList, HttpStatus.OK);
    }
    
    private List<BimarRequests> listDepositUsers(int userid){
        return reqRepository.findBySiteUserAndRequestType(userid, 2);
    }
    
    private List<BimarRequests> listWithdrawUsers(int userid){
        return reqRepository.findBySiteUserAndRequestType(userid, 1);
    }
    
    @RequestMapping(value = "/history/deposit", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<Object> getHistoryDeposit(@RequestParam(value = "users", defaultValue = "") String users ) {
        BimarSiteUsers bsu = userRepository.findByUsers(users);
        List<CustomHistoryJsonClass> jsonClassList = new ArrayList<CustomHistoryJsonClass>();
        int loop = 0;
        try {
            List<BimarRequests> brs = listDepositUsers(bsu.getId());
            if (brs!=null) {
                for (BimarRequests tmp : brs) {
                    if (loop == 100) {
                        return new ResponseEntity<Object>(jsonClassList, HttpStatus.OK);
                    }
                    CustomHistoryJsonClass jsonClass = new CustomHistoryJsonClass();
                    jsonClass.setFromCoin(tmp.getCryptoId());
                    jsonClass.setToCoin(0);
                    jsonClass.setType("Deposit");
                    jsonClass.setQty(BigDecimal.ZERO);
                    jsonClass.setPrice(BigDecimal.ZERO);
                    jsonClass.setAmount(new BigDecimal(tmp.getNetAmount()));
                    jsonClass.setTrdatetime(tmp.getDate());
                    jsonClassList.add(jsonClass);

                    loop++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error getHistoryDeposit() class HistoryController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "get history deposit error"), HttpStatus.OK);
        }

        return new ResponseEntity<Object>(jsonClassList, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/history/withdraw", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<Object> getHistoryWithdraw(@RequestParam(value = "users", defaultValue = "") String users ) {
        BimarSiteUsers bsu = userRepository.findByUsers(users);
        List<CustomHistoryJsonClass> jsonClassList = new ArrayList<CustomHistoryJsonClass>();
        int loop = 0;
        try {
            List<BimarRequests> brs = listWithdrawUsers(bsu.getId());
            if (brs!=null) {
                for (BimarRequests tmp : brs) {
                    if (loop == 100) {
                        return new ResponseEntity<Object>(jsonClassList, HttpStatus.OK);
                    }
                    CustomHistoryJsonClass jsonClass = new CustomHistoryJsonClass();
                    jsonClass.setFromCoin(tmp.getCryptoId());
                    jsonClass.setToCoin(0);
                    jsonClass.setType("Deposit");
                    jsonClass.setQty(BigDecimal.ZERO);
                    jsonClass.setPrice(BigDecimal.ZERO);
                    jsonClass.setAmount(new BigDecimal(tmp.getNetAmount()));
                    jsonClass.setTrdatetime(tmp.getDate());
                    jsonClassList.add(jsonClass);

                    loop++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error getHistoryDeposit() class HistoryController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "get history withdraw error"), HttpStatus.OK);
        }

        return new ResponseEntity<Object>(jsonClassList, HttpStatus.OK);
    }
}
