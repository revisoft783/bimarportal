/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.id.bimar.dao.BApiCoinsRepository;
import com.id.bimar.dao.BMCoinChartRepository;
import com.id.bimar.dao.BMCoinRepository;
import com.id.bimar.entity.BimarApiCoins;
import com.id.bimar.entity.BimarApiMcoins;
import com.id.bimar.entity.BimarApiMcoinsChart;
import com.id.bimar.util.Format;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import me.joshmcfarlin.CryptoCompareAPI.Market;
import me.joshmcfarlin.CryptoCompareAPI.Utils.OutOfCallsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author afes
 */
@RestController
@RequestMapping("/api")
public class MarketController {
    
    private static DecimalFormat df2 = new DecimalFormat(".########");
    
    @Autowired
    private BMCoinRepository repository;
    @Autowired
    private BMCoinRepository mCoinRepository;
    @Autowired
    private BMCoinChartRepository chartRepository;
    @Autowired
    private BApiCoinsRepository coinRepository;

    @RequestMapping(value = "ticker/btc", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<Object> getTickerBTC() throws OutOfCallsException {
        try {
            return new ResponseEntity<Object>(findTickerMastersBTC(), HttpStatus.OK);
        } catch (IOException ex) {
            Logger.getLogger(MarketController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @RequestMapping(value = "ticker/doge", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<Object> getTickerDoge() throws OutOfCallsException {
        try {
            return new ResponseEntity<Object>(findTickerMastersDoge(), HttpStatus.OK);
        } catch (IOException ex) {
            Logger.getLogger(MarketController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @RequestMapping(value = "ticker/ltc", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<Object> getTickerLtc() throws OutOfCallsException {
        try {
            return new ResponseEntity<Object>(findTickerMastersLTC(), HttpStatus.OK);
        } catch (IOException ex) {
            Logger.getLogger(MarketController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private List<BimarApiMcoins> findTickerMastersBTC() throws OutOfCallsException, IOException {
        return mCoinRepository.findByIdCoins(new BimarApiCoins(1));
    }

    private List<BimarApiMcoins> findTickerMastersDoge() throws OutOfCallsException, IOException {
        return mCoinRepository.findByIdCoins(new BimarApiCoins(2));
    }

    private List<BimarApiMcoins> findTickerMastersLTC() throws OutOfCallsException, IOException {
        return mCoinRepository.findByIdCoins(new BimarApiCoins(3));
    }

    /**
     * GetMarket from Api Crytocompare for generateAvg fsym = from coin tsym =
     * to coin e = market extraparams = bimar.io
     */
    private List<String> generateAvg(String fSym, String tSym, String market, String extraParams) throws IOException, OutOfCallsException {
        Market.ExchangeAverage exchangeAverage = Market.getExchangeAverage(fSym, tSym, market);
        List<String> sortedValueList = null;
        Map<String, String> map = new HashMap();
        df2.setRoundingMode(RoundingMode.UP);
        if (null != exchangeAverage) {
            BimarApiMcoinsChart chart = chartRepository.findByLastUpdate(exchangeAverage.lastUpdate);
            if (null == chart) {
                chart = new BimarApiMcoinsChart();
                chart.setFromcoins(exchangeAverage.fromSymbol);
                chart.setTocoins(exchangeAverage.toSymbol);                
                chart.setLastPrice(exchangeAverage.price);
                chart.setLastUpdate(exchangeAverage.lastUpdate);
                chart.setLastVolume(exchangeAverage.lastVolume);
                chart.setLastVolumeTo(exchangeAverage.lastVolumeTo);
                chart.setLastTradeId(exchangeAverage.lastTradeID);
                chart.setVolume24h(exchangeAverage.volume24Hour);
                chart.setVolume24hto(exchangeAverage.volume24HourTo);                   
                chart.setOpen24h(exchangeAverage.open24Hour);
                chart.setHight24hto(exchangeAverage.high24Hour);
                chart.setLow24hto(exchangeAverage.low24Hour);
                chart.setChange24h(exchangeAverage.change24Hour);
                chart.setChangePct24h(exchangeAverage.changePCT24Hour);
                chart.setLastModified(new Date());
                chartRepository.save(chart);
            }

            map.put("fromSymbol", exchangeAverage.fromSymbol);
            map.put("market", exchangeAverage.market);
            map.put("lastMarket", exchangeAverage.lastMarket);
            map.put("confirmations", exchangeAverage.lastTradeID);
            map.put("toSymbol", exchangeAverage.toSymbol);
            map.put("change24Hour", String.valueOf(exchangeAverage.change24Hour));
            map.put("changeDay", String.valueOf(exchangeAverage.changeDay));
            map.put("changePCT24Hour", String.valueOf(exchangeAverage.changePCT24Hour));
            map.put("changePCTDay", String.valueOf(exchangeAverage.changePCTDay));
            map.put("flags", String.valueOf(exchangeAverage.flags));
            map.put("high24Hour", String.valueOf(exchangeAverage.high24Hour));
            map.put("lastUpdate", String.valueOf(exchangeAverage.lastUpdate));
            map.put("lastVolume", String.valueOf(exchangeAverage.lastVolume));
            map.put("lastVolumeTo", String.valueOf(exchangeAverage.lastVolumeTo));
            map.put("low24Hour", String.valueOf(exchangeAverage.low24Hour));
            map.put("volume24Hour", String.valueOf(exchangeAverage.volume24Hour));
            map.put("volume24HourTo", String.valueOf(exchangeAverage.volume24HourTo));
        }

        sortedValueList = map.values().stream()
                .sorted().collect(Collectors.toList());
        return sortedValueList;
    }

    //@RequestMapping(value = "generateAvg", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public void getGenerateAvg() throws OutOfCallsException {
        try {
            List<BimarApiCoins> fromCoinList = coinRepository.findAll();
            if (!fromCoinList.isEmpty()) {
                for (BimarApiCoins fCoin : fromCoinList) {
                    List<BimarApiCoins> toCoinList = coinRepository.findAll();
                    if (!toCoinList.isEmpty()) {
                        for (BimarApiCoins toCoin : toCoinList) {
                            if (!fCoin.getName().equalsIgnoreCase(toCoin.getName())) {
                                generateAvg(fCoin.getName(), toCoin.getName(), "Yobit", "Bimar.io");
                                getMarket(fCoin.getName(), toCoin.getName());
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            System.err.println("error generateAvg() class MarketController");
            Logger.getLogger(MarketController.class.getName()).log(Level.SEVERE, null, ex);            
        }        
    }

    /*
     * Kebutuhan untuk chart ambil dari api BimarApiMcoinsChart
     */
    private List<BimarApiMcoinsChart> getBimarApiMcoinsChartEntities(String fSym, String tSym) throws OutOfCallsException, IOException {
        return chartRepository.findByFromcoinsAndTocoins(fSym, tSym);
    }

    //@RequestMapping(value = "marketPair", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<Object> getMarket(@RequestParam(value = "fsym", defaultValue = "") String fsym,
            @RequestParam(value = "tsym", defaultValue = "") String tsym) throws OutOfCallsException {
        String chatToJson = null;
        ObjectMapper mapper = new ObjectMapper();
        //Set pretty printing of json
    	mapper.enable(SerializationFeature.INDENT_OUTPUT);
        Gson gson = new Gson();        
        String path = "";
        List<BimarApiMcoinsChart> chartList = null;
        try {
            chartList = getBimarApiMcoinsChartEntities(fsym, tsym);            
            chatToJson = gson.toJson(chartList);
            //2. Convert JSON to List of BimarApiMcoinsChart objects
            //Define Custom Type reference for List<BimarApiMcoinsChart> type
            String arrayToJson = mapper.writeValueAsString(chartList);
            TypeReference<List<BimarApiMcoinsChart>> mapType = new TypeReference<List<BimarApiMcoinsChart>>() {};
            List<BimarApiMcoinsChart> jsonToPersonList = mapper.readValue(arrayToJson, mapType);
            System.out.println("\n2. Convert JSON to List of BimarApiMcoinsChart objects :"+jsonToPersonList.toString());
            System.out.println(System.getProperty("os.name"));
            if ("Windows 7".equals(System.getProperty("os.name"))) {                
                if ("LTC".equals(fsym) && "BTC".equals(tsym)) {
                    path = "C:/Users/public/data_LTC_BTC.json";
                }else if ("LTC".equals(fsym) && "DOGE".equals(tsym)) {
                    path = "C:/Users/public/data_LTC_DOGE.json";
                }else if ("DOGE".equals(fsym) && "LTC".equals(tsym)) {
                    path = "C:/Users/public/data_DOGE_LTC.json";
                }else if ("DOGE".equals(fsym) && "BTC".equals(tsym)) {
                    path = "C:/Users/public/data_DOGE_BTC.json";
                }else if ("BTC".equals(fsym) && "LTC".equals(tsym)) {
                    path = "C:/Users/public/data_BTC_LTC.json";
                }else if ("BTC".equals(fsym) && "DOGE".equals(tsym)) {
                    path = "C:/Users/public/data_BTC_DOGE.json";
                }else{
                    path = "C:/Users/public/file1.json";
                }
            } else{
                if ("LTC".equals(fsym) && "BTC".equals(tsym)) {
                    path = "/var/www/html/assets/json/data_LTC_BTC.json";
                }else if ("LTC".equals(fsym) && "DOGE".equals(tsym)) {
                    path = "/var/www/html/assets/json/data_LTC_DOGE.json";
                }else if ("DOGE".equals(fsym) && "LTC".equals(tsym)) {
                    path = "/var/www/html/assets/json/data_DOGE_LTC.json";
                }else if ("DOGE".equals(fsym) && "BTC".equals(tsym)) {
                    path = "/var/www/html/assets/json/data_DOGE_BTC.json";
                }else if ("BTC".equals(fsym) && "LTC".equals(tsym)) {
                    path = "/var/www/html/assets/json/data_BTC_LTC.json";
                }else if ("BTC".equals(fsym) && "DOGE".equals(tsym)) {
                    path = "/var/www/html/assets/json/data_BTC_DOGE.json";
                }else{
                    path = "/home/bimar/Documents/file1.json";
                }
            } 
            
            try (
                FileWriter file = new FileWriter(path)) {
                file.write(jsonToPersonList.toString());
                System.out.println("Successfully Copied JSON Object to File...");
//                System.out.println("\nJSON Object: " + jObj);
            }
        } catch (Exception ex) {
            Logger.getLogger(MarketController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ResponseEntity<Object>(chatToJson, HttpStatus.OK);
    }
    
    
}
