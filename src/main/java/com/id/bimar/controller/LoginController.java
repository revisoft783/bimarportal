package com.id.bimar.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.id.bimar.dao.BAdminSiteUserRepository;
import com.id.bimar.entity.BimarSiteUsers;
import com.id.bimar.entity.CustomJsonClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.id.bimar.util.Utilities;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * @author yusril
 */
@RestController
public class LoginController {

    @Autowired
    private BAdminSiteUserRepository service;
    private ObjectMapper mapper = null;

    @RequestMapping("/api/login")
    public ResponseEntity<Object> getLogin(@RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "pass", defaultValue = "") String pass) {

        BimarSiteUsers pengguna = service.findByEmail(email);

        if (null != pengguna) {
            if (0 == pengguna.getVerifiedAuthy()) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", "User Not Yet Verified"), HttpStatus.OK);
            }

            if (new Utilities().setEncrypt(pass).equalsIgnoreCase(pengguna.getPass())) {
                return new ResponseEntity<Object>(pengguna, HttpStatus.OK);
            } else {
                return new ResponseEntity<Object>(new CustomJsonClass("error", "Password Error !"), HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<Object>(new CustomJsonClass("error", "User Not Found !"), HttpStatus.OK);
        }
    }

}
