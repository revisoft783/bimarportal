package com.id.bimar.controller;

import com.id.bimar.dao.BAdminSiteUserRepository;
import com.id.bimar.dao.BApiAddressRepository;
import com.id.bimar.dao.BApiCoinsRepository;
import com.id.bimar.dao.BHAffliateRepository;
import com.id.bimar.entity.BimarApiAddress;
import com.id.bimar.entity.BimarApiCoins;
import com.id.bimar.entity.BimarSiteUsers;
import com.id.bimar.entity.BimarSiteUsersBalances;
import com.id.bimar.entity.CustomJsonClass;
import com.id.bimar.util.Utilities;
import io.block.api.BlockIO;
import io.block.api.model.NewAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.mail.MailSender;
import org.springframework.web.bind.annotation.RestController;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.id.bimar.dao.BSiteUserBalanceRepository;
import com.id.bimar.entity.BimarHistoryAffliate;

/**
 * @author yusril
 */
@RestController
public class RegisterController {

    @Autowired
    private BAdminSiteUserRepository userRepository;
    @Autowired
    private BApiCoinsRepository coinRepository;
    @Autowired
    private BApiAddressRepository addressRepository;
    @Autowired
    private BSiteUserBalanceRepository userbalanceRepository;
    @Autowired
    private MailSender mailSender;
    @Autowired
    private BHAffliateRepository affliateRepo;
    
    private final String setInsertAddress(BimarSiteUsers user) {
        String result = "success";
        List<BimarApiCoins> listOfCoins = null;
        try {
            listOfCoins = coinRepository.findAll();
            if (!listOfCoins.isEmpty()) {
                for (BimarApiCoins coins : listOfCoins) {
                    BlockIO blockIO = new BlockIO(coins.getKey());
                    System.out.println(blockIO);
                    if (null != blockIO) {
                        NewAddress address = blockIO.getNewAddress("user" + user.getUsers());
                        if (null != address) {
                            //simpan ke address berdasarkan coins
                            BimarApiAddress newAddress = new BimarApiAddress();
                            newAddress.setIdCoins(new BimarApiCoins(coins.getId()));
                            newAddress.setSiteUser(new BimarSiteUsers(user.getId()));
                            newAddress.setAddress(address.address);
                            newAddress.setLabel("user" + user.getUsers());
                            addressRepository.save(newAddress);

                            //simpan ke site users balance
                            BimarSiteUsersBalances usersBalances =  new BimarSiteUsersBalances();
                            usersBalances.setBalance(0.0);
                            usersBalances.setSiteUser(user.getId());
                            usersBalances.setSiteUser(user.getId());
                            usersBalances.setCoinid(coins.getId());
                            userbalanceRepository.save(usersBalances);
                        }
                    }
                }
            } else {
                result = "coins not found";
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = "failed";
        }
        return result;
    }

    @RequestMapping("/api/register")
    public ResponseEntity<Object> getRegister(@RequestParam(value = "user", defaultValue = "") String user,
            @RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "password", defaultValue = "") String password) {
        String token = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        Date expDate = new Date();
        BimarSiteUsers newUser = null;
        try {
            //Pencegahan request dari Adit jika di refresh berkali2
            if (!"".equalsIgnoreCase(user) && !"".equalsIgnoreCase(email)) {
                newUser = new BimarSiteUsers();
                newUser.setUsers(user);
                newUser.setEmail(email);
                newUser.setPass(new Utilities().setEncrypt(password.trim()));
                newUser.setIsAdmin(new Short("0"));
                newUser.setVerifiedAuthy(new Short("0"));
                newUser.setAuthyId("");
                newUser.setDateAuthyId(expDate);
                newUser.setAuthyRequested(new Short("0"));
                newUser.setFirstName(user);
                newUser.setLastName(user);
                newUser.setWebsite("");
                newUser.setAddress("");
                newUser.setCity("");
                newUser.setPhone("");
                newUser.setCountryCode(0);
                newUser.setAffliate(0);
                //cek apakah user sudah ada
                BimarSiteUsers exists = userRepository.findByEmail(email);
                if (null != exists) {
                    //jika email ada dan sudah verifikasi
                    if (exists.getEmail().equalsIgnoreCase(email)) {
                        System.out.println("tanggal sekarang : " + sdf.format(new Date()));
                        System.out.println("tanggal authentikasi : " + sdf.format(exists.getDateAuthyId()));
                        if (new Short("0").equals(exists.getVerifiedAuthy())
                                && !sdf.format(new Date()).equalsIgnoreCase(sdf.format(exists.getDateAuthyId()))) {
                            return new ResponseEntity<Object>(new CustomJsonClass("error", "Authentication expired"), HttpStatus.OK);
                        }
                        return new ResponseEntity<Object>(new CustomJsonClass("error", "Email already registered"), HttpStatus.OK);
                    }
                }
                //simpan pengguna
                userRepository.save(newUser);
                //Buat token dan simpan token
                token = Jwts.builder()
                        .setExpiration(expDate)
                        .claim("Email", newUser.getEmail())
                        .signWith(SignatureAlgorithm.HS256, "key".getBytes("UTF-8")).compact();
                new Utilities().sendMail(mailSender, newUser.getEmail(),
                        "trdcrpt@gmail.com", "You Registration Information", "<!DOCTYPE html>\n"
                        + "<html> \n"
                        + "<body> \n"
                        + "<h3>Confirm your email address</h3> \n"
                        + "<a href=\"http://bimar.io/index.php/C_register/verification_process/" + newUser.getEmail() + "/" + token + "\">Confirm</a> \n"
                        + "</body>\n"
                        + "</html>");

                //update token
                newUser.setAuthyId(token);
                userRepository.save(newUser);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error getRegister() class RegisterController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "Register failed"), HttpStatus.OK);
        }
        return new ResponseEntity<Object>(newUser, HttpStatus.OK);
    }

    @RequestMapping("/api/refreshtoken")
    public ResponseEntity<Object> getRefreshtoken(@RequestParam(value = "user", defaultValue = "") String user,
            @RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "password", defaultValue = "") String password) {
        String token = "";
        Date expDate = new Date();
        try {
            //Pencegahan request dari Adit jika di refresh berkali2
            if (!"".equalsIgnoreCase(user) && !"".equalsIgnoreCase(email)) {
                //cek apakah user sudah ada
                BimarSiteUsers exists = userRepository.findByEmail(email);
                if (null != exists) {
                    //perbarui token
                    token = Jwts.builder()
                            .setExpiration(expDate)
                            .claim("Email", exists.getEmail())
                            .signWith(SignatureAlgorithm.HS256, "key".getBytes("UTF-8")).compact();
                    new Utilities().sendMail(mailSender, exists.getEmail(),
                            "trdcrpt@gmail.com", "Request New Token", "<!DOCTYPE html>\n"
                            + "<html> \n"
                            + "<body> \n"
                            + "<h3>Open this <a href=\"http://bimar.io/index.php/verify.html/new_token/"+email+"/"+token+"/" + "\">URL</a> to get your token.</h3> \n"
                            + "</body>\n"
                            + "</html>");
                    exists.setAuthyId(token);
                    exists.setDateAuthyId(expDate);
                    exists.setVerifiedAuthy(new Short("0"));
                    //simpan pengguna
                    userRepository.save(exists);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error getRegister() class RegisterController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "Refresh token failed"), HttpStatus.OK);
        }
        return new ResponseEntity<Object>(token, HttpStatus.OK);
    }

    @RequestMapping("/api/confirm")
    public ResponseEntity<Object> getConfirmToken(@RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "token", defaultValue = "") String token) {
        BimarSiteUsers confirmUser = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        String address = "";
        try {
            confirmUser = userRepository.findByEmail(email);
            if (null != confirmUser) {
                //tidak boleh diverivikasi 2x
                if (confirmUser.getVerifiedAuthy().equals(new Short("1"))) {
                    return new ResponseEntity<Object>(new CustomJsonClass("error", "Already verified !"), HttpStatus.OK);
                }

                //Jika token sama dg email & tanggal tidak kadaluarsa
                if (token.equalsIgnoreCase(confirmUser.getAuthyId())
                        && sdf.format(new Date()).equalsIgnoreCase(sdf.format(confirmUser.getDateAuthyId()))) {
                    //update admin user
                    confirmUser.setVerifiedAuthy(new Short("1"));
                    userRepository.save(confirmUser);
                } else {
                    return new ResponseEntity<Object>(new CustomJsonClass("error", "Token Expired !"), HttpStatus.OK);
                }

                //Proses pembuatan address berdasarkan jumlah coins
                address = setInsertAddress(confirmUser);
                if ("failed".equalsIgnoreCase(address)) {
                    return new ResponseEntity<Object>(new CustomJsonClass("error", "Failed insert address !"), HttpStatus.OK);
                } else if ("coins not found".equalsIgnoreCase(address)) {
                    return new ResponseEntity<Object>(new CustomJsonClass("error", "Coins not found !"), HttpStatus.OK);
                }
            } else {
                return new ResponseEntity<Object>(new CustomJsonClass("error", "User Not Found !"), HttpStatus.OK);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error getConfirmToken() class RegisterController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "Confim Failed !"), HttpStatus.OK);
        }

        return new ResponseEntity<Object>("Verification success !", HttpStatus.OK);
    }

    @RequestMapping(value = "/api/forgetpass", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> getForgetPassword(@RequestParam(value = "email", defaultValue = "") String email) {
        BimarSiteUsers user = null;
        if (!"".equalsIgnoreCase(email)) {
            try {
                user = userRepository.findByEmail(email);
                if (null != user) {

                    user.setAuthyRequested(new Short("1"));
                    userRepository.save(user);
                    new Utilities().sendMail(mailSender, email,
                            "trdcrpt@gmail.com", "Forget Password", "<!DOCTYPE html>\n"
                            + "<html> \n"
                            + "<body> \n"
                            + "<h3>Open this <a href=\"http://bimar.io/index.php/forgot/new_pass.html/" + email + "/" + "\">URL</a> to change your password</h3> \n"
                            + "</body>\n"
                            + "</html>");
                } else {
                    return new ResponseEntity<Object>(new CustomJsonClass("error", "Email does not match !"), HttpStatus.OK);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("error getForgetPassword() class RegisterController");
                return new ResponseEntity<Object>(new CustomJsonClass("error", "Request Error !"), HttpStatus.OK);
            }
        }

        return new ResponseEntity<Object>(new CustomJsonClass("success", "Email has been sent !"), HttpStatus.OK);
    }

    @RequestMapping(value = "/api/createnewpass", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> getCreateNewPass(@RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "newpass", defaultValue = "") String newPass) {
        BimarSiteUsers user = null;
        if (!"".equalsIgnoreCase(email) && !"".equalsIgnoreCase(newPass)) {
            try {
                user = userRepository.findByEmail(email);
                if (null != user) {
                    if (user.getEmail().equalsIgnoreCase(email) && user.getAuthyRequested() == 1) {
                        user.setAuthyRequested(new Short("0"));
                        user.setPass(new Utilities().setEncrypt(newPass.trim()));
                        userRepository.save(user);
                    } else if (user.getAuthyRequested() == 0) {
                        return new ResponseEntity<Object>(new CustomJsonClass("error", "This User not request ne password !"), HttpStatus.OK);
                    }
                } else {
                    return new ResponseEntity<Object>(new CustomJsonClass("error", "User no found !"), HttpStatus.OK);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("error getCreateNewPass() class RegisterController");
                return new ResponseEntity<Object>(new CustomJsonClass("error", "Request Error !"), HttpStatus.OK);
            }
        }

        return new ResponseEntity<Object>("Success !", HttpStatus.OK);
    }
    
    @RequestMapping("/api/register/affliate")
    public ResponseEntity<Object> getRegisterAffliate(@RequestParam(value = "user", defaultValue = "") String user,
            @RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "password", defaultValue = "") String password,
            @RequestParam(value = "affliate", defaultValue = "") String affliate) {
        String token = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        Date expDate = new Date();
        BimarSiteUsers newUser = null;
        try {
            //Pencegahan request dari Adit jika di refresh berkali2
            // ditambahkan parameter email affliate 
            if (!"".equalsIgnoreCase(user) && !"".equalsIgnoreCase(email) && !"".equalsIgnoreCase(affliate)) {
                newUser = new BimarSiteUsers();
                newUser.setUsers(user);
                newUser.setEmail(email);
                newUser.setPass(new Utilities().setEncrypt(password.trim()));
                newUser.setIsAdmin(new Short("0"));
                newUser.setVerifiedAuthy(new Short("0"));
                newUser.setAuthyId("");
                newUser.setDateAuthyId(expDate);
                newUser.setAuthyRequested(new Short("0"));
                newUser.setFirstName(user);
                newUser.setLastName(user);
                newUser.setWebsite("");
                newUser.setAddress("");
                newUser.setCity("");
                newUser.setPhone("");
                newUser.setCountryCode(0);
                newUser.setAffliate(1); //karena beraffliasi, jadi diset 1

                //cek apakah user sudah ada
                BimarSiteUsers exists = userRepository.findByEmail(email);
                if (null != exists) {
                    //jika email ada dan sudah verifikasi
                    if (exists.getEmail().equalsIgnoreCase(email)) {
                        System.out.println("tanggal sekarang : " + sdf.format(new Date()));
                        System.out.println("tanggal authentikasi : " + sdf.format(exists.getDateAuthyId()));
                        if (new Short("0").equals(exists.getVerifiedAuthy())
                                && !sdf.format(new Date()).equalsIgnoreCase(sdf.format(exists.getDateAuthyId()))) {
                            return new ResponseEntity<Object>(new CustomJsonClass("error", "Authentication expired"), HttpStatus.OK);
                        }
                        return new ResponseEntity<Object>(new CustomJsonClass("error", "Email already registered"), HttpStatus.OK);
                    }
                }
                //simpan pengguna
                userRepository.save(newUser);
                
                //simpan bimar_history_affliate
                BimarHistoryAffliate newAffliate = BimarHistoryAffliate();
                newAffliate.setTglTransaksi(new Date());
                newAffliate.setIdUserAfflite(Integer.MIN_VALUE);
                newAffliate.setIdUser(Integer.SIZE);
                affliateRepo.save(newAffliate);
                
                //Buat token dan simpan token
                token = Jwts.builder()
                        .setExpiration(expDate)
                        .claim("Email", newUser.getEmail())
                        .signWith(SignatureAlgorithm.HS256, "key".getBytes("UTF-8")).compact();
                new Utilities().sendMail(mailSender, newUser.getEmail(),
                        "trdcrpt@gmail.com", "You Registration Information", "<!DOCTYPE html>\n"
                        + "<html> \n"
                        + "<body> \n"
                        + "<h3>Confirm your email address</h3> \n"
                        + "<a href=\"http://bimar.io/index.php/C_register/verification_process/" + newUser.getEmail() + "/" + token + "\">Confirm</a> \n"
                        + "</body>\n"
                        + "</html>");

                //update token
                newUser.setAuthyId(token);
                userRepository.save(newUser);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error getRegister() class RegisterController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "Register failed"), HttpStatus.OK);
        }
        return new ResponseEntity<Object>(newUser, HttpStatus.OK);
    }

    private BimarHistoryAffliate BimarHistoryAffliate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
