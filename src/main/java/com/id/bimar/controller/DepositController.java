package com.id.bimar.controller;

import com.id.bimar.dao.BAdminSiteUserRepository;
import com.id.bimar.dao.BApiCoinsRepository;
import com.id.bimar.entity.CustomJsonClass;
import com.id.bimar.dao.BApiAddressRepository;
import com.id.bimar.dao.BAppConfigRepository;
import com.id.bimar.dao.BCurrencyRepository;
import com.id.bimar.dao.BHistoryRepository;
import com.id.bimar.dao.BRequestsRepository;
import com.id.bimar.entity.BimarApiAddress;
import com.id.bimar.entity.BimarApiCoins;
import com.id.bimar.entity.BimarAppConfig;
import com.id.bimar.entity.BimarCurrencies;
import com.id.bimar.entity.BimarHistory;
import com.id.bimar.entity.BimarRequests;
import com.id.bimar.entity.BimarSiteUsers;
import com.id.bimar.entity.BimarSiteUsersBalances;
import com.id.bimar.entity.BimarTransactions;
import com.id.bimar.util.Utilities;
import io.block.api.BlockIO;
import io.block.api.model.AddressByLabel;
import io.block.api.model.Price;
import io.block.api.model.TransactionReceived;
import io.block.api.model.TransactionsReceived;
import io.block.api.utils.BlockIOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailSender;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.id.bimar.dao.BSiteUserBalanceRepository;
import com.id.bimar.dao.BStockCoinsRepository;
import com.id.bimar.dao.BTransactionRepository;
import com.id.bimar.entity.BimarStockCoins;
import com.id.bimar.entity.CustomTransJsonClass;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author afes
 */
@RestController
@RequestMapping("/api")
public class DepositController {

    @Autowired
    private BApiCoinsRepository coinsRepository;
    @Autowired
    private BApiAddressRepository addressRepository;
    @Autowired
    private BAdminSiteUserRepository userRepository;
    @Autowired
    private BSiteUserBalanceRepository balanceRepository;
    @Autowired
    private MailSender mailSender;
    @Autowired
    private BHistoryRepository historyRepository;
    @Autowired
    private BRequestsRepository requestsRepository;
    @Autowired
    private BAppConfigRepository configRepository;
    @Autowired
    private BCurrencyRepository currRepository;
    @Autowired
    private BTransactionRepository trRepository;
    @Autowired
    private BSiteUserBalanceRepository userbalanceRepository;
    @Autowired
    private BStockCoinsRepository stockCoinsRepository;

    @RequestMapping("/deposit/payeer")
    public Callable<ResponseEntity> getAccountPayeer(@RequestParam(value = "account", defaultValue = "") String account,
            @RequestParam(value = "apiId", defaultValue = "") String id,
            @RequestParam(value = "apiPass", defaultValue = "") String pass) {
        return null;
    }

    @RequestMapping(value = "/getDepositAddressUsers", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<?> getDepositAddressUsers(@RequestParam(value = "name", defaultValue = "") String name,
            @RequestParam(value = "users", defaultValue = "") String users) throws BlockIOException {
        String result = null;
        BimarSiteUsers bsu = userRepository.findByUsers(users);
        BimarApiCoins bac = coinsRepository.findByName(name);
        if (bsu != null && bac != null) {
            BimarApiAddress apiAddress = addressRepository.getByIdCoinsAndSiteUser(bac, bsu);
            if (apiAddress != null) {
                BlockIO blockIO = new BlockIO(bac.getKey());
                AddressByLabel AddressByLabel = blockIO.getAddressByLabel(apiAddress.getLabel());
                result = AddressByLabel.address;
                return new ResponseEntity<Object>(new CustomJsonClass("address", result), HttpStatus.OK);
            } else {
                result = "Address Not Found";
                return new ResponseEntity<Object>(new CustomJsonClass("error", result), HttpStatus.NOT_FOUND);
            }
        }
        return new ResponseEntity<Object>(new CustomJsonClass("error", "not found"), HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/getBalanceAddressUsers", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<Object> getBalanceAddressUsers(@RequestParam(value = "name", defaultValue = "") String name,
            @RequestParam(value = "users", defaultValue = "") String users) throws BlockIOException {
        BimarSiteUsers bsu = userRepository.findByUsers(users);
        BimarApiCoins bac = coinsRepository.findByName(name);
        AddressByLabel label = null;
        try {
            BlockIO blockIO = new BlockIO(bac.getKey());
            System.out.println("Block IO : " + blockIO);
            if (null != blockIO) {
                BimarApiAddress apiAddress = addressRepository.getByIdCoinsAndSiteUser(bac, bsu);
                System.out.println("Cek INI : " + apiAddress);
                if (null != apiAddress) {
                    label = blockIO.getAddressByLabel(apiAddress.getLabel());
                    System.out.println("balance baru :" + label.address);
                    getTransReceivedBlockIO(bac, bac.getKey(), label.address, apiAddress.getIdAddress(), bsu);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error getRegister() class RegisterController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "get balance error"), HttpStatus.OK);
        }
        return new ResponseEntity<Object>(new CustomJsonClass("balance", getLastBalance(bsu.getEmail(), bac.getName()).toString()), HttpStatus.NOT_FOUND);
    }

    /**
     * @param email
     * @param coinName
     * @return real time balance from API(User)
     */
    private BigDecimal getLastBalance(String email, String coinName) {
        BigDecimal balance = BigDecimal.ZERO;
        BimarSiteUsers user = null;
        BimarApiCoins coin = null;
        try {
            user = userRepository.findByEmail(email);
            coin = coinsRepository.findByName(coinName);
            if (null != user && null != coin) {
                //cari dulu berdasarkan nama user
                List<BimarSiteUsersBalances> userBalances = balanceRepository.findAllBySiteUser(user.getId());
                if (null != userBalances) {
                    for (BimarSiteUsersBalances bal : userBalances) {
                        //cari balance yang cocok dengan inputan coin user
                        if (bal.getCoinid().equals(coin.getId())) {
                            balance = new BigDecimal(bal.getBalance());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            balance = BigDecimal.ZERO;
            System.err.println("error getLastBalance() class DepositController");
        }
        return balance;
    }

    /**
     * @param coinName
     * @param currency
     * @return real time price from API
     */
    private BigDecimal getLastPrice(String coinName, String currency) {
        BigDecimal lastPrice = BigDecimal.ZERO;
        try {
            BimarApiCoins coin = coinsRepository.findByName(coinName);
            if (null != coin) {
                BlockIO blockIO = new BlockIO(coin.getKey());
                List<Price> listOfPrice = blockIO.getPrices(currency).prices;
                if (!listOfPrice.isEmpty()) {
                    for (Price p : listOfPrice) {
                        if ("BTC".equals(coinName) && "USD".equals(currency)) {
                            if ("coinbase".equalsIgnoreCase(p.exchange)
                                    && currency.equalsIgnoreCase(p.priceBase)) {
                                lastPrice = new BigDecimal(p.price);
                            }
                        }
                        if (("DOGE".equals(coinName) || "LTC".equals(coinName)) && "BTC".equals(currency)) {
                            if ("bittrex".equalsIgnoreCase(p.exchange)
                                    && currency.equalsIgnoreCase(p.priceBase)) {
                                lastPrice = new BigDecimal(p.price);
                            }
                            if ("bitfinex".equalsIgnoreCase(p.exchange)
                                    && currency.equalsIgnoreCase(p.priceBase)) {
                                lastPrice = new BigDecimal(p.price);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            lastPrice = BigDecimal.ZERO;
            System.err.println("error getLastPrice() class DepositController");
        }
        return lastPrice;
    }

    @RequestMapping("/balance")
    public ResponseEntity<Object> getBalanceForPage(@RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "coin", defaultValue = "") String coin) {
        return new ResponseEntity<Object>(getLastBalance(email, coin), HttpStatus.OK);

    }

    @RequestMapping("/price")
    public ResponseEntity<Object> getPriceForPage(@RequestParam(value = "fsym", defaultValue = "") String fsym,
            @RequestParam(value = "tsym", defaultValue = "") String tsym) {
//        return new ResponseEntity<Object>(getLastPrice(coin, currency), HttpStatus.OK);
        Utilities util = new Utilities();
        return new ResponseEntity<Object>(util.getPriceConvert(fsym, tsym), HttpStatus.OK);

    }

    @RequestMapping(value = "/buy", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ResponseEntity<Object> getBuy(@RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "balance", defaultValue = "0") BigDecimal latesBalance,
            @RequestParam(value = "currency", defaultValue = "BTC") String currency,
            @RequestParam(value = "tocurrency", defaultValue = "") String toCurrency,
            @RequestParam(value = "amount", defaultValue = "0") BigDecimal amount) {
        BimarStockCoins bimarStock = null;
        BigDecimal fixBalance = latesBalance;
        BigDecimal buyAmt = amount;
        BigDecimal priceFromThird = BigDecimal.ZERO;
        BigDecimal fixFee = BigDecimal.ZERO;
        BigDecimal grossAmount = BigDecimal.ZERO;
        BigDecimal netAmount = BigDecimal.ZERO;
        NumberFormat formatter = new DecimalFormat("###.########");
        Optional<BimarAppConfig> config = configRepository.findById(1);
        Utilities util = new Utilities();
        if (null != config) {
            fixFee = new BigDecimal(config.get().getCurrencyConversionFee());
        }
        try {
            //pengecekan user
            BimarSiteUsers user = userRepository.findByEmail(email);
            if (null == user) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", "User not found !"), HttpStatus.OK);
            }
            //pengecekan currency default
            BimarCurrencies cry = currRepository.findByCurrency(currency);
            if (null == cry) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", currency + " not found !"), HttpStatus.OK);
            }

            //pengecekan currency tujuan
            BimarCurrencies tocry = currRepository.findByCurrency(toCurrency);
            if (null == tocry) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", toCurrency + " not found !"), HttpStatus.OK);
            }
            //saldo user
            fixBalance = latesBalance;
            //jumlah yang ingin dibeli
            buyAmt = amount;
            //harga terakhir
            priceFromThird = util.getPriceConvert(currency, toCurrency);

            if (buyAmt.doubleValue() > fixBalance.doubleValue()) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", "Not enough balance !"), HttpStatus.OK);
            }

            //perhitungan
            fixBalance = new BigDecimal((fixBalance.doubleValue() - buyAmt.doubleValue()));
            grossAmount = new BigDecimal((buyAmt.doubleValue() * priceFromThird.doubleValue()));
            netAmount = new BigDecimal((grossAmount.doubleValue() * fixFee.doubleValue() / 100));

            //cek stok coin tujuan
            bimarStock = stockCoinsRepository.getByNamaCoins(tocry.getCurrency());
            if (null != bimarStock) {
                //status pending
                if (bimarStock.getAmount() < netAmount.doubleValue() || requestsRepository.getSumPendingBuyNetAmount(netAmount.doubleValue()) == 0) {
                    BimarApiCoins toCoin = coinsRepository.findByName(tocry.getCurrency());
                    if (null != toCoin) {
                        BimarApiAddress address = addressRepository.getByIdCoinsAndSiteUser(toCoin, user);
                        if (null != address) {
                            BimarRequests request = new BimarRequests();
                            request.setDate(new Date());
                            request.setSiteUser(user.getId());
                            request.setCurrency(tocry.getId());
                            request.setAmount(amount.doubleValue());
                            request.setRequestStatus(1);
                            request.setRequestType(3);
                            request.setAddressId(address.getIdAddress());
                            request.setSendAddress(address.getAddress());
                            request.setTransactionId("");
                            request.setDone(Short.valueOf("0"));
                            request.setCryptoId(toCoin.getId());
                            request.setFee(fixFee.doubleValue());
                            request.setNetAmount(netAmount.doubleValue());
                            request.setNotified(0);
                            request.setFcurrency(cry.getId());
                            request.setTcurrency(tocry.getId());
                            request.setLastPrice(priceFromThird.doubleValue());
                            //masuk pending
                            requestsRepository.save(request); 

                            return new ResponseEntity<Object>(new CustomTransJsonClass("pending", "Transaction is pending",
                                    fixFee.doubleValue(), buyAmt.doubleValue(), netAmount.doubleValue(), grossAmount.doubleValue()), HttpStatus.OK);
                        }
                    }
                } else {
                    //simpan ke tabel transaksi
                    BimarTransactions tr = new BimarTransactions();
                    tr.setDate(new Date());
                    tr.setSiteUser(user.getId());
                    tr.setBtc(Double.parseDouble(formatter.format(buyAmt)));
                    tr.setCurrency(tocry.getId());
                    tr.setBtcPrice(Double.parseDouble(formatter.format(priceFromThird)));
                    tr.setFee(fixFee.doubleValue());
                    tr.setTransactionType(1);
                    tr.setBtcNet(Double.parseDouble(formatter.format(netAmount)));
                    tr.setBtcBefore(Double.parseDouble(formatter.format(netAmount)));
                    tr.setBtcAfter(Double.parseDouble(formatter.format(grossAmount)));
                    tr.setConvertFromCurrency(cry.getId());
                    tr.setConvertToCurrency(tocry.getId());
                    tr.setLogId(0);
                    tr.setLogId1(2); // penanda bahwa sell berasal dari transaksi pair
                    trRepository.save(tr);
                }
            } else {
                BimarApiCoins toCoin = coinsRepository.findByName(tocry.getCurrency());
                if (null != toCoin) {
                    BimarApiAddress address = addressRepository.getByIdCoinsAndSiteUser(toCoin, user);
                    if (null != address) {
                        BimarRequests request = new BimarRequests();
                        request.setDate(new Date());
                        request.setSiteUser(user.getId());
                        request.setCurrency(tocry.getId());
                        request.setAmount(amount.doubleValue());
                        request.setRequestStatus(1);
                        request.setRequestType(3);
                        request.setAddressId(address.getIdAddress());
                        request.setSendAddress(address.getAddress());
                        request.setTransactionId("");
                        request.setDone(Short.valueOf("0"));
                        request.setCryptoId(toCoin.getId());
                        request.setFee(fixFee.doubleValue());
                        request.setNetAmount(netAmount.doubleValue());
                        request.setNotified(0);
                        request.setFcurrency(cry.getId());
                        request.setTcurrency(tocry.getId());
                        request.setLastPrice(priceFromThird.doubleValue());
                        //masuk pending
                        requestsRepository.save(request); 

                        return new ResponseEntity<Object>(new CustomTransJsonClass("pending", "Transaction is pending",
                                fixFee.doubleValue(), buyAmt.doubleValue(), netAmount.doubleValue(), grossAmount.doubleValue()), HttpStatus.OK);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error getBuy() class DepositController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "Failed to buy !"), HttpStatus.OK);
        }
        return new ResponseEntity<Object>(new CustomTransJsonClass("success", "Transaction complete",
                fixFee.doubleValue(), buyAmt.doubleValue(), netAmount.doubleValue(), grossAmount.doubleValue()), HttpStatus.OK);

    }
    
    @RequestMapping(value = "/buy/exchange", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ResponseEntity<Object> getBuyExchange(@RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "balance", defaultValue = "0") BigDecimal latesBalance,
            @RequestParam(value = "currency", defaultValue = "BTC") String currency,
            @RequestParam(value = "tocurrency", defaultValue = "") String toCurrency,
            @RequestParam(value = "amount", defaultValue = "0") BigDecimal amount) {
        BigDecimal fixBalance = latesBalance;
        BigDecimal buyAmt = amount;
        BigDecimal priceFromThird = BigDecimal.ZERO;
        BigDecimal fixFee = BigDecimal.ZERO;
        BigDecimal grossAmount = BigDecimal.ZERO;
        BigDecimal netAmount = BigDecimal.ZERO;
        NumberFormat formatter = new DecimalFormat("###.########");
        Optional<BimarAppConfig> config = configRepository.findById(1);
        Utilities util = new Utilities();
        if (null != config) {
            fixFee = new BigDecimal(config.get().getCurrencyConversionFee());
        }
        try {
            //pengecekan user
            BimarSiteUsers user = userRepository.findByEmail(email);
            if (null == user) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", "User not found !"), HttpStatus.OK);
            }
            //pengecekan currency default
            BimarCurrencies cry = currRepository.findByCurrency(currency);
            if (null == cry) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", currency + " not found !"), HttpStatus.OK);
            }

            //pengecekan currency tujuan
            BimarCurrencies tocry = currRepository.findByCurrency(toCurrency);
            System.out.println("tocurrency ="+ tocry);
            if (null == tocry) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", toCurrency + " not found !"), HttpStatus.OK);
            }
            //saldo user
            fixBalance = latesBalance;
            //jumlah yang ingin dibeli
            buyAmt = amount;
            //harga terakhir
            priceFromThird = util.getPriceConvert(currency, toCurrency);

            if (buyAmt.doubleValue() > fixBalance.doubleValue()) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", "Not enough balance !"), HttpStatus.OK);
            }

            //perhitungan
            fixBalance = new BigDecimal((fixBalance.doubleValue() - buyAmt.doubleValue()));
            grossAmount = new BigDecimal((buyAmt.doubleValue() * priceFromThird.doubleValue()));
            netAmount = new BigDecimal((grossAmount.doubleValue() - (grossAmount.doubleValue() * fixFee.doubleValue() / 100)));

            //simpan ke tabel transaksi
            BimarTransactions tr = new BimarTransactions();
            tr.setDate(new Date());
            tr.setSiteUser(user.getId());
            tr.setBtc(buyAmt.doubleValue());
            tr.setCurrency(tocry.getId());
            tr.setBtcPrice(priceFromThird.doubleValue());
            tr.setFee(fixFee.doubleValue());
            tr.setTransactionType(1);
            tr.setBtcNet(netAmount.doubleValue());
            tr.setBtcBefore(netAmount.doubleValue());
            tr.setBtcAfter(grossAmount.doubleValue());
            tr.setConvertFromCurrency(cry.getId());
            tr.setConvertToCurrency(tocry.getId());
            tr.setLogId(0); // penanda bahwa sell berasal dari transaksi pair
            tr.setLogId1(1); // penanda bahwa sell berasal dari transaksi exchange
            trRepository.save(tr);

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error getBuy() class DepositController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "Failed to buy !"), HttpStatus.OK);
        }
        return new ResponseEntity<Object>(new CustomTransJsonClass("success", "Transaction complete",
                fixFee.doubleValue(), buyAmt.doubleValue(), netAmount.doubleValue(), grossAmount.doubleValue()), HttpStatus.OK);

    }    

    @RequestMapping(value = "/sell", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ResponseEntity<Object> getSell(@RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "coin", defaultValue = "") String coin,
            @RequestParam(value = "coin2", defaultValue = "") String coin2,
            @RequestParam(value = "price", defaultValue = "") String price,
            @RequestParam(value = "amount", defaultValue = "") String amount,
            @RequestParam(value = "fee", defaultValue = "") String fee) {
        /*
            Simulasi User akan jual LTC/BTC
            1. cari balance LTC => Coin
            2. cari balance BTC => coin2
         */
        double bruto = 0.0;
        double feeAdm = 0.0;
        double netto = 0.0;
        BimarStockCoins bimarStock = null;
        BigDecimal fixFee = BigDecimal.ZERO;
        Optional<BimarAppConfig> config = configRepository.findById(1);
        if (null != config) {
            fixFee = new BigDecimal(config.get().getCurrencyConversionFee());
        }
        try {
            //pengecekan user
            BimarSiteUsers user = userRepository.findByEmail(email);
            if (null == user) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", "User not found !"), HttpStatus.OK);
            }
            //Cari balance terhadap coin 1
            BigDecimal oldBalance = getLastBalance(user.getEmail(), coin);
            //Cari balance terhadap coin 2
            BigDecimal oldBalance2 = getLastBalance(user.getEmail(), coin2);

            //pengecekan currency default
            BimarCurrencies cry = currRepository.findByCurrency(coin);
            if (null == cry) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", coin + " not found !"), HttpStatus.OK);
            }

            //pengecekan currency tujuan
            BimarCurrencies tocry = currRepository.findByCurrency(coin2);
            if (null == tocry) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", coin2 + " not found !"), HttpStatus.OK);
            }
            
            if (Double.parseDouble(amount) > oldBalance.doubleValue()) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", "Not enough balance !"), HttpStatus.OK);
            }
            
            //harga terakhir
            BigDecimal priceFromThird = new Utilities().getPriceConvert(coin, coin2);

            bruto = Double.parseDouble(amount) * Double.parseDouble(price);
            feeAdm = (bruto * (Double.parseDouble(fee) / 100));
            netto = bruto - feeAdm;
            
            //cek stok coin tujuan
            bimarStock = stockCoinsRepository.getByNamaCoins(tocry.getCurrency());
            if (null != bimarStock) {
                //status pending
                if (bimarStock.getAmount() < netto || requestsRepository.getSumPendingBuyNetAmount(netto) == 0) {
                    BimarApiCoins toCoin = coinsRepository.findByName(tocry.getCurrency());
                    if (null != toCoin) {
                        BimarApiAddress address = addressRepository.getByIdCoinsAndSiteUser(toCoin, user);
                        if (null != address) {
                            BimarRequests request = new BimarRequests();
                            request.setDate(new Date());
                            request.setSiteUser(user.getId());
                            request.setCurrency(tocry.getId());
                            request.setAmount(Double.parseDouble(amount));
                            request.setRequestStatus(1);
                            request.setRequestType(4);
                            request.setAddressId(address.getIdAddress());
                            request.setSendAddress(address.getAddress());
                            request.setTransactionId("");
                            request.setDone(Short.valueOf("0"));
                            request.setCryptoId(toCoin.getId());
                            request.setFee(fixFee.doubleValue());
                            request.setNetAmount(netto);
                            request.setNotified(0);
                            request.setFcurrency(cry.getId());
                            request.setTcurrency(tocry.getId());
                            request.setLastPrice(priceFromThird.doubleValue());
                            //masuk pending
                            requestsRepository.save(request);

                            return new ResponseEntity<Object>(new CustomTransJsonClass("pending", "Transaction is pending",
                                    fixFee.doubleValue(), Double.parseDouble(amount), netto, bruto), HttpStatus.OK);
                        }
                    }
                } else {
                    //Jika harga jual transaksi lebih kecil dari pada harga sekarang maka pending        
                    //simpan kedalam table transaksi dengan status ok            
                    BimarTransactions tr = new BimarTransactions();
                    tr.setDate(new Date());
                    tr.setSiteUser(user.getId());
                    tr.setBtc(Double.parseDouble(price));
                    tr.setCurrency(tocry.getId());
                    tr.setBtcPrice(priceFromThird.doubleValue());
                    tr.setFee(feeAdm);
                    tr.setTransactionType(2);
                    tr.setBtcNet(netto);
                    tr.setBtcBefore(bruto);
                    tr.setBtcAfter(netto);
                    tr.setConvertFromCurrency(cry.getId());
                    tr.setConvertToCurrency(tocry.getId());
                    tr.setLogId(0);
                    tr.setLogId1(2); // penanda bahwa sell berasal dari transaksi pair
                    trRepository.save(tr);
                }
            }else {
                BimarApiCoins toCoin = coinsRepository.findByName(tocry.getCurrency());
                if (null != toCoin) {
                    BimarApiAddress address = addressRepository.getByIdCoinsAndSiteUser(toCoin, user);
                    if (null != address) {
                        BimarRequests request = new BimarRequests();
                        request.setDate(new Date());
                        request.setSiteUser(user.getId());
                        request.setCurrency(tocry.getId());
                        request.setAmount(Double.parseDouble(amount));
                        request.setRequestStatus(1);
                        request.setRequestType(4);
                        request.setAddressId(address.getIdAddress());
                        request.setSendAddress(address.getAddress());
                        request.setTransactionId("");
                        request.setDone(Short.valueOf("0"));
                        request.setCryptoId(toCoin.getId());
                        request.setFee(fixFee.doubleValue());
                        request.setNetAmount(netto);
                        request.setNotified(0);
                        request.setFcurrency(cry.getId());
                        request.setTcurrency(tocry.getId());
                        request.setLastPrice(priceFromThird.doubleValue());
                        //masuk pending
                        requestsRepository.save(request);

                        return new ResponseEntity<Object>(new CustomTransJsonClass("pending", "Transaction is pending",
                                fixFee.doubleValue(), Double.parseDouble(amount), netto, bruto), HttpStatus.OK);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error geSell() class DepositController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "Failed to sell !"), HttpStatus.OK);
        }
        //update masing masing balance coins
        return new ResponseEntity<Object>(new CustomTransJsonClass("success", "Transaction complete",
                fixFee.doubleValue(), Double.parseDouble(amount), netto, bruto), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/sell/exchange", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public ResponseEntity<Object> getSellExchange(@RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "coin", defaultValue = "") String coin,
            @RequestParam(value = "coin2", defaultValue = "") String coin2,
            @RequestParam(value = "price", defaultValue = "") String price,
            @RequestParam(value = "amount", defaultValue = "") String amount,
            @RequestParam(value = "fee", defaultValue = "") String fee) {
        /*
            Simulasi User akan jual LTC/BTC
            1. cari balance LTC => Coin
            2. cari balance BTC => coin2
         */
        double bruto = 0.0;
        double feeAdm = 0.0;
        double netto = 0.0;
        double newBalance = 0.0;
        double newBalance2 = 0.0;
        BigDecimal fixFee = BigDecimal.ZERO;
        Optional<BimarAppConfig> config = configRepository.findById(1);
        if (null != config) {
            fixFee = new BigDecimal(config.get().getCurrencyConversionFee());
        }
        try {
            //pengecekan user
            BimarSiteUsers user = userRepository.findByEmail(email);
            if (null == user) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", "User not found !"), HttpStatus.OK);
            }
            //Cari balance terhadap coin 1
            BigDecimal oldBalance = getLastBalance(user.getEmail(), coin);
            //Cari balance terhadap coin 2
            BigDecimal oldBalance2 = getLastBalance(user.getEmail(), coin2);

            //pengecekan currency default
            BimarCurrencies cry = currRepository.findByCurrency(coin);
            if (null == cry) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", coin + " not found !"), HttpStatus.OK);
            }

            //pengecekan currency tujuan
            BimarCurrencies tocry = currRepository.findByCurrency(coin2);
            if (null == tocry) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", coin2 + " not found !"), HttpStatus.OK);
            }
            
            if (Double.parseDouble(amount) > oldBalance.doubleValue()) {
                return new ResponseEntity<Object>(new CustomJsonClass("error", "Not enough balance !"), HttpStatus.OK);
            }
            //harga terakhir
//            BigDecimal priceFromThird = new Utilities().getPriceConvert(coin2, coin);
//
//            bruto = Double.parseDouble(amount) * Double.parseDouble(price);
//            feeAdm = (bruto * Double.parseDouble(fee)) / 100;
//            netto = bruto - feeAdm;
//            newBalance = oldBalance.doubleValue() - netto;
//            newBalance2 = oldBalance2.doubleValue() + netto;
            //harga terakhir
            BigDecimal priceFromThird = new Utilities().getPriceConvert(coin,coin2);

            bruto = Double.parseDouble(amount) * Double.parseDouble(price);
            feeAdm = (bruto * (Double.parseDouble(fee) / 100));
            netto = bruto - feeAdm;
            
            //Jika harga jual transaksi lebih kecil dari pada harga sekarang maka pending        
            //simpan kedalam table transaksi dengan status ok            
            BimarTransactions tr = new BimarTransactions();
            tr.setDate(new Date());
            tr.setSiteUser(user.getId());
            tr.setBtc(Double.parseDouble(price));
            tr.setCurrency(tocry.getId());
            tr.setBtcPrice(priceFromThird.doubleValue());
            tr.setFee(feeAdm);
            tr.setTransactionType(2);
            tr.setBtcNet(netto);
            tr.setBtcBefore(bruto);
            tr.setBtcAfter(netto);
            tr.setConvertFromCurrency(cry.getId());
            tr.setConvertToCurrency(tocry.getId());
            tr.setLogId(0);
            tr.setLogId1(1); // penanda bahwa sell berasal dari transaksi exchange
            trRepository.save(tr);

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error geSell() class DepositController");
            return new ResponseEntity<Object>(new CustomJsonClass("error", "Failed to sell !"), HttpStatus.OK);
        }
        //update masing masing balance coins
        return new ResponseEntity<Object>(new CustomTransJsonClass("success", "Transaction complete",
                fixFee.doubleValue(), Double.parseDouble(amount), netto, bruto), HttpStatus.OK);
    }

    private void getTransReceivedBlockIO(BimarApiCoins bac, String network, String address, Integer addressID, BimarSiteUsers bsu) throws BlockIOException {
        BlockIO blockIO = new BlockIO(network);
        TransactionsReceived received;
        List<String> sortedValueList = null;
        System.out.println(address);
        BimarRequests request = null;
        BimarHistory bimarHistory = null;
        Double lastAmount = 0.0;
        Double lastBalance = 0.0;
        if (null != blockIO) {
            System.err.println("INI ADDRESS :" + address);
            received = blockIO.getTransactionsReceivedByAddress2(address);
            System.out.println("received.txs : " + received.txs);
            Map<String, String> map = new HashMap();
            for (TransactionReceived tr : received.txs) {
                request = requestsRepository.findByTransactionId(tr.txid);
                System.out.println("isi Request : " + request);
                if (request == null) {
                    BimarRequests newRequest = new BimarRequests();
                    newRequest.setTransactionId(tr.txid);
                    newRequest.setAmount(Double.valueOf(String.valueOf(tr.amountsReceived.get(0).amount)));
                    newRequest.setDate(new Date(tr.time * 1000));
                    newRequest.setSendAddress(String.valueOf(tr.senders.get(0).toString()));
                    newRequest.setRequestStatus(2);
                    newRequest.setRequestType(2);
                    newRequest.setFee(0.0);
                    newRequest.setNetAmount(Double.valueOf(String.valueOf(tr.amountsReceived.get(0).amount)));
                    newRequest.setSiteUser(bsu.getId());
                    newRequest.setCurrency(bac.getCurrencyid());
                    newRequest.setCryptoId(bac.getId());
                    newRequest.setAddressId(addressID);
                    newRequest.setDone(Short.parseShort(String.valueOf(tr.confirmations)));
                    requestsRepository.save(newRequest);
                    // Update nilai balance database dari nilai transaksi yang dicreate oleh block io                                
                    // Insert transaksi deposit kedalam table history
                    bimarHistory = historyRepository.findByDateAndSiteUser(new Date(tr.time * 1000), bsu.getId());
                    System.out.println("isi bimarHistory : " + bimarHistory);
                    if (bimarHistory == null) {
                        // Cari informasi amount terakhir yang didepositkan oleh user
                        BimarRequests requests = requestsRepository.findByTransactionId(tr.txid);
                        if (requests != null) {
                            lastAmount = requests.getAmount();
                        }
                        System.out.println("isi request : " + requests.getCryptoId());
                        // Cari informasi balance terakhir yang ditelah digunakan oleh user
                        BimarSiteUsersBalances balances = balanceRepository.findBySiteUserAndCoinid(bsu.getId(), requests.getCryptoId());

                        if (balances != null) {
                            lastBalance = balances.getBalance();
                            balances.setBalance(lastBalance + lastAmount);
                            balanceRepository.saveAndFlush(balances);
                        } else {
                            balances = new BimarSiteUsersBalances();
                            balances.setSiteUser(bsu.getId());
                            balances.setCoinid(requests.getCryptoId());
                            balances.setBalance(lastAmount);
                            balanceRepository.save(balances);
                        }
                        bimarHistory = new BimarHistory();
                        bimarHistory.setSiteUser(bsu.getId());
                        bimarHistory.setBalanceBefore(lastBalance);
                        bimarHistory.setBalanceAfter(lastBalance + lastAmount);
                        bimarHistory.setRequestId(requests.getId());
                        bimarHistory.setBitcoinAddress(address);
                        bimarHistory.setHistoryAction(2);
                        bimarHistory.setDate(new Date(tr.time * 1000));
                        historyRepository.save(bimarHistory);
                    }
                }
            }
        }
    }
}
