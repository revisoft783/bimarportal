/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 *
 * @author afes
 */
@Entity
@Table(name = "bimar_api_mcoins",catalog = "bimardb", schema = "bimar")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "BimarApiMcoins.findAll", query = "SELECT b FROM BimarApiMcoins b")
//    , @NamedQuery(name = "BimarApiMcoins.findById", query = "SELECT b FROM BimarApiMcoins b WHERE b.id = :id")
//    , @NamedQuery(name = "BimarApiMcoins.findByName", query = "SELECT b FROM BimarApiMcoins b WHERE b.name = :name")
//    , @NamedQuery(name = "BimarApiMcoins.findByNameSkt", query = "SELECT b FROM BimarApiMcoins b WHERE b.nameSkt = :nameSkt")})
public class BimarApiMcoins implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "name_skt")
    private String nameSkt;    
    @Column(name = "price", precision = 17, scale = 17)
    private Double price;
    @Column(name = "lastupdate", precision = 17, scale = 17)
    private Double lastUpdate;
    @Column(name = "changepct24h", precision = 17, scale = 17)
    private Double changePCT24H;
    @Column(name = "changepct1h", precision = 17, scale = 17)
    private Double changePCT1H;  
    @JoinColumn(name = "coins2", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private BimarApiCoins idCoins;
    
    public BimarApiMcoins() {
    }

    public BimarApiMcoins(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameSkt() {
        return nameSkt;
    }

    public void setNameSkt(String nameSkt) {
        this.nameSkt = nameSkt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarApiMcoins)) {
            return false;
        }
        BimarApiMcoins other = (BimarApiMcoins) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarApiMcoins[ id=" + id + " ]";
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Double lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Double getChangePCT24H() {
        return changePCT24H;
    }

    public void setChangePCT24H(Double changePCT24H) {
        this.changePCT24H = changePCT24H;
    }

    public Double getChangePCT1H() {
        return changePCT1H;
    }

    public void setChangePCT1H(Double changePCT1H) {
        this.changePCT1H = changePCT1H;
    }

    public BimarApiCoins getIdCoins() {
        return idCoins;
    }

    public void setIdCoins(BimarApiCoins idCoins) {
        this.idCoins = idCoins;
    }
    
}
