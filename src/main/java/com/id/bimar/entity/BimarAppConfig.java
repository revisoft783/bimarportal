/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_app_config", catalog = "bimardb", schema = "bimar")
public class BimarAppConfig implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Size(max = 255)
    @Column(name = "default_timezone", length = 255)
    private String defaultTimezone;
    @Size(max = 255)
    @Column(name = "orders_under_market_percent", length = 255)
    private String ordersUnderMarketPercent;
    @Size(max = 255)
    @Column(name = "orders_min_usd", length = 255)
    private String ordersMinUsd;
    @Size(max = 255)
    @Column(name = "bitcoin_sending_fee", length = 255)
    private String bitcoinSendingFee;
    @Size(max = 255)
    @Column(name = "frontend_baseurl", length = 255)
    private String frontendBaseurl;
    @Size(max = 255)
    @Column(name = "frontend_dirroot", length = 255)
    private String frontendDirroot;
    @Size(max = 255)
    @Column(name = "fiat_withdraw_fee", length = 255)
    private String fiatWithdrawFee;
    @Column(name = "api_db_debug")
    private Short apiDbDebug;
    @Size(max = 255)
    @Column(name = "api_dirroot", length = 255)
    private String apiDirroot;
    @Size(max = 255)
    @Column(name = "support_email", length = 255)
    private String supportEmail;
    @Size(max = 255)
    @Column(name = "email_smtp_host", length = 255)
    private String emailSmtpHost;
    @Size(max = 255)
    @Column(name = "email_smtp_port", length = 255)
    private String emailSmtpPort;
    @Size(max = 255)
    @Column(name = "email_smtp_security", length = 255)
    private String emailSmtpSecurity;
    @Size(max = 255)
    @Column(name = "email_smtp_username", length = 255)
    private String emailSmtpUsername;
    @Size(max = 255)
    @Column(name = "email_smtp_password", length = 255)
    private String emailSmtpPassword;
    @Size(max = 255)
    @Column(name = "email_smtp_send_from", length = 255)
    private String emailSmtpSendFrom;
    @Size(max = 255)
    @Column(name = "bitcoin_username", length = 255)
    private String bitcoinUsername;
    @Size(max = 255)
    @Column(name = "bitcoin_accountname", length = 255)
    private String bitcoinAccountname;
    @Size(max = 255)
    @Column(name = "bitcoin_passphrase", length = 255)
    private String bitcoinPassphrase;
    @Size(max = 255)
    @Column(name = "bitcoin_host", length = 255)
    private String bitcoinHost;
    @Size(max = 255)
    @Column(name = "bitcoin_port", length = 255)
    private String bitcoinPort;
    @Size(max = 255)
    @Column(name = "bitcoin_protocol", length = 255)
    private String bitcoinProtocol;
    @Size(max = 255)
    @Column(name = "authy_api_key", length = 255)
    private String authyApiKey;
    @Size(max = 255)
    @Column(name = "helpdesk_key", length = 255)
    private String helpdeskKey;
    @Size(max = 255)
    @Column(name = "exchange_name", length = 255)
    private String exchangeName;
    @Size(max = 255)
    @Column(name = "mcrypt_key", length = 255)
    private String mcryptKey;
    @Size(max = 255)
    @Column(name = "currency_conversion_fee", length = 255)
    private String currencyConversionFee;
    @Column(name = "cross_currency_trades")
    private Short crossCurrencyTrades;
    @Size(max = 255)
    @Column(name = "btc_currency_id", length = 255)
    private String btcCurrencyId;
    @Size(max = 255)
    @Column(name = "deposit_bitcoin_desc", length = 255)
    private String depositBitcoinDesc;
    @Size(max = 255)
    @Column(name = "default_fee_schedule_id", length = 255)
    private String defaultFeeScheduleId;
    @Size(max = 255)
    @Column(name = "history_buy_id", length = 255)
    private String historyBuyId;
    @Size(max = 255)
    @Column(name = "history_deposit_id", length = 255)
    private String historyDepositId;
    @Size(max = 255)
    @Column(name = "history_login_id", length = 255)
    private String historyLoginId;
    @Size(max = 255)
    @Column(name = "history_sell_id", length = 255)
    private String historySellId;
    @Size(max = 255)
    @Column(name = "history_withdraw_id", length = 255)
    private String historyWithdrawId;
    @Size(max = 255)
    @Column(name = "order_type_ask", length = 255)
    private String orderTypeAsk;
    @Size(max = 255)
    @Column(name = "request_awaiting_id", length = 255)
    private String requestAwaitingId;
    @Size(max = 255)
    @Column(name = "request_cancelled_id", length = 255)
    private String requestCancelledId;
    @Size(max = 255)
    @Column(name = "request_completed_id", length = 255)
    private String requestCompletedId;
    @Size(max = 255)
    @Column(name = "order_type_bid", length = 255)
    private String orderTypeBid;
    @Size(max = 255)
    @Column(name = "request_deposit_id", length = 255)
    private String requestDepositId;
    @Size(max = 255)
    @Column(name = "request_pending_id", length = 255)
    private String requestPendingId;
    @Size(max = 255)
    @Column(name = "request_withdrawal_id", length = 255)
    private String requestWithdrawalId;
    @Size(max = 255)
    @Column(name = "transactions_buy_id", length = 255)
    private String transactionsBuyId;
    @Size(max = 255)
    @Column(name = "transactions_sell_id", length = 255)
    private String transactionsSellId;
    @Size(max = 255)
    @Column(name = "withdraw_fiat_desc", length = 255)
    private String withdrawFiatDesc;
    @Size(max = 255)
    @Column(name = "withdraw_btc_desc", length = 255)
    private String withdrawBtcDesc;
    @Size(max = 255)
    @Column(name = "form_email_from", length = 255)
    private String formEmailFrom;
    @Column(name = "email_notify_new_users")
    private Short emailNotifyNewUsers;
    @Size(max = 255)
    @Column(name = "pass_regex", length = 255)
    private String passRegex;
    @Size(max = 255)
    @Column(name = "pass_min_chars", length = 255)
    private String passMinChars;
    @Column(name = "auth_db_debug")
    private Short authDbDebug;
    @Size(max = 255)
    @Column(name = "bitcoin_reserve_min", length = 255)
    private String bitcoinReserveMin;
    @Size(max = 255)
    @Column(name = "bitcoin_reserve_ratio", length = 255)
    private String bitcoinReserveRatio;
    @Size(max = 255)
    @Column(name = "bitcoin_warm_wallet_address", length = 255)
    private String bitcoinWarmWalletAddress;
    @Column(name = "cron_db_debug")
    private Short cronDbDebug;
    @Size(max = 255)
    @Column(name = "quandl_api_key", length = 255)
    private String quandlApiKey;
    @Size(max = 255)
    @Column(name = "cron_dirroot", length = 255)
    private String cronDirroot;
    @Column(name = "backstage_db_debug")
    private Short backstageDbDebug;
    @Size(max = 255)
    @Column(name = "backstage_dirroot", length = 255)
    private String backstageDirroot;
    @Column(name = "email_notify_fiat_withdrawals")
    private Short emailNotifyFiatWithdrawals;
    @Size(max = 255)
    @Column(name = "contact_email", length = 255)
    private String contactEmail;
    @Size(max = 255)
    @Column(name = "cloudflare_api_key", length = 255)
    private String cloudflareApiKey;
    @Size(max = 255)
    @Column(name = "google_recaptch_api_key", length = 255)
    private String googleRecaptchApiKey;
    @Column(name = "cloudflare_blacklist")
    private Short cloudflareBlacklist;
    @Size(max = 255)
    @Column(name = "cloudflare_email", length = 255)
    private String cloudflareEmail;
    @Size(max = 255)
    @Column(name = "google_recaptch_api_secret", length = 255)
    private String googleRecaptchApiSecret;
    @Column(name = "cloudflare_blacklist_attempts")
    private Integer cloudflareBlacklistAttempts;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cloudflare_blacklist_timeframe", precision = 17, scale = 17)
    private Double cloudflareBlacklistTimeframe;
    @Size(max = 255)
    @Column(name = "crypto_capital_pk", length = 255)
    private String cryptoCapitalPk;
    @Size(max = 255)
    @Column(name = "deposit_fiat_desc", length = 255)
    private String depositFiatDesc;
    @Column(name = "email_notify_fiat_failed")
    private Short emailNotifyFiatFailed;
    @Size(max = 255)
    @Column(name = "trading_status", length = 255)
    private String tradingStatus;
    @Size(max = 255)
    @Column(name = "withdrawals_status", length = 255)
    private String withdrawalsStatus;
    @Column(name = "affliate_pct")
    private double affliatePct;
    @Column(name = "affliate_active")
    private short affliateActive;
    
    public BimarAppConfig() {
    }

    public BimarAppConfig(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDefaultTimezone() {
        return defaultTimezone;
    }

    public void setDefaultTimezone(String defaultTimezone) {
        this.defaultTimezone = defaultTimezone;
    }

    public String getOrdersUnderMarketPercent() {
        return ordersUnderMarketPercent;
    }

    public void setOrdersUnderMarketPercent(String ordersUnderMarketPercent) {
        this.ordersUnderMarketPercent = ordersUnderMarketPercent;
    }

    public String getOrdersMinUsd() {
        return ordersMinUsd;
    }

    public void setOrdersMinUsd(String ordersMinUsd) {
        this.ordersMinUsd = ordersMinUsd;
    }

    public String getBitcoinSendingFee() {
        return bitcoinSendingFee;
    }

    public void setBitcoinSendingFee(String bitcoinSendingFee) {
        this.bitcoinSendingFee = bitcoinSendingFee;
    }

    public String getFrontendBaseurl() {
        return frontendBaseurl;
    }

    public void setFrontendBaseurl(String frontendBaseurl) {
        this.frontendBaseurl = frontendBaseurl;
    }

    public String getFrontendDirroot() {
        return frontendDirroot;
    }

    public void setFrontendDirroot(String frontendDirroot) {
        this.frontendDirroot = frontendDirroot;
    }

    public String getFiatWithdrawFee() {
        return fiatWithdrawFee;
    }

    public void setFiatWithdrawFee(String fiatWithdrawFee) {
        this.fiatWithdrawFee = fiatWithdrawFee;
    }

    public Short getApiDbDebug() {
        return apiDbDebug;
    }

    public void setApiDbDebug(Short apiDbDebug) {
        this.apiDbDebug = apiDbDebug;
    }

    public String getApiDirroot() {
        return apiDirroot;
    }

    public void setApiDirroot(String apiDirroot) {
        this.apiDirroot = apiDirroot;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

    public String getEmailSmtpHost() {
        return emailSmtpHost;
    }

    public void setEmailSmtpHost(String emailSmtpHost) {
        this.emailSmtpHost = emailSmtpHost;
    }

    public String getEmailSmtpPort() {
        return emailSmtpPort;
    }

    public void setEmailSmtpPort(String emailSmtpPort) {
        this.emailSmtpPort = emailSmtpPort;
    }

    public String getEmailSmtpSecurity() {
        return emailSmtpSecurity;
    }

    public void setEmailSmtpSecurity(String emailSmtpSecurity) {
        this.emailSmtpSecurity = emailSmtpSecurity;
    }

    public String getEmailSmtpUsername() {
        return emailSmtpUsername;
    }

    public void setEmailSmtpUsername(String emailSmtpUsername) {
        this.emailSmtpUsername = emailSmtpUsername;
    }

    public String getEmailSmtpPassword() {
        return emailSmtpPassword;
    }

    public void setEmailSmtpPassword(String emailSmtpPassword) {
        this.emailSmtpPassword = emailSmtpPassword;
    }

    public String getEmailSmtpSendFrom() {
        return emailSmtpSendFrom;
    }

    public void setEmailSmtpSendFrom(String emailSmtpSendFrom) {
        this.emailSmtpSendFrom = emailSmtpSendFrom;
    }

    public String getBitcoinUsername() {
        return bitcoinUsername;
    }

    public void setBitcoinUsername(String bitcoinUsername) {
        this.bitcoinUsername = bitcoinUsername;
    }

    public String getBitcoinAccountname() {
        return bitcoinAccountname;
    }

    public void setBitcoinAccountname(String bitcoinAccountname) {
        this.bitcoinAccountname = bitcoinAccountname;
    }

    public String getBitcoinPassphrase() {
        return bitcoinPassphrase;
    }

    public void setBitcoinPassphrase(String bitcoinPassphrase) {
        this.bitcoinPassphrase = bitcoinPassphrase;
    }

    public String getBitcoinHost() {
        return bitcoinHost;
    }

    public void setBitcoinHost(String bitcoinHost) {
        this.bitcoinHost = bitcoinHost;
    }

    public String getBitcoinPort() {
        return bitcoinPort;
    }

    public void setBitcoinPort(String bitcoinPort) {
        this.bitcoinPort = bitcoinPort;
    }

    public String getBitcoinProtocol() {
        return bitcoinProtocol;
    }

    public void setBitcoinProtocol(String bitcoinProtocol) {
        this.bitcoinProtocol = bitcoinProtocol;
    }

    public String getAuthyApiKey() {
        return authyApiKey;
    }

    public void setAuthyApiKey(String authyApiKey) {
        this.authyApiKey = authyApiKey;
    }

    public String getHelpdeskKey() {
        return helpdeskKey;
    }

    public void setHelpdeskKey(String helpdeskKey) {
        this.helpdeskKey = helpdeskKey;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getMcryptKey() {
        return mcryptKey;
    }

    public void setMcryptKey(String mcryptKey) {
        this.mcryptKey = mcryptKey;
    }

    public String getCurrencyConversionFee() {
        return currencyConversionFee;
    }

    public void setCurrencyConversionFee(String currencyConversionFee) {
        this.currencyConversionFee = currencyConversionFee;
    }

    public Short getCrossCurrencyTrades() {
        return crossCurrencyTrades;
    }

    public void setCrossCurrencyTrades(Short crossCurrencyTrades) {
        this.crossCurrencyTrades = crossCurrencyTrades;
    }

    public String getBtcCurrencyId() {
        return btcCurrencyId;
    }

    public void setBtcCurrencyId(String btcCurrencyId) {
        this.btcCurrencyId = btcCurrencyId;
    }

    public String getDepositBitcoinDesc() {
        return depositBitcoinDesc;
    }

    public void setDepositBitcoinDesc(String depositBitcoinDesc) {
        this.depositBitcoinDesc = depositBitcoinDesc;
    }

    public String getDefaultFeeScheduleId() {
        return defaultFeeScheduleId;
    }

    public void setDefaultFeeScheduleId(String defaultFeeScheduleId) {
        this.defaultFeeScheduleId = defaultFeeScheduleId;
    }

    public String getHistoryBuyId() {
        return historyBuyId;
    }

    public void setHistoryBuyId(String historyBuyId) {
        this.historyBuyId = historyBuyId;
    }

    public String getHistoryDepositId() {
        return historyDepositId;
    }

    public void setHistoryDepositId(String historyDepositId) {
        this.historyDepositId = historyDepositId;
    }

    public String getHistoryLoginId() {
        return historyLoginId;
    }

    public void setHistoryLoginId(String historyLoginId) {
        this.historyLoginId = historyLoginId;
    }

    public String getHistorySellId() {
        return historySellId;
    }

    public void setHistorySellId(String historySellId) {
        this.historySellId = historySellId;
    }

    public String getHistoryWithdrawId() {
        return historyWithdrawId;
    }

    public void setHistoryWithdrawId(String historyWithdrawId) {
        this.historyWithdrawId = historyWithdrawId;
    }

    public String getOrderTypeAsk() {
        return orderTypeAsk;
    }

    public void setOrderTypeAsk(String orderTypeAsk) {
        this.orderTypeAsk = orderTypeAsk;
    }

    public String getRequestAwaitingId() {
        return requestAwaitingId;
    }

    public void setRequestAwaitingId(String requestAwaitingId) {
        this.requestAwaitingId = requestAwaitingId;
    }

    public String getRequestCancelledId() {
        return requestCancelledId;
    }

    public void setRequestCancelledId(String requestCancelledId) {
        this.requestCancelledId = requestCancelledId;
    }

    public String getRequestCompletedId() {
        return requestCompletedId;
    }

    public void setRequestCompletedId(String requestCompletedId) {
        this.requestCompletedId = requestCompletedId;
    }

    public String getOrderTypeBid() {
        return orderTypeBid;
    }

    public void setOrderTypeBid(String orderTypeBid) {
        this.orderTypeBid = orderTypeBid;
    }

    public String getRequestDepositId() {
        return requestDepositId;
    }

    public void setRequestDepositId(String requestDepositId) {
        this.requestDepositId = requestDepositId;
    }

    public String getRequestPendingId() {
        return requestPendingId;
    }

    public void setRequestPendingId(String requestPendingId) {
        this.requestPendingId = requestPendingId;
    }

    public String getRequestWithdrawalId() {
        return requestWithdrawalId;
    }

    public void setRequestWithdrawalId(String requestWithdrawalId) {
        this.requestWithdrawalId = requestWithdrawalId;
    }

    public String getTransactionsBuyId() {
        return transactionsBuyId;
    }

    public void setTransactionsBuyId(String transactionsBuyId) {
        this.transactionsBuyId = transactionsBuyId;
    }

    public String getTransactionsSellId() {
        return transactionsSellId;
    }

    public void setTransactionsSellId(String transactionsSellId) {
        this.transactionsSellId = transactionsSellId;
    }

    public String getWithdrawFiatDesc() {
        return withdrawFiatDesc;
    }

    public void setWithdrawFiatDesc(String withdrawFiatDesc) {
        this.withdrawFiatDesc = withdrawFiatDesc;
    }

    public String getWithdrawBtcDesc() {
        return withdrawBtcDesc;
    }

    public void setWithdrawBtcDesc(String withdrawBtcDesc) {
        this.withdrawBtcDesc = withdrawBtcDesc;
    }

    public String getFormEmailFrom() {
        return formEmailFrom;
    }

    public void setFormEmailFrom(String formEmailFrom) {
        this.formEmailFrom = formEmailFrom;
    }

    public Short getEmailNotifyNewUsers() {
        return emailNotifyNewUsers;
    }

    public void setEmailNotifyNewUsers(Short emailNotifyNewUsers) {
        this.emailNotifyNewUsers = emailNotifyNewUsers;
    }

    public String getPassRegex() {
        return passRegex;
    }

    public void setPassRegex(String passRegex) {
        this.passRegex = passRegex;
    }

    public String getPassMinChars() {
        return passMinChars;
    }

    public void setPassMinChars(String passMinChars) {
        this.passMinChars = passMinChars;
    }

    public Short getAuthDbDebug() {
        return authDbDebug;
    }

    public void setAuthDbDebug(Short authDbDebug) {
        this.authDbDebug = authDbDebug;
    }

    public String getBitcoinReserveMin() {
        return bitcoinReserveMin;
    }

    public void setBitcoinReserveMin(String bitcoinReserveMin) {
        this.bitcoinReserveMin = bitcoinReserveMin;
    }

    public String getBitcoinReserveRatio() {
        return bitcoinReserveRatio;
    }

    public void setBitcoinReserveRatio(String bitcoinReserveRatio) {
        this.bitcoinReserveRatio = bitcoinReserveRatio;
    }

    public String getBitcoinWarmWalletAddress() {
        return bitcoinWarmWalletAddress;
    }

    public void setBitcoinWarmWalletAddress(String bitcoinWarmWalletAddress) {
        this.bitcoinWarmWalletAddress = bitcoinWarmWalletAddress;
    }

    public Short getCronDbDebug() {
        return cronDbDebug;
    }

    public void setCronDbDebug(Short cronDbDebug) {
        this.cronDbDebug = cronDbDebug;
    }

    public String getQuandlApiKey() {
        return quandlApiKey;
    }

    public void setQuandlApiKey(String quandlApiKey) {
        this.quandlApiKey = quandlApiKey;
    }

    public String getCronDirroot() {
        return cronDirroot;
    }

    public void setCronDirroot(String cronDirroot) {
        this.cronDirroot = cronDirroot;
    }

    public Short getBackstageDbDebug() {
        return backstageDbDebug;
    }

    public void setBackstageDbDebug(Short backstageDbDebug) {
        this.backstageDbDebug = backstageDbDebug;
    }

    public String getBackstageDirroot() {
        return backstageDirroot;
    }

    public void setBackstageDirroot(String backstageDirroot) {
        this.backstageDirroot = backstageDirroot;
    }

    public Short getEmailNotifyFiatWithdrawals() {
        return emailNotifyFiatWithdrawals;
    }

    public void setEmailNotifyFiatWithdrawals(Short emailNotifyFiatWithdrawals) {
        this.emailNotifyFiatWithdrawals = emailNotifyFiatWithdrawals;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getCloudflareApiKey() {
        return cloudflareApiKey;
    }

    public void setCloudflareApiKey(String cloudflareApiKey) {
        this.cloudflareApiKey = cloudflareApiKey;
    }

    public String getGoogleRecaptchApiKey() {
        return googleRecaptchApiKey;
    }

    public void setGoogleRecaptchApiKey(String googleRecaptchApiKey) {
        this.googleRecaptchApiKey = googleRecaptchApiKey;
    }

    public Short getCloudflareBlacklist() {
        return cloudflareBlacklist;
    }

    public void setCloudflareBlacklist(Short cloudflareBlacklist) {
        this.cloudflareBlacklist = cloudflareBlacklist;
    }

    public String getCloudflareEmail() {
        return cloudflareEmail;
    }

    public void setCloudflareEmail(String cloudflareEmail) {
        this.cloudflareEmail = cloudflareEmail;
    }

    public String getGoogleRecaptchApiSecret() {
        return googleRecaptchApiSecret;
    }

    public void setGoogleRecaptchApiSecret(String googleRecaptchApiSecret) {
        this.googleRecaptchApiSecret = googleRecaptchApiSecret;
    }

    public Integer getCloudflareBlacklistAttempts() {
        return cloudflareBlacklistAttempts;
    }

    public void setCloudflareBlacklistAttempts(Integer cloudflareBlacklistAttempts) {
        this.cloudflareBlacklistAttempts = cloudflareBlacklistAttempts;
    }

    public Double getCloudflareBlacklistTimeframe() {
        return cloudflareBlacklistTimeframe;
    }

    public void setCloudflareBlacklistTimeframe(Double cloudflareBlacklistTimeframe) {
        this.cloudflareBlacklistTimeframe = cloudflareBlacklistTimeframe;
    }

    public String getCryptoCapitalPk() {
        return cryptoCapitalPk;
    }

    public void setCryptoCapitalPk(String cryptoCapitalPk) {
        this.cryptoCapitalPk = cryptoCapitalPk;
    }

    public String getDepositFiatDesc() {
        return depositFiatDesc;
    }

    public void setDepositFiatDesc(String depositFiatDesc) {
        this.depositFiatDesc = depositFiatDesc;
    }

    public Short getEmailNotifyFiatFailed() {
        return emailNotifyFiatFailed;
    }

    public void setEmailNotifyFiatFailed(Short emailNotifyFiatFailed) {
        this.emailNotifyFiatFailed = emailNotifyFiatFailed;
    }

    public String getTradingStatus() {
        return tradingStatus;
    }

    public void setTradingStatus(String tradingStatus) {
        this.tradingStatus = tradingStatus;
    }

    public String getWithdrawalsStatus() {
        return withdrawalsStatus;
    }

    public void setWithdrawalsStatus(String withdrawalsStatus) {
        this.withdrawalsStatus = withdrawalsStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarAppConfig)) {
            return false;
        }
        BimarAppConfig other = (BimarAppConfig) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarAppConfig[ id=" + id + " ]";
    }

    public double getAffliatePct() {
        return affliatePct;
    }

    public void setAffliatePct(double affliatePct) {
        this.affliatePct = affliatePct;
    }

    public short getAffliateActive() {
        return affliateActive;
    }

    public void setAffliateActive(short affliateActive) {
        this.affliateActive = affliateActive;
    }
    
}
