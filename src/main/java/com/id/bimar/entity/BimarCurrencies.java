package com.id.bimar.entity;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_currencies", catalog = "bimardb", schema = "bimar")
public class BimarCurrencies implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Size(max = 3)
    @Column(name = "currency", length = 3)
    private String currency;
    @Size(max = 255)
    @Column(name = "fa_symbol", length = 255)
    private String faSymbol;
    @Column(name = "account_number")
    private BigInteger accountNumber;
    @Size(max = 255)
    @Column(name = "account_name", length = 255)
    private String accountName;
    @Column(name = "is_active")
    private Short isActive;
    @Size(max = 255)
    @Column(name = "usd_bid", length = 255)
    private String usdBid;
    @Size(max = 255)
    @Column(name = "usd_ask", length = 255)
    private String usdAsk;
    @Size(max = 255)
    @Column(name = "name_en", length = 255)
    private String nameEn;
    @Size(max = 255)
    @Column(name = "name_es", length = 255)
    private String nameEs;
    @Size(max = 255)
    @Column(name = "name_ru", length = 255)
    private String nameRu;
    @Size(max = 255)
    @Column(name = "name_zh", length = 255)
    private String nameZh;

    public BimarCurrencies() {
    }

    public BimarCurrencies(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFaSymbol() {
        return faSymbol;
    }

    public void setFaSymbol(String faSymbol) {
        this.faSymbol = faSymbol;
    }

    public BigInteger getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(BigInteger accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Short getIsActive() {
        return isActive;
    }

    public void setIsActive(Short isActive) {
        this.isActive = isActive;
    }

    public String getUsdBid() {
        return usdBid;
    }

    public void setUsdBid(String usdBid) {
        this.usdBid = usdBid;
    }

    public String getUsdAsk() {
        return usdAsk;
    }

    public void setUsdAsk(String usdAsk) {
        this.usdAsk = usdAsk;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameEs() {
        return nameEs;
    }

    public void setNameEs(String nameEs) {
        this.nameEs = nameEs;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameZh() {
        return nameZh;
    }

    public void setNameZh(String nameZh) {
        this.nameZh = nameZh;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarCurrencies)) {
            return false;
        }
        BimarCurrencies other = (BimarCurrencies) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarCurrencies[ id=" + id + " ]";
    }
    
}
