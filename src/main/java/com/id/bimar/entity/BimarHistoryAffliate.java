/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author afes
 */
@Entity
@Table(name = "bimar_history_affliate")
@NamedQueries({
    @NamedQuery(name = "BimarHistoryAffliate.findAll", query = "SELECT b FROM BimarHistoryAffliate b")})
public class BimarHistoryAffliate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "tgl_transaksi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglTransaksi;
    @Column(name = "id_user")
    private Integer idUser;
    @Column(name = "id_user_afflite")
    private Integer idUserAfflite;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;
    @Column(name = "amount_pct_affliate")
    private Double amountPctAffliate;

    public BimarHistoryAffliate() {
    }

    public BimarHistoryAffliate(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTglTransaksi() {
        return tglTransaksi;
    }

    public void setTglTransaksi(Date tglTransaksi) {
        this.tglTransaksi = tglTransaksi;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdUserAfflite() {
        return idUserAfflite;
    }

    public void setIdUserAfflite(Integer idUserAfflite) {
        this.idUserAfflite = idUserAfflite;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAmountPctAffliate() {
        return amountPctAffliate;
    }

    public void setAmountPctAffliate(Double amountPctAffliate) {
        this.amountPctAffliate = amountPctAffliate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarHistoryAffliate)) {
            return false;
        }
        BimarHistoryAffliate other = (BimarHistoryAffliate) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarHistoryAffliate[ id=" + id + " ]";
    }
    
}
