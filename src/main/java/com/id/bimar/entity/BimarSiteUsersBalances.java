package com.id.bimar.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_site_users_balances", catalog = "bimardb", schema = "bimar")
public class BimarSiteUsersBalances implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "balance", precision = 17, scale = 17)
    private Double balance;
    @Column(name = "site_user")
    private Integer siteUser;
    @Column(name = "coinid")
    private Integer coinid;

    public BimarSiteUsersBalances() {
    }

    public BimarSiteUsersBalances(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Integer getSiteUser() {
        return siteUser;
    }

    public void setSiteUser(Integer siteUser) {
        this.siteUser = siteUser;
    }

    public Integer getCoinid() {
        return coinid;
    }

    public void setCoinid(Integer coinid) {
        this.coinid = coinid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarSiteUsersBalances)) {
            return false;
        }
        BimarSiteUsersBalances other = (BimarSiteUsersBalances) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarSiteUsersBalances[ id=" + id + " ]";
    }
    
}
