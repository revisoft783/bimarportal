package com.id.bimar.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_orders", catalog = "bimardb", schema = "bimar")
public class BimarOrders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "order_type")
    private Integer orderType;
    @Column(name = "site_user")
    private Integer siteUser;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "btc", precision = 17, scale = 17)
    private Double btc;
    @Column(name = "fiat", precision = 17, scale = 17)
    private Double fiat;
    @Column(name = "currency")
    private Integer currency;
    @Column(name = "btc_price", precision = 17, scale = 17)
    private Double btcPrice;
    @Column(name = "market_price")
    private Short marketPrice;
    @Column(name = "log_id")
    private Integer logId;
    @Column(name = "stop_price", precision = 17, scale = 17)
    private Double stopPrice;
    @Column(name = "fee1", precision = 17, scale = 17)
    private Double fee;

    public BimarOrders() {
    }

    public BimarOrders(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getSiteUser() {
        return siteUser;
    }

    public void setSiteUser(Integer siteUser) {
        this.siteUser = siteUser;
    }

    public Double getBtc() {
        return btc;
    }

    public void setBtc(Double btc) {
        this.btc = btc;
    }

    public Double getFiat() {
        return fiat;
    }

    public void setFiat(Double fiat) {
        this.fiat = fiat;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public Double getBtcPrice() {
        return btcPrice;
    }

    public void setBtcPrice(Double btcPrice) {
        this.btcPrice = btcPrice;
    }

    public Short getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Short marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public Double getStopPrice() {
        return stopPrice;
    }

    public void setStopPrice(Double stopPrice) {
        this.stopPrice = stopPrice;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarOrders)) {
            return false;
        }
        BimarOrders other = (BimarOrders) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarOrders[ id=" + id + " ]";
    }
    
}
