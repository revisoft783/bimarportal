/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_requests", catalog = "bimardb", schema = "bimar")
public class BimarRequests implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "site_user")
    private Integer siteUser;
    @Column(name = "currency")
    private Integer currency;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount", precision = 17, scale = 17)
    private Double amount;
    @Column(name = "description")
    private Integer description;
    @Column(name = "request_status")
    private Integer requestStatus;
    @Column(name = "request_type")
    private Integer requestType;
    @Basic(optional = false)
    @Column(name = "address_id", nullable = false)
    private int addressId;
    @Column(name = "account")
    private BigInteger account;
    @Size(max = 255)
    @Column(name = "send_address", length = 255)
    private String sendAddress;
    @Size(max = 255)
    @Column(name = "transaction_id", length = 255)
    private String transactionId;
    @Column(name = "increment", precision = 17, scale = 17)
    private Double increment;
    @Column(name = "done")
    private Short done;
    @Column(name = "crypto_id")
    private Integer cryptoId;
    @Column(name = "fee", precision = 17, scale = 17)
    private Double fee;
    @Column(name = "net_amount", precision = 17, scale = 17)
    private Double netAmount;
    @Column(name = "notified")
    private Integer notified;
    @Column(name = "FCurrency")
    private Integer fcurrency;
    @Column(name = "TCurrency")
    private Integer tcurrency;
    @Column(name = "lastprice", precision = 17, scale = 17)
    private Double lastPrice;
    
    public BimarRequests() {
    }

    public BimarRequests(Integer id) {
        this.id = id;
    }

    public BimarRequests(Integer id, int addressId) {
        this.id = id;
        this.addressId = addressId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getSiteUser() {
        return siteUser;
    }

    public void setSiteUser(Integer siteUser) {
        this.siteUser = siteUser;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getDescription() {
        return description;
    }

    public void setDescription(Integer description) {
        this.description = description;
    }

    public Integer getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(Integer requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Integer getRequestType() {
        return requestType;
    }

    public void setRequestType(Integer requestType) {
        this.requestType = requestType;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public BigInteger getAccount() {
        return account;
    }

    public void setAccount(BigInteger account) {
        this.account = account;
    }

    public String getSendAddress() {
        return sendAddress;
    }

    public void setSendAddress(String sendAddress) {
        this.sendAddress = sendAddress;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getIncrement() {
        return increment;
    }

    public void setIncrement(Double increment) {
        this.increment = increment;
    }

    public Short getDone() {
        return done;
    }

    public void setDone(Short done) {
        this.done = done;
    }

    public Integer getCryptoId() {
        return cryptoId;
    }

    public void setCryptoId(Integer cryptoId) {
        this.cryptoId = cryptoId;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public Integer getNotified() {
        return notified;
    }

    public void setNotified(Integer notified) {
        this.notified = notified;
    }

    public Integer getFcurrency() {
        return fcurrency;
    }

    public void setFcurrency(Integer fcurrency) {
        this.fcurrency = fcurrency;
    }

    public Integer getTcurrency() {
        return tcurrency;
    }

    public void setTcurrency(Integer tcurrency) {
        this.tcurrency = tcurrency;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarRequests)) {
            return false;
        }
        BimarRequests other = (BimarRequests) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarRequests[ id=" + id + " ]";
    }

    public Double getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(Double lastPrice) {
        this.lastPrice = lastPrice;
    }
    
}
