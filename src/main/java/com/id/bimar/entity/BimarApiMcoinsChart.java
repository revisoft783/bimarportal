/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import com.id.bimar.util.Format;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author afes
 */
@Entity
@Table(name = "bimar_api_mcoins_chart",catalog = "bimardb", schema = "bimar")
public class BimarApiMcoinsChart implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 255)
    @Column(name = "fromcoins")
    private String fromcoins;
    @Size(max = 255)
    @Column(name = "tocoins")
    private String tocoins;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "last_price")
    private Double lastPrice;
    @Column(name = "last_update")
    private int lastUpdate;
    @Column(name = "last_volume")
    private Double lastVolume;
    @Column(name = "last_volume_to")
    private Double lastVolumeTo;
    @Column(name = "last_trade_id")
    private String lastTradeId;
    @Column(name = "volume_24h")
    private Double volume24h;
    @Column(name = "volume_24hto")
    private Double volume24hto;
    @Column(name = "open_24h")
    private Double open24h;
    @Column(name = "hight_24hto")
    private Double hight24hto;
    @Column(name = "low_24hto")
    private Double low24hto;
    @Column(name = "change_pct24h")
    private Double changePct24h;
    @Column(name = "change_24h")
    private Double change24h;
    @Column(name = "last_modified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;

    public BimarApiMcoinsChart() {
    }

    public BimarApiMcoinsChart(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromcoins() {
        return fromcoins;
    }

    public void setFromcoins(String fromcoins) {
        this.fromcoins = fromcoins;
    }

    public String getTocoins() {
        return tocoins;
    }

    public void setTocoins(String tocoins) {
        this.tocoins = tocoins;
    }

    public Double getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(Double lastPrice) {
        this.lastPrice = lastPrice;
    }

    public int getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(int lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Double getLastVolume() {
        return lastVolume;
    }

    public void setLastVolume(Double lastVolume) {
        this.lastVolume = lastVolume;
    }

    public Double getLastVolumeTo() {
        return lastVolumeTo;
    }

    public void setLastVolumeTo(Double lastVolumeTo) {
        this.lastVolumeTo = lastVolumeTo;
    }

    public String getLastTradeId() {
        return lastTradeId;
    }

    public void setLastTradeId(String lastTradeId) {
        this.lastTradeId = lastTradeId;
    }

    public Double getVolume24h() {
        return volume24h;
    }

    public void setVolume24h(Double volume24h) {
        this.volume24h = volume24h;
    }

    public Double getVolume24hto() {
        return volume24hto;
    }

    public void setVolume24hto(Double volume24hto) {
        this.volume24hto = volume24hto;
    }

    public Double getOpen24h() {
        return open24h;
    }

    public void setOpen24h(Double open24h) {
        this.open24h = open24h;
    }

    public Double getHight24hto() {
        return hight24hto;
    }

    public void setHight24hto(Double hight24hto) {
        this.hight24hto = hight24hto;
    }

    public Double getLow24hto() {
        return low24hto;
    }

    public void setLow24hto(Double low24hto) {
        this.low24hto = low24hto;
    }

    public Double getChangePct24h() {
        return changePct24h;
    }

    public void setChangePct24h(Double changePct24h) {
        this.changePct24h = changePct24h;
    }

    public Double getChange24h() {
        return change24h;
    }

    public void setChange24h(Double change24h) {
        this.change24h = change24h;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarApiMcoinsChart)) {
            return false;
        }
        BimarApiMcoinsChart other = (BimarApiMcoinsChart) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "["+lastUpdate+","+new Format("%.8f").format(open24h)+","+new Format("%.8f").format(hight24hto)+","+new Format("%.8f").format(low24hto)+","+new Format("%.8f").format(lastPrice)+","+new Format("%.8f").format(volume24h)+"]";
    }
    
}
