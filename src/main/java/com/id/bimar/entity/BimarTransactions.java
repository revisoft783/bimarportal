package com.id.bimar.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author yusril
 */
@Entity
@Table(name = "bimar_transactions", catalog = "bimardb", schema = "bimar")
public class BimarTransactions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "site_user")
    private Integer siteUser;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "btc", precision = 17, scale = 17)
    private Double btc;
    @Basic(optional = false)
    @Column(name = "currency", nullable = false)
    private int currency;
    @Column(name = "btc_price", precision = 17, scale = 17)
    private Double btcPrice;
    @Column(name = "fiat", precision = 17, scale = 17)
    private Double fiat;
    @Column(name = "fee", precision = 17, scale = 17)
    private Double fee;
    @Column(name = "transaction_type")
    private Integer transactionType;
    @Column(name = "site_user1")
    private Integer siteUser1;
    @Column(name = "transaction_type1")
    private Integer transactionType1;
    @Column(name = "fee1", precision = 17, scale = 17)
    private Double fee1;
    @Column(name = "btc_net", precision = 17, scale = 17)
    private Double btcNet;
    @Column(name = "btc_net1", precision = 17, scale = 17)
    private Double btcNet1;
    @Column(name = "btc_before1", precision = 17, scale = 17)
    private Double btcBefore1;
    @Column(name = "btc_after1", precision = 17, scale = 17)
    private Double btcAfter1;
    @Column(name = "fiat_before1", precision = 17, scale = 17)
    private Double fiatBefore1;
    @Column(name = "fiat_after1", precision = 17, scale = 17)
    private Double fiatAfter1;
    @Column(name = "btc_before", precision = 17, scale = 17)
    private Double btcBefore;
    @Column(name = "btc_after", precision = 17, scale = 17)
    private Double btcAfter;
    @Column(name = "fiat_before", precision = 17, scale = 17)
    private Double fiatBefore;
    @Column(name = "fiat_after", precision = 17, scale = 17)
    private Double fiatAfter;
    @Column(name = "log_id1")
    private Integer logId1;
    @Column(name = "log_id")
    private Integer logId;
    @Column(name = "fee_level", precision = 17, scale = 17)
    private Double feeLevel;
    @Column(name = "fee_level1", precision = 17, scale = 17)
    private Double feeLevel1;
    @Column(name = "currency1")
    private Integer currency1;
    @Column(name = "orig_btc_price", precision = 17, scale = 17)
    private Double origBtcPrice;
    @Column(name = "conversion_fee", precision = 17, scale = 17)
    private Double conversionFee;
    @Column(name = "convert_amount", precision = 17, scale = 17)
    private Double convertAmount;
    @Column(name = "convert_rate_given", precision = 17, scale = 17)
    private Double convertRateGiven;
    @Column(name = "convert_system_rate", precision = 17, scale = 17)
    private Double convertSystemRate;
    @Column(name = "convert_from_currency")
    private Integer convertFromCurrency;
    @Column(name = "convert_to_currency")
    private Integer convertToCurrency;
    @Column(name = "conversion")
    private Short conversion;
    @Column(name = "bid_at_transaction", precision = 17, scale = 17)
    private Double bidAtTransaction;
    @Column(name = "ask_at_transaction", precision = 17, scale = 17)
    private Double askAtTransaction;
    @Column(name = "factored")
    private Short factored;

    public BimarTransactions() {
    }

    public BimarTransactions(Integer id) {
        this.id = id;
    }

    public BimarTransactions(Integer id, int currency) {
        this.id = id;
        this.currency = currency;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getSiteUser() {
        return siteUser;
    }

    public void setSiteUser(Integer siteUser) {
        this.siteUser = siteUser;
    }

    public Double getBtc() {
        return btc;
    }

    public void setBtc(Double btc) {
        this.btc = btc;
    }

    public int getCurrency() {
        return currency;
    }

    public void setCurrency(int currency) {
        this.currency = currency;
    }

    public Double getBtcPrice() {
        return btcPrice;
    }

    public void setBtcPrice(Double btcPrice) {
        this.btcPrice = btcPrice;
    }

    public Double getFiat() {
        return fiat;
    }

    public void setFiat(Double fiat) {
        this.fiat = fiat;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Integer getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Integer transactionType) {
        this.transactionType = transactionType;
    }

    public Integer getSiteUser1() {
        return siteUser1;
    }

    public void setSiteUser1(Integer siteUser1) {
        this.siteUser1 = siteUser1;
    }

    public Integer getTransactionType1() {
        return transactionType1;
    }

    public void setTransactionType1(Integer transactionType1) {
        this.transactionType1 = transactionType1;
    }

    public Double getFee1() {
        return fee1;
    }

    public void setFee1(Double fee1) {
        this.fee1 = fee1;
    }

    public Double getBtcNet() {
        return btcNet;
    }

    public void setBtcNet(Double btcNet) {
        this.btcNet = btcNet;
    }

    public Double getBtcNet1() {
        return btcNet1;
    }

    public void setBtcNet1(Double btcNet1) {
        this.btcNet1 = btcNet1;
    }

    public Double getBtcBefore1() {
        return btcBefore1;
    }

    public void setBtcBefore1(Double btcBefore1) {
        this.btcBefore1 = btcBefore1;
    }

    public Double getBtcAfter1() {
        return btcAfter1;
    }

    public void setBtcAfter1(Double btcAfter1) {
        this.btcAfter1 = btcAfter1;
    }

    public Double getFiatBefore1() {
        return fiatBefore1;
    }

    public void setFiatBefore1(Double fiatBefore1) {
        this.fiatBefore1 = fiatBefore1;
    }

    public Double getFiatAfter1() {
        return fiatAfter1;
    }

    public void setFiatAfter1(Double fiatAfter1) {
        this.fiatAfter1 = fiatAfter1;
    }

    public Double getBtcBefore() {
        return btcBefore;
    }

    public void setBtcBefore(Double btcBefore) {
        this.btcBefore = btcBefore;
    }

    public Double getBtcAfter() {
        return btcAfter;
    }

    public void setBtcAfter(Double btcAfter) {
        this.btcAfter = btcAfter;
    }

    public Double getFiatBefore() {
        return fiatBefore;
    }

    public void setFiatBefore(Double fiatBefore) {
        this.fiatBefore = fiatBefore;
    }

    public Double getFiatAfter() {
        return fiatAfter;
    }

    public void setFiatAfter(Double fiatAfter) {
        this.fiatAfter = fiatAfter;
    }

    public Integer getLogId1() {
        return logId1;
    }

    public void setLogId1(Integer logId1) {
        this.logId1 = logId1;
    }

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public Double getFeeLevel() {
        return feeLevel;
    }

    public void setFeeLevel(Double feeLevel) {
        this.feeLevel = feeLevel;
    }

    public Double getFeeLevel1() {
        return feeLevel1;
    }

    public void setFeeLevel1(Double feeLevel1) {
        this.feeLevel1 = feeLevel1;
    }

    public Integer getCurrency1() {
        return currency1;
    }

    public void setCurrency1(Integer currency1) {
        this.currency1 = currency1;
    }

    public Double getOrigBtcPrice() {
        return origBtcPrice;
    }

    public void setOrigBtcPrice(Double origBtcPrice) {
        this.origBtcPrice = origBtcPrice;
    }

    public Double getConversionFee() {
        return conversionFee;
    }

    public void setConversionFee(Double conversionFee) {
        this.conversionFee = conversionFee;
    }

    public Double getConvertAmount() {
        return convertAmount;
    }

    public void setConvertAmount(Double convertAmount) {
        this.convertAmount = convertAmount;
    }

    public Double getConvertRateGiven() {
        return convertRateGiven;
    }

    public void setConvertRateGiven(Double convertRateGiven) {
        this.convertRateGiven = convertRateGiven;
    }

    public Double getConvertSystemRate() {
        return convertSystemRate;
    }

    public void setConvertSystemRate(Double convertSystemRate) {
        this.convertSystemRate = convertSystemRate;
    }

    public Integer getConvertFromCurrency() {
        return convertFromCurrency;
    }

    public void setConvertFromCurrency(Integer convertFromCurrency) {
        this.convertFromCurrency = convertFromCurrency;
    }

    public Integer getConvertToCurrency() {
        return convertToCurrency;
    }

    public void setConvertToCurrency(Integer convertToCurrency) {
        this.convertToCurrency = convertToCurrency;
    }

    public Short getConversion() {
        return conversion;
    }

    public void setConversion(Short conversion) {
        this.conversion = conversion;
    }

    public Double getBidAtTransaction() {
        return bidAtTransaction;
    }

    public void setBidAtTransaction(Double bidAtTransaction) {
        this.bidAtTransaction = bidAtTransaction;
    }

    public Double getAskAtTransaction() {
        return askAtTransaction;
    }

    public void setAskAtTransaction(Double askAtTransaction) {
        this.askAtTransaction = askAtTransaction;
    }

    public Short getFactored() {
        return factored;
    }

    public void setFactored(Short factored) {
        this.factored = factored;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarTransactions)) {
            return false;
        }
        BimarTransactions other = (BimarTransactions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarTransactions[ id=" + id + " ]";
    }
    
}
