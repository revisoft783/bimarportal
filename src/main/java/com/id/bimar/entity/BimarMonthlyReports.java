/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_monthly_reports", catalog = "bimardb", schema = "bimar")
public class BimarMonthlyReports implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "transactions_btc", precision = 17, scale = 17)
    private Double transactionsBtc;
    @Column(name = "avg_transaction_size_btc", precision = 17, scale = 17)
    private Double avgTransactionSizeBtc;
    @Column(name = "transaction_volume_per_user", precision = 17, scale = 17)
    private Double transactionVolumePerUser;
    @Column(name = "total_fees_btc", precision = 17, scale = 17)
    private Double totalFeesBtc;
    @Column(name = "fees_per_user_btc", precision = 17, scale = 17)
    private Double feesPerUserBtc;
    @Column(name = "gross_profit_btc", precision = 17, scale = 17)
    private Double grossProfitBtc;

    public BimarMonthlyReports() {
    }

    public BimarMonthlyReports(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getTransactionsBtc() {
        return transactionsBtc;
    }

    public void setTransactionsBtc(Double transactionsBtc) {
        this.transactionsBtc = transactionsBtc;
    }

    public Double getAvgTransactionSizeBtc() {
        return avgTransactionSizeBtc;
    }

    public void setAvgTransactionSizeBtc(Double avgTransactionSizeBtc) {
        this.avgTransactionSizeBtc = avgTransactionSizeBtc;
    }

    public Double getTransactionVolumePerUser() {
        return transactionVolumePerUser;
    }

    public void setTransactionVolumePerUser(Double transactionVolumePerUser) {
        this.transactionVolumePerUser = transactionVolumePerUser;
    }

    public Double getTotalFeesBtc() {
        return totalFeesBtc;
    }

    public void setTotalFeesBtc(Double totalFeesBtc) {
        this.totalFeesBtc = totalFeesBtc;
    }

    public Double getFeesPerUserBtc() {
        return feesPerUserBtc;
    }

    public void setFeesPerUserBtc(Double feesPerUserBtc) {
        this.feesPerUserBtc = feesPerUserBtc;
    }

    public Double getGrossProfitBtc() {
        return grossProfitBtc;
    }

    public void setGrossProfitBtc(Double grossProfitBtc) {
        this.grossProfitBtc = grossProfitBtc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarMonthlyReports)) {
            return false;
        }
        BimarMonthlyReports other = (BimarMonthlyReports) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarMonthlyReports[ id=" + id + " ]";
    }
    
}
