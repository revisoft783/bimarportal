/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_bitcoin_addresses", catalog = "bimardb", schema = "bimar")
public class BimarBitcoinAddresses implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Size(max = 255)
    @Column(name = "address", length = 255)
    private String address;
    @Column(name = "site_user")
    private Integer siteUser;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "system_address")
    private Short systemAddress;
    @Column(name = "hot_wallet")
    private Short hotWallet;
    @Column(name = "warm_wallet")
    private Short warmWallet;

    public BimarBitcoinAddresses() {
    }

    public BimarBitcoinAddresses(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getSiteUser() {
        return siteUser;
    }

    public void setSiteUser(Integer siteUser) {
        this.siteUser = siteUser;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Short getSystemAddress() {
        return systemAddress;
    }

    public void setSystemAddress(Short systemAddress) {
        this.systemAddress = systemAddress;
    }

    public Short getHotWallet() {
        return hotWallet;
    }

    public void setHotWallet(Short hotWallet) {
        this.hotWallet = hotWallet;
    }

    public Short getWarmWallet() {
        return warmWallet;
    }

    public void setWarmWallet(Short warmWallet) {
        this.warmWallet = warmWallet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarBitcoinAddresses)) {
            return false;
        }
        BimarBitcoinAddresses other = (BimarBitcoinAddresses) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarBitcoinAddresses[ id=" + id + " ]";
    }
    
}
