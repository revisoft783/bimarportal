package com.id.bimar.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author yusril
 */
public class CustomHistoryJsonClass {
    private int fromCoin;
    private int toCoin;
    private String type;
    private BigDecimal qty;
    private BigDecimal price;
    private BigDecimal amount;
    private Date trdatetime;

    public CustomHistoryJsonClass() {
    }

    public int getFromCoin() {
        return fromCoin;
    }

    public void setFromCoin(int fromCoin) {
        this.fromCoin = fromCoin;
    }

    public int getToCoin() {
        return toCoin;
    }

    public void setToCoin(int toCoin) {
        this.toCoin = toCoin;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getTrdatetime() {
        return trdatetime;
    }

    public void setTrdatetime(Date trdatetime) {
        this.trdatetime = trdatetime;
    }
    
}
