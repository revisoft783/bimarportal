/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_fee_schedule", catalog = "bimardb", schema = "bimar")
public class BimarFeeSchedule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "fee", precision = 17, scale = 17)
    private Double fee;
    @Column(name = "from_usd", precision = 17, scale = 17)
    private Double fromUsd;
    @Column(name = "to_usd", precision = 17, scale = 17)
    private Double toUsd;
    @Column(name = "orders")
    private Integer orders;
    @Column(name = "fee1", precision = 17, scale = 17)
    private Double fee1;
    @Column(name = "global_btc", precision = 17, scale = 17)
    private Double globalBtc;

    public BimarFeeSchedule() {
    }

    public BimarFeeSchedule(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Double getFromUsd() {
        return fromUsd;
    }

    public void setFromUsd(Double fromUsd) {
        this.fromUsd = fromUsd;
    }

    public Double getToUsd() {
        return toUsd;
    }

    public void setToUsd(Double toUsd) {
        this.toUsd = toUsd;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public Double getFee1() {
        return fee1;
    }

    public void setFee1(Double fee1) {
        this.fee1 = fee1;
    }

    public Double getGlobalBtc() {
        return globalBtc;
    }

    public void setGlobalBtc(Double globalBtc) {
        this.globalBtc = globalBtc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarFeeSchedule)) {
            return false;
        }
        BimarFeeSchedule other = (BimarFeeSchedule) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarFeeSchedule[ id=" + id + " ]";
    }
    
}
