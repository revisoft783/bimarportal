package com.id.bimar.entity;

/**
 * @author yusril
 */
    public class CustomTransJsonClass {
    private String id;
    private String message;
    private double fee;
    private double Qty;
    private double bfAmount;
    private double afAmount;

    public CustomTransJsonClass() {
    }

    public CustomTransJsonClass(String id, String message, double fee, double Qty, double bfAmount, double afAmount) {
        this.id = id;
        this.message = message;
        this.fee = fee;
        this.Qty = Qty;
        this.bfAmount = bfAmount;
        this.afAmount = afAmount;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getQty() {
        return Qty;
    }

    public void setQty(double Qty) {
        this.Qty = Qty;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public double getBfAmount() {
        return bfAmount;
    }

    public void setBfAmount(double bfAmount) {
        this.bfAmount = bfAmount;
    }

    public double getAfAmount() {
        return afAmount;
    }

    public void setAfAmount(double afAmount) {
        this.afAmount = afAmount;
    }

   
    
}
