/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_sessions", catalog = "bimardb", schema = "bimar")
public class BimarSessions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "session_id", nullable = false)
    private Integer sessionId;
    @Column(name = "session_start")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sessionStart;
    @Column(name = "session_end")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sessionEnd;
    @Size(max = 255)
    @Column(name = "session_key", length = 255)
    private String sessionKey;
    @Basic(optional = false)
    @Column(name = "user_id", nullable = false)
    private int userId;
    @Basic(optional = false)
    @Column(name = "nonce", nullable = false)
    private int nonce;
    @Column(name = "awaiting")
    private Short awaiting;
    @Size(max = 255)
    @Column(name = "ip", length = 255)
    private String ip;

    public BimarSessions() {
    }

    public BimarSessions(Integer sessionId) {
        this.sessionId = sessionId;
    }

    public BimarSessions(Integer sessionId, int userId, int nonce) {
        this.sessionId = sessionId;
        this.userId = userId;
        this.nonce = nonce;
    }

    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    public Date getSessionStart() {
        return sessionStart;
    }

    public void setSessionStart(Date sessionStart) {
        this.sessionStart = sessionStart;
    }

    public Date getSessionEnd() {
        return sessionEnd;
    }

    public void setSessionEnd(Date sessionEnd) {
        this.sessionEnd = sessionEnd;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getNonce() {
        return nonce;
    }

    public void setNonce(int nonce) {
        this.nonce = nonce;
    }

    public Short getAwaiting() {
        return awaiting;
    }

    public void setAwaiting(Short awaiting) {
        this.awaiting = awaiting;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sessionId != null ? sessionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarSessions)) {
            return false;
        }
        BimarSessions other = (BimarSessions) object;
        if ((this.sessionId == null && other.sessionId != null) || (this.sessionId != null && !this.sessionId.equals(other.sessionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarSessions[ sessionId=" + sessionId + " ]";
    }
    
}
