package com.id.bimar.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_site_users", catalog = "bimardb", schema = "bimar")
public class BimarSiteUsers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Size(max = 200)
    @Column(name = "pass", length = 200)
    private String pass;
    @Size(max = 200)
    @Column(name = "first_name", length = 200)
    private String firstName;
    @Size(max = 200)
    @Column(name = "last_name", length = 200)
    private String lastName;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "email", length = 255)
    private String email;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Size(max = 255)
    @Column(name = "tel", length = 255)
    private String tel;
    @Column(name = "country")
    private Integer country;
    @Size(max = 255)
    @Column(name = "users", length = 255)
    private String users;
    @Column(name = "fee_schedule")
    private Integer feeSchedule;
    @Column(name = "country_code")
    private Integer countryCode;
    @Column(name = "authy_requested")
    private Short authyRequested;
    @Column(name = "verified_authy")
    private Short verifiedAuthy;
    @Column(name = "using_sms")
    private Short usingSms;
    @Column(name = "dont_ask_30_days")
    private Short dontAsk30Days;
    @Column(name = "dont_ask_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dontAskDate;
    @Column(name = "confirm_withdrawal_email_btc")
    private Short confirmWithdrawalEmailBtc;
    @Column(name = "confirm_withdrawal_2fa_btc")
    private Short confirmWithdrawal2faBtc;
    @Column(name = "confirm_withdrawal_2fa_bank")
    private Short confirmWithdrawal2faBank;
    @Column(name = "confirm_withdrawal_email_bank")
    private Short confirmWithdrawalEmailBank;
    @Column(name = "notify_deposit_btc")
    private Short notifyDepositBtc;
    @Column(name = "notify_deposit_bank")
    private Short notifyDepositBank;
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @Column(name = "no_logins")
    private Short noLogins;
    @Column(name = "notify_login")
    private Short notifyLogin;
    @Column(name = "deactivated")
    private Short deactivated;
    @Column(name = "locked")
    private Short locked;
    @Size(max = 255)
    @Column(name = "google_2fa_code", length = 255)
    private String google2faCode;
    @Column(name = "verified_google")
    private Short verifiedGoogle;
    @Size(max = 255)
    @Column(name = "last_lang", length = 255)
    private String lastLang;
    @Column(name = "default_currency")
    private Integer defaultCurrency;
    @Column(name = "notify_withdraw_btc")
    private Short notifyWithdrawBtc;
    @Column(name = "notify_withdraw_bank")
    private Short notifyWithdrawBank;
    @Column(name = "trusted")
    private Short trusted;
    @Column(name = "is_admin")
    private Short isAdmin;
    @Column(name = "date_authy_id")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAuthyId;
    @Column(name = "orders")
    private Short orders;
    @Size(max = 50)
    @Column(name = "website")
    private String website;
    @Size(max = 50)
    @Column(name = "address")
    private String address;
    @Size(max = 50)
    @Column(name = "city")
    private String city;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 20)
    @Column(name = "phone")
    private String phone;
    @Size(max = 255)
    @Column(name = "authy_id")
    private String authyId;
    @OneToMany(mappedBy = "siteUser", fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<BimarApiAddress> bimarApiAddressList;
    @Column(name = "affliate")
    private Integer affliate;
    
    public BimarSiteUsers() {
    }

    public BimarSiteUsers(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public Integer getFeeSchedule() {
        return feeSchedule;
    }

    public void setFeeSchedule(Integer feeSchedule) {
        this.feeSchedule = feeSchedule;
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    public Short getAuthyRequested() {
        return authyRequested;
    }

    public void setAuthyRequested(Short authyRequested) {
        this.authyRequested = authyRequested;
    }

    public Short getVerifiedAuthy() {
        return verifiedAuthy;
    }

    public void setVerifiedAuthy(Short verifiedAuthy) {
        this.verifiedAuthy = verifiedAuthy;
    }

    public Short getUsingSms() {
        return usingSms;
    }

    public void setUsingSms(Short usingSms) {
        this.usingSms = usingSms;
    }

    public Short getDontAsk30Days() {
        return dontAsk30Days;
    }

    public void setDontAsk30Days(Short dontAsk30Days) {
        this.dontAsk30Days = dontAsk30Days;
    }

    public Date getDontAskDate() {
        return dontAskDate;
    }

    public void setDontAskDate(Date dontAskDate) {
        this.dontAskDate = dontAskDate;
    }

    public Short getConfirmWithdrawalEmailBtc() {
        return confirmWithdrawalEmailBtc;
    }

    public void setConfirmWithdrawalEmailBtc(Short confirmWithdrawalEmailBtc) {
        this.confirmWithdrawalEmailBtc = confirmWithdrawalEmailBtc;
    }

    public Short getConfirmWithdrawal2faBtc() {
        return confirmWithdrawal2faBtc;
    }

    public void setConfirmWithdrawal2faBtc(Short confirmWithdrawal2faBtc) {
        this.confirmWithdrawal2faBtc = confirmWithdrawal2faBtc;
    }

    public Short getConfirmWithdrawal2faBank() {
        return confirmWithdrawal2faBank;
    }

    public void setConfirmWithdrawal2faBank(Short confirmWithdrawal2faBank) {
        this.confirmWithdrawal2faBank = confirmWithdrawal2faBank;
    }

    public Short getConfirmWithdrawalEmailBank() {
        return confirmWithdrawalEmailBank;
    }

    public void setConfirmWithdrawalEmailBank(Short confirmWithdrawalEmailBank) {
        this.confirmWithdrawalEmailBank = confirmWithdrawalEmailBank;
    }

    public Short getNotifyDepositBtc() {
        return notifyDepositBtc;
    }

    public void setNotifyDepositBtc(Short notifyDepositBtc) {
        this.notifyDepositBtc = notifyDepositBtc;
    }

    public Short getNotifyDepositBank() {
        return notifyDepositBank;
    }

    public void setNotifyDepositBank(Short notifyDepositBank) {
        this.notifyDepositBank = notifyDepositBank;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Short getNoLogins() {
        return noLogins;
    }

    public void setNoLogins(Short noLogins) {
        this.noLogins = noLogins;
    }

    public Short getNotifyLogin() {
        return notifyLogin;
    }

    public void setNotifyLogin(Short notifyLogin) {
        this.notifyLogin = notifyLogin;
    }

    public Short getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(Short deactivated) {
        this.deactivated = deactivated;
    }

    public Short getLocked() {
        return locked;
    }

    public void setLocked(Short locked) {
        this.locked = locked;
    }

    public String getGoogle2faCode() {
        return google2faCode;
    }

    public void setGoogle2faCode(String google2faCode) {
        this.google2faCode = google2faCode;
    }

    public Short getVerifiedGoogle() {
        return verifiedGoogle;
    }

    public void setVerifiedGoogle(Short verifiedGoogle) {
        this.verifiedGoogle = verifiedGoogle;
    }

    public String getLastLang() {
        return lastLang;
    }

    public void setLastLang(String lastLang) {
        this.lastLang = lastLang;
    }

    public Integer getDefaultCurrency() {
        return defaultCurrency;
    }

    public void setDefaultCurrency(Integer defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    public Short getNotifyWithdrawBtc() {
        return notifyWithdrawBtc;
    }

    public void setNotifyWithdrawBtc(Short notifyWithdrawBtc) {
        this.notifyWithdrawBtc = notifyWithdrawBtc;
    }

    public Short getNotifyWithdrawBank() {
        return notifyWithdrawBank;
    }

    public void setNotifyWithdrawBank(Short notifyWithdrawBank) {
        this.notifyWithdrawBank = notifyWithdrawBank;
    }

    public Short getTrusted() {
        return trusted;
    }

    public void setTrusted(Short trusted) {
        this.trusted = trusted;
    }

    public Short getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Short isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Date getDateAuthyId() {
        return dateAuthyId;
    }

    public void setDateAuthyId(Date dateAuthyId) {
        this.dateAuthyId = dateAuthyId;
    }

    public Short getOrders() {
        return orders;
    }

    public void setOrders(Short orders) {
        this.orders = orders;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAuthyId() {
        return authyId;
    }

    public void setAuthyId(String authyId) {
        this.authyId = authyId;
    }

    @XmlTransient
    @JsonIgnore
    public List<BimarApiAddress> getBimarApiAddressList() {
        return bimarApiAddressList;
    }

    public void setBimarApiAddressList(List<BimarApiAddress> bimarApiAddressList) {
        this.bimarApiAddressList = bimarApiAddressList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarSiteUsers)) {
            return false;
        }
        BimarSiteUsers other = (BimarSiteUsers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarSiteUsers[ id=" + id + " ]";
    }

    public Integer getAffliate() {
        return affliate;
    }

    public void setAffliate(Integer affliate) {
        this.affliate = affliate;
    }
    
}
