/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_current_stats", catalog = "bimardb", schema = "bimar")
public class BimarCurrentStats implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "total_btc")
    private BigInteger totalBtc;
    @Column(name = "market_cap")
    private BigInteger marketCap;
    @Column(name = "trade_volume")
    private BigInteger tradeVolume;

    public BimarCurrentStats() {
    }

    public BimarCurrentStats(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigInteger getTotalBtc() {
        return totalBtc;
    }

    public void setTotalBtc(BigInteger totalBtc) {
        this.totalBtc = totalBtc;
    }

    public BigInteger getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(BigInteger marketCap) {
        this.marketCap = marketCap;
    }

    public BigInteger getTradeVolume() {
        return tradeVolume;
    }

    public void setTradeVolume(BigInteger tradeVolume) {
        this.tradeVolume = tradeVolume;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarCurrentStats)) {
            return false;
        }
        BimarCurrentStats other = (BimarCurrentStats) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarCurrentStats[ id=" + id + " ]";
    }
    
}
