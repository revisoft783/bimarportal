/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yusril
 */
@Entity
@Table(name = "bimar_emails", catalog = "bimardb", schema = "bimar")
public class BimarEmails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Size(max = 255)
    @Column(name = "key", length = 255)
    private String key;
    @Size(max = 255)
    @Column(name = "title_en", length = 255)
    private String titleEn;
    @Size(max = 255)
    @Column(name = "title_es", length = 255)
    private String titleEs;
    @Lob
    @Column(name = "content_en")
    private String contentEn;
    @Lob
    @Column(name = "content_es")
    private byte[] contentEs;
    @Size(max = 255)
    @Column(name = "title_ru", length = 255)
    private String titleRu;
    @Size(max = 255)
    @Column(name = "title_zh", length = 255)
    private String titleZh;
    @Lob
    @Column(name = "content_ru")
    private byte[] contentRu;
    @Lob
    @Column(name = "content_zh")
    private byte[] contentZh;

    public BimarEmails() {
    }

    public BimarEmails(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getTitleEs() {
        return titleEs;
    }

    public void setTitleEs(String titleEs) {
        this.titleEs = titleEs;
    }

    public String getContentEn() {
        return contentEn;
    }

    public void setContentEn(String contentEn) {
        this.contentEn = contentEn;
    }

    public byte[] getContentEs() {
        return contentEs;
    }

    public void setContentEs(byte[] contentEs) {
        this.contentEs = contentEs;
    }

    public String getTitleRu() {
        return titleRu;
    }

    public void setTitleRu(String titleRu) {
        this.titleRu = titleRu;
    }

    public String getTitleZh() {
        return titleZh;
    }

    public void setTitleZh(String titleZh) {
        this.titleZh = titleZh;
    }

    public byte[] getContentRu() {
        return contentRu;
    }

    public void setContentRu(byte[] contentRu) {
        this.contentRu = contentRu;
    }

    public byte[] getContentZh() {
        return contentZh;
    }

    public void setContentZh(byte[] contentZh) {
        this.contentZh = contentZh;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarEmails)) {
            return false;
        }
        BimarEmails other = (BimarEmails) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarEmails[ id=" + id + " ]";
    }
    
}
