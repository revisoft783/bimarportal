package com.id.bimar.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * @author yusril
 */
@Entity
@Table(name = "bimar_api_coins",catalog = "bimardb", schema = "bimar")
public class BimarApiCoins implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "key")
    private String key;
    @Size(max = 255)
    @Column(name = "secret")
    private String secret;
    @Size(max = 255)
    @Column(name = "website")
    private String website;
    @Column(name = "currencyID")
    private Integer currencyid;
    @OneToMany(mappedBy = "idCoins", fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<BimarApiAddress> bimarApiAddressList;
    @OneToMany(mappedBy = "idCoins", fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<BimarApiMcoins> bimarApiMCoinsList;
    
    public BimarApiCoins() {
    }

    public BimarApiCoins(Integer id, String name, String key, String secret, String website, Integer currencyid, List<BimarApiAddress> bimarApiAddressList) {
        this.id = id;
        this.name = name;
        this.key = key;
        this.secret = secret;
        this.website = website;
        this.currencyid = currencyid;
        this.bimarApiAddressList = bimarApiAddressList;
    }
    
    public BimarApiCoins(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Integer getCurrencyid() {
        return currencyid;
    }

    public void setCurrencyid(Integer currencyid) {
        this.currencyid = currencyid;
    }

    @XmlTransient
    @JsonIgnore
    public List<BimarApiAddress> getBimarApiAddressList() {
        return bimarApiAddressList;
    }

    public void setBimarApiAddressList(List<BimarApiAddress> bimarApiAddressList) {
        this.bimarApiAddressList = bimarApiAddressList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BimarApiCoins)) {
            return false;
        }
        BimarApiCoins other = (BimarApiCoins) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.id.bimar.entity.BimarApiCoins[ id=" + id + " ]";
    }

    public List<BimarApiMcoins> getBimarApiMCoinsList() {
        return bimarApiMCoinsList;
    }

    public void setBimarApiMCoinsList(List<BimarApiMcoins> bimarApiMCoinsList) {
        this.bimarApiMCoinsList = bimarApiMCoinsList;
    }
    
}
