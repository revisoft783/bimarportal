/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.id.bimar.entity;

import com.google.gson.annotations.SerializedName;
import io.block.api.model.Response;

/**
 *
 * @author afes
 */
public class ResponceBimar {
    public String status;
    
    public class ResponseError extends Response {
        @SerializedName("data")
        public io.block.api.model.Error error;
    }
    
    public class ResponseBimarApiMCoins extends Response {
        @SerializedName("data")
        public com.id.bimar.entity.BimarApiMcoins bimarApiMcoins;
    }
}
